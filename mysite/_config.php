<?php

global $project;
$project = 'mysite';

global $database;
$database = 'santos';

// Use _ss_environment.php file for configuration
require_once("conf/ConfigureFromEnv.php");

// Set the site locale
i18n::set_locale('en_NZ');

HtmlEditorConfig::get('cms')->setOptions(array(
  "skin" => "default",
	"style_formats" => array(
		
		array(
			"title" => "Lead",
			"selector" => "p",
			"classes" => "lead"
		)


	)
));
HtmlEditorConfig::get('cms')->setOption('content_css', project() . '/css/editor.css');
//HtmlEditorConfig::get('cms')->setOption('ContentCSS', project() . '/css/editor.css');
HtmlEditorConfig::get('cms')->setButtonsForLine(1, array());
HtmlEditorConfig::get('cms')->setButtonsForLine(2, array());
HtmlEditorConfig::get('cms')->setButtonsForLine(3, array());

HtmlEditorConfig::get('cms')->setButtonsForLine(1, 'pastetext', 'styleselect', 'formatselect', 'bold', 'italic', 'underline', 'strikethrough', 'separator', 'justifyleft', 'justifyright', 'justifycenter', 'justifyfull', 'separator', 'bullist', 'numlist', 'separator', 'image', 'link', 'unlink', 'anchor', 'separator', 'charmap', 'code', 'fullscreen','hr');