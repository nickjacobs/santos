<?php
//Model
class CafePage extends Page
{
	private static $db = array(				
	);

	private static $has_many = array(
	'Photos' => 'Photo'				
	);


	
	//CMS fields
	function getCMSFields() 
	{
		$fields = parent::getCMSFields();

		$gridFieldConfig = GridFieldConfig_RecordEditor::create(); 
		$gridFieldConfig->addComponent(new GridFieldSortableRows('SortOrder'));
		$gridfield = new GridField("Photos", "Photos", $this->Photos()->sort("SortOrder"), $gridFieldConfig);
		
		$fields->addFieldToTab('Root.Photos', $gridfield);
		
		
		return $fields;	
	}

}

// Controller
class CafePage_Controller extends Page_Controller
{
}

