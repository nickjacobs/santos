<?php
/**
 * Testimonial
 */
class CoffeeItem extends DataObject
{

	
	private static $db = array (
		'Title'      	=> 'Varchar(255)',		
		'Content'     	=> 'HTMLText',
		'BGColour'      => 'Text',
		'Sort' => 'Int'	
	);
	private static $has_one = array (
		'Image' => 'Image'
		//'Link' => 'SiteTree'
		);
	private static $default_sort = 'Sort';
	
	
		
	/*
	* Method to show CMS fields for creating or updating
	*/
	public function getCMSFields()
	{
		$fields =  new FieldList(
			TextField::create('Title'),			
			HtmlEditorField::create('Content'),
			Textfield::create('BGColour'),
			UploadField::create('Image')			
		);
		
		return $fields;
	}
	
	/**
	 * Can view the record
	 */
	public function canView($member = null) {
        return true;
    }

    public function getShopLink()
    {
        $maincategory = ProductCategory::get()
            ->sort(
                array(
                    "ParentID" => "ASC",
                    "ID"       => "ASC",
                )
            )->first();
        if ($maincategory) {
            return $maincategory->Link();
        }

        return Director::baseURL();
    }
}