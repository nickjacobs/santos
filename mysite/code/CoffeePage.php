<?php
//Model
class CoffeePage extends Page
{
	private static $db = array(	
	'Content2' => 'HTMLText'			
	);

	private static $has_many = array(
			
	);


	
	//CMS fields
	function getCMSFields() 
	{
		$fields = parent::getCMSFields();

		// $gridFieldConfig = GridFieldConfig_RecordEditor::create(); 
		// $gridFieldConfig->addComponent(new GridFieldSortableRows('SortOrder'));
		// $gridfield = new GridField("Photos", "Photos", $this->Photos()->sort("SortOrder"), $gridFieldConfig);
		
		// $fields->addFieldToTab('Root.Photos', $gridfield);
		$fields->addFieldToTab('Root.Main', HtmlEditorField::create('Content2', 'Second content section (after products)'));
		return $fields;	
	}

}

// Controller
class CoffeePage_Controller extends Page_Controller
{


	public function FeaturedProducts(){
		$ret = Product::get()
				->filter("Featured", true)
				->sort("Sort ASC");
		return $ret;

	}


	public function CoffeeItems() {
			return CoffeeItem::get()->sort('Sort ASC');
	}
}


