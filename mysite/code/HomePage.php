<?php
//Model
class HomePage extends Page
{
	private static $db = array(	
	'Block1Text'=>'HTMLText',
	'Block2Text'=>'HTMLText',
	'Block3Text'=>'HTMLText'			
	);
	
	//permissions
	function canCreate($Member = null){
	    if(permission::check('Administrator')){
	        return true;
	    }else{
	        return false;
	    }
	}
	function canDelete($Member = null){
	    if(permission::check('Administrator')){
	        return true;
	    }else{
	        return false;
	    }
	}	

	
	//CMS fields
	function getCMSFields() 
	{
		$fields = parent::getCMSFields();
		$fields->removeByName('Content');
		$fields->addFieldToTab("Root.Main", HtmlEditorField::create('Block1Text', 'Block1Text')->setRows(6));	
		$fields->addFieldToTab("Root.Main", HtmlEditorField::create('Block2Text', 'Block2Text')->setRows(6));	
		$fields->addFieldToTab("Root.Main", HtmlEditorField::create('Block3Text', 'Block3Text')->setRows(6));	

		
		
		return $fields;	
	}

}

// Controller
class HomePage_Controller extends Page_Controller
{
	//Define our form function as allowed
	private static $allowed_actions = array(
		
	);

	public function Testimonials() {
		return Testimonial::get()->sort('RAND()');
	}
	
	

}

