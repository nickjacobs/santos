<?php

class TestimonialAdmin extends ModelAdmin
{
    private static $menu_icon = 'framework/admin/images/menu-icons/16x16/pencil.png';

    private static $managed_models = array(
        'Testimonial'
    );
    private static $url_segment = 'testimonials';
    private static $menu_title = 'Testimonials';
    //private static $menu_priority = 12;

    public $showImportForm = false;

}







class CoffeeAdmin extends ModelAdmin
{
    private static $menu_icon = 'framework/admin/images/menu-icons/16x16/pencil.png';

    private static $managed_models = array(
        'CoffeeItem'
    );
    private static $url_segment = 'coffeeproducts';
    private static $menu_title = 'Coffee Products';
    //private static $menu_priority = 12;

    public $showImportForm = false;

    public function getEditForm($id = null, $fields = null) {
        $form=parent::getEditForm($id, $fields);

        //This check is simply to ensure you are on the managed model you want adjust accordingly
        if($this->modelClass=='CoffeeItem' && $gridField=$form->Fields()->dataFieldByName($this->sanitiseClassName($this->modelClass))) {
            //This is just a precaution to ensure we got a GridField from dataFieldByName() which you should have
            if($gridField instanceof GridField) {
                $gridField->getConfig()->addComponent(new GridFieldSortableRows('Sort'));
            }
        }

        return $form;
    }
}



