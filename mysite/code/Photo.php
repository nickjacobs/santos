<?php

class Photo extends DataObject
{
	private static $db = array (
	
	'SortOrder' => 'Int'
	
	);
	
	private static $has_one = array (
		'Page' => 'SiteTree',
		'PhotoImage' => 'BetterImage'
		
	);
	
	static $summary_fields = array(
			'Thumbnail' => ''
		);



	
	public function getCMSFields()
	{
		$f = new FieldList();		  
	

		
		$ff1 = new UploadField('PhotoImage','Upload a JPG,JPEG,or PNG file');
		$ff1->setFolderName('Gallery');
		$f->push($ff1);
		
		return $f;
	}
	
	
	function LinkToPage(){
		return $this->PageLink()->Link();
	}
	
		
	
	function getThumbnail() 
	{
    if ($Image = $this->PhotoImage()) 
    {
        return $Image->SetWidth(150);
    } 
    else
    {
        return '(No Image)';
    }
	}
	
	
		

	
}