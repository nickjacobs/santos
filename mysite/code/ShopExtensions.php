<?php



class ProductExtension extends DataExtension {

    private static $db = array(
      'ProductGroup'=>'Enum("250g,500g,1kg","250g")'
    );

    private static $casting = array(
        'variationPrice' => 'Currency',
    );



	public function updateCMSFields(FieldList $fields) {
		  $fields->removeByName('Model');
          $fields->removeByName('Banner');
          $fields->removeByName('Height');
          $fields->removeByName('Width');
          $fields->removeByName('Depth');
          $fields->addFieldToTab('Root.Main', new DropdownField('ProductGroup','Product Group:',
            singleton('Product')->dbObject('ProductGroup')->enumValues()),'Content');

    }
    public function VarWeight(){
        if($pv = ProductVariation::get()->filter("ProductID", $this->owner->ID)->first()){
           return $pv->Weight;
        }               
    }

    public function niceweight(){
        if($pv = ProductVariation::get()->filter("ProductID", $this->owner->ID)->first()){
    	   $ow = $pv->Weight;

           if($ow >=1){
                return ($ow*1) . 'KG';
            }else{
                return ($ow*1000) . 'G';
            }
        }   	    	
    }

    public function variationPrice() {
        if($pv = ProductVariation::get()->filter("ProductID", $this->owner->ID)->first())
                return $pv->Price;
    }
}

class ProductCategoryExtension extends DataExtension {

	public function OrderedProducts($recursive = false){
		return $this->owner->ProductsShowable()->sort('Sort');
	}
}

class ProductVariationExtension extends DataExtension {

	private static $title_has_label   = false;

	public function updateCMSFields(FieldList $fields) {
		  $fields->removeByName('Image');
          $fields->removeByName('Height');
          $fields->removeByName('Width');
          $fields->removeByName('Depth');
    }
}




class ProductOrderItemExtension extends DataExtension {

  public function TableMenuTitle()
    {
        $product = $this->owner->Product();
        $tabletitle = ($product) ? $product->MenuTitle : $this->i18n_singular_name();
        $this->owner->extend('updateTableTitle', $tabletitle);
        return $tabletitle;
    }

}