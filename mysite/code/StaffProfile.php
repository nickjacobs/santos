<?php
class StaffProfile extends Page {

	private static $db = array(
	);

	private static $has_one = array(
		'StaffImage' => 'BetterImage'
	);

	private static $has_many = array(	
	);


	//CMS fields
	function getCMSFields() 
	{
		$fields = parent::getCMSFields();		
			$fields->removeByName('Banner');
			$fields->addFieldToTab("Root.Main", UploadField::create('StaffImage', 'Staff photo')->setFolderName('Staff'));

		return $fields;	
	}

}
class StaffProfile_Controller extends Page_Controller {

	
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		
	}

	

}
