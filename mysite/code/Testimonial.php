<?php
/**
 * Testimonial
 */
class Testimonial extends DataObject
{
	private static $singular_name = 'Testimonial';
    private static $plural_name   = 'Testimonials';
	
	private static $db = array (
		'Author'      	=> 'Varchar(255)',		
		'Comment'     	=> 'Text'
		
	);
	
	private static $summary_fields = array(
		'Author'   => 'Author'		
	);	
	
		
	/*
	* Method to show CMS fields for creating or updating
	*/
	public function getCMSFields()
	{
		$fields =  new FieldList(
			TextField::create('Author', _t('Testimonial.AUTHOR', 'Author')),			
			TextareaField::create('Comment', _t('Testimonial.COMMENT', 'Comment'))			
		);
		
		return $fields;
	}
	
	/**
	 * Can view the record
	 */
	public function canView($member = null) {
        return true;
    }
}