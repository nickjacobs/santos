/*
 Navicat MySQL Data Transfer

 Source Server         : local
 Source Server Version : 50542
 Source Host           : localhost
 Source Database       : santos

 Target Server Version : 50542
 File Encoding         : utf-8

 Date: 09/02/2016 13:08:11 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `Address`
-- ----------------------------
DROP TABLE IF EXISTS `Address`;
CREATE TABLE `Address` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Address') CHARACTER SET utf8 DEFAULT 'Address',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Country` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `State` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `City` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `PostalCode` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `AddressLine2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Company` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `FirstName` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Surname` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Phone` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `MemberID` (`MemberID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `ArchiveWidget`
-- ----------------------------
DROP TABLE IF EXISTS `ArchiveWidget`;
CREATE TABLE `ArchiveWidget` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DisplayMode` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Blog`
-- ----------------------------
DROP TABLE IF EXISTS `Blog`;
CREATE TABLE `Blog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PostsPerPage` int(11) NOT NULL DEFAULT '0',
  `InheritSideBar` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SideBarID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SideBarID` (`SideBarID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Blog`
-- ----------------------------
BEGIN;
INSERT INTO `Blog` VALUES ('12', '6', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `BlogArchiveWidget`
-- ----------------------------
DROP TABLE IF EXISTS `BlogArchiveWidget`;
CREATE TABLE `BlogArchiveWidget` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NumberToDisplay` int(11) NOT NULL DEFAULT '0',
  `ArchiveType` enum('Monthly','Yearly') CHARACTER SET utf8 DEFAULT 'Monthly',
  `BlogID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `BlogCategoriesWidget`
-- ----------------------------
DROP TABLE IF EXISTS `BlogCategoriesWidget`;
CREATE TABLE `BlogCategoriesWidget` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Limit` int(11) NOT NULL DEFAULT '0',
  `Order` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Direction` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `BlogCategoriesWidget`
-- ----------------------------
BEGIN;
INSERT INTO `BlogCategoriesWidget` VALUES ('1', '0', 'Title', 'ASC', '12');
COMMIT;

-- ----------------------------
--  Table structure for `BlogCategory`
-- ----------------------------
DROP TABLE IF EXISTS `BlogCategory`;
CREATE TABLE `BlogCategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('BlogCategory') CHARACTER SET utf8 DEFAULT 'BlogCategory',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `BlogCategory`
-- ----------------------------
BEGIN;
INSERT INTO `BlogCategory` VALUES ('1', 'BlogCategory', '2016-09-01 13:23:43', '2016-09-01 13:23:43', 'Cafe', 'cafe', '12'), ('2', 'BlogCategory', '2016-09-02 12:32:01', '2016-09-01 13:23:50', 'Roastery', 'roastery', '12'), ('3', 'BlogCategory', '2016-09-02 12:32:01', '2016-09-01 13:23:56', 'Coffee', 'coffee', '12'), ('4', 'BlogCategory', '2016-09-01 13:24:01', '2016-09-01 13:24:01', 'Machines', 'machines', '12'), ('5', 'BlogCategory', '2016-09-01 13:24:06', '2016-09-01 13:24:06', 'Events', 'events', '12');
COMMIT;

-- ----------------------------
--  Table structure for `BlogEntry`
-- ----------------------------
DROP TABLE IF EXISTS `BlogEntry`;
CREATE TABLE `BlogEntry` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` datetime DEFAULT NULL,
  `Author` mediumtext CHARACTER SET utf8,
  `Tags` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `BlogEntry_Live`
-- ----------------------------
DROP TABLE IF EXISTS `BlogEntry_Live`;
CREATE TABLE `BlogEntry_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Date` datetime DEFAULT NULL,
  `Author` mediumtext CHARACTER SET utf8,
  `Tags` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `BlogEntry_versions`
-- ----------------------------
DROP TABLE IF EXISTS `BlogEntry_versions`;
CREATE TABLE `BlogEntry_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Date` datetime DEFAULT NULL,
  `Author` mediumtext CHARACTER SET utf8,
  `Tags` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `BlogHolder`
-- ----------------------------
DROP TABLE IF EXISTS `BlogHolder`;
CREATE TABLE `BlogHolder` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AllowCustomAuthors` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowFullEntry` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `BlogHolder_Live`
-- ----------------------------
DROP TABLE IF EXISTS `BlogHolder_Live`;
CREATE TABLE `BlogHolder_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AllowCustomAuthors` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowFullEntry` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `BlogHolder_versions`
-- ----------------------------
DROP TABLE IF EXISTS `BlogHolder_versions`;
CREATE TABLE `BlogHolder_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `AllowCustomAuthors` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowFullEntry` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `OwnerID` (`OwnerID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `BlogPost`
-- ----------------------------
DROP TABLE IF EXISTS `BlogPost`;
CREATE TABLE `BlogPost` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PublishDate` datetime DEFAULT NULL,
  `AuthorNames` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Summary` mediumtext CHARACTER SET utf8,
  `FeaturedImageID` int(11) NOT NULL DEFAULT '0',
  `InheritSideBar` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SideBarID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FeaturedImageID` (`FeaturedImageID`),
  KEY `SideBarID` (`SideBarID`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `BlogPost`
-- ----------------------------
BEGIN;
INSERT INTO `BlogPost` VALUES ('17', '2016-09-01 10:12:56', null, null, '31', '0', '0'), ('18', '2016-09-01 10:13:30', null, null, '32', '0', '0'), ('19', '2016-09-01 10:14:03', null, null, '33', '0', '0'), ('20', '2016-09-01 10:43:00', null, null, '0', '1', '4'), ('21', '2016-09-01 10:47:59', null, null, '0', '0', '0'), ('22', '2016-09-01 10:48:00', null, null, '36', '1', '3'), ('23', '2016-09-01 11:45:00', null, null, '37', '1', '2');
COMMIT;

-- ----------------------------
--  Table structure for `BlogPost_Authors`
-- ----------------------------
DROP TABLE IF EXISTS `BlogPost_Authors`;
CREATE TABLE `BlogPost_Authors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogPostID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogPostID` (`BlogPostID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `BlogPost_Authors`
-- ----------------------------
BEGIN;
INSERT INTO `BlogPost_Authors` VALUES ('1', '17', '1'), ('2', '18', '1'), ('3', '19', '1'), ('4', '20', '1'), ('5', '21', '1'), ('6', '22', '1'), ('7', '23', '1'), ('8', '67', '1');
COMMIT;

-- ----------------------------
--  Table structure for `BlogPost_Categories`
-- ----------------------------
DROP TABLE IF EXISTS `BlogPost_Categories`;
CREATE TABLE `BlogPost_Categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogPostID` int(11) NOT NULL DEFAULT '0',
  `BlogCategoryID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogPostID` (`BlogPostID`),
  KEY `BlogCategoryID` (`BlogCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `BlogPost_Categories`
-- ----------------------------
BEGIN;
INSERT INTO `BlogPost_Categories` VALUES ('1', '23', '2'), ('2', '23', '3');
COMMIT;

-- ----------------------------
--  Table structure for `BlogPost_Live`
-- ----------------------------
DROP TABLE IF EXISTS `BlogPost_Live`;
CREATE TABLE `BlogPost_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PublishDate` datetime DEFAULT NULL,
  `AuthorNames` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Summary` mediumtext CHARACTER SET utf8,
  `FeaturedImageID` int(11) NOT NULL DEFAULT '0',
  `InheritSideBar` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `SideBarID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FeaturedImageID` (`FeaturedImageID`),
  KEY `SideBarID` (`SideBarID`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `BlogPost_Live`
-- ----------------------------
BEGIN;
INSERT INTO `BlogPost_Live` VALUES ('17', '2016-09-01 10:12:56', null, null, '31', '1', '0'), ('18', '2016-09-01 10:13:30', null, null, '32', '1', '0'), ('19', '2016-09-01 10:14:03', null, null, '33', '1', '0'), ('20', '2016-09-01 10:43:00', null, null, '0', '1', '4'), ('21', '2016-09-01 10:47:59', null, null, '0', '1', '0'), ('22', '2016-09-01 10:48:00', null, null, '36', '1', '3'), ('23', '2016-09-01 11:45:00', null, null, '37', '1', '2');
COMMIT;

-- ----------------------------
--  Table structure for `BlogPost_Tags`
-- ----------------------------
DROP TABLE IF EXISTS `BlogPost_Tags`;
CREATE TABLE `BlogPost_Tags` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogPostID` int(11) NOT NULL DEFAULT '0',
  `BlogTagID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogPostID` (`BlogPostID`),
  KEY `BlogTagID` (`BlogTagID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `BlogPost_Tags`
-- ----------------------------
BEGIN;
INSERT INTO `BlogPost_Tags` VALUES ('1', '23', '1'), ('2', '23', '2'), ('3', '23', '3');
COMMIT;

-- ----------------------------
--  Table structure for `BlogPost_versions`
-- ----------------------------
DROP TABLE IF EXISTS `BlogPost_versions`;
CREATE TABLE `BlogPost_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `PublishDate` datetime DEFAULT NULL,
  `AuthorNames` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Summary` mediumtext CHARACTER SET utf8,
  `FeaturedImageID` int(11) NOT NULL DEFAULT '0',
  `InheritSideBar` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SideBarID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `FeaturedImageID` (`FeaturedImageID`),
  KEY `SideBarID` (`SideBarID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `BlogPost_versions`
-- ----------------------------
BEGIN;
INSERT INTO `BlogPost_versions` VALUES ('1', '17', '1', null, null, null, '0', '0', '0'), ('2', '17', '2', null, null, null, '31', '0', '0'), ('3', '17', '3', '2016-09-01 10:12:56', null, null, '31', '0', '0'), ('4', '18', '1', null, null, null, '0', '0', '0'), ('5', '18', '2', null, null, null, '32', '0', '0'), ('6', '18', '3', '2016-09-01 10:13:30', null, null, '32', '0', '0'), ('7', '19', '1', null, null, null, '0', '0', '0'), ('8', '19', '2', null, null, null, '33', '0', '0'), ('9', '19', '3', '2016-09-01 10:14:03', null, null, '33', '0', '0'), ('10', '20', '1', null, null, null, '0', '0', '0'), ('11', '20', '2', null, null, null, '0', '0', '0'), ('12', '20', '3', '2016-09-01 10:43:03', null, null, '0', '0', '0'), ('13', '21', '1', null, null, null, '0', '0', '0'), ('14', '21', '2', null, null, null, '0', '0', '0'), ('15', '21', '3', '2016-09-01 10:47:59', null, null, '0', '0', '0'), ('16', '22', '1', null, null, null, '0', '0', '0'), ('17', '22', '2', null, null, null, '36', '0', '0'), ('18', '22', '3', '2016-09-01 10:48:46', null, null, '36', '0', '0'), ('19', '23', '1', null, null, null, '0', '0', '0'), ('20', '23', '2', null, null, null, '37', '0', '0'), ('21', '23', '3', '2016-09-01 11:45:55', null, null, '37', '0', '0'), ('22', '23', '4', '2016-09-01 11:45:00', null, null, '37', '0', '2'), ('23', '23', '5', '2016-09-01 11:45:00', null, null, '37', '1', '2'), ('24', '22', '4', '2016-09-01 10:48:00', null, null, '36', '1', '3'), ('25', '20', '4', '2016-09-01 10:43:00', null, null, '0', '1', '4'), ('26', '67', '1', null, null, null, '0', '1', '0'), ('27', '67', '2', null, null, null, '0', '1', '5'), ('28', '67', '3', '2016-09-02 12:41:25', null, null, '0', '1', '5');
COMMIT;

-- ----------------------------
--  Table structure for `BlogRecentPostsWidget`
-- ----------------------------
DROP TABLE IF EXISTS `BlogRecentPostsWidget`;
CREATE TABLE `BlogRecentPostsWidget` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NumberOfPosts` int(11) NOT NULL DEFAULT '0',
  `BlogID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `BlogTag`
-- ----------------------------
DROP TABLE IF EXISTS `BlogTag`;
CREATE TABLE `BlogTag` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('BlogTag') CHARACTER SET utf8 DEFAULT 'BlogTag',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `BlogTag`
-- ----------------------------
BEGIN;
INSERT INTO `BlogTag` VALUES ('1', 'BlogTag', '2016-09-02 12:32:01', '2016-09-01 13:24:13', 'Cup', 'cup', '12'), ('2', 'BlogTag', '2016-09-02 12:32:01', '2016-09-01 13:24:18', 'Auckland', 'auckland', '12'), ('3', 'BlogTag', '2016-09-02 12:32:01', '2016-09-01 13:24:26', 'Tag', 'tag', '12'), ('4', 'BlogTag', '2016-09-01 13:24:30', '2016-09-01 13:24:30', 'Tag2', 'tag2', '12');
COMMIT;

-- ----------------------------
--  Table structure for `BlogTagsCloudWidget`
-- ----------------------------
DROP TABLE IF EXISTS `BlogTagsCloudWidget`;
CREATE TABLE `BlogTagsCloudWidget` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `BlogTagsWidget`
-- ----------------------------
DROP TABLE IF EXISTS `BlogTagsWidget`;
CREATE TABLE `BlogTagsWidget` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Limit` int(11) NOT NULL DEFAULT '0',
  `Order` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Direction` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `BlogTagsWidget`
-- ----------------------------
BEGIN;
INSERT INTO `BlogTagsWidget` VALUES ('2', '0', 'Title', 'ASC', '12');
COMMIT;

-- ----------------------------
--  Table structure for `BlogTree`
-- ----------------------------
DROP TABLE IF EXISTS `BlogTree`;
CREATE TABLE `BlogTree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LandingPageFreshness` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `BlogTree_Live`
-- ----------------------------
DROP TABLE IF EXISTS `BlogTree_Live`;
CREATE TABLE `BlogTree_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LandingPageFreshness` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `BlogTree_versions`
-- ----------------------------
DROP TABLE IF EXISTS `BlogTree_versions`;
CREATE TABLE `BlogTree_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `LandingPageFreshness` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Blog_Contributors`
-- ----------------------------
DROP TABLE IF EXISTS `Blog_Contributors`;
CREATE TABLE `Blog_Contributors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Blog_Editors`
-- ----------------------------
DROP TABLE IF EXISTS `Blog_Editors`;
CREATE TABLE `Blog_Editors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Blog_Live`
-- ----------------------------
DROP TABLE IF EXISTS `Blog_Live`;
CREATE TABLE `Blog_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PostsPerPage` int(11) NOT NULL DEFAULT '0',
  `InheritSideBar` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SideBarID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SideBarID` (`SideBarID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Blog_Live`
-- ----------------------------
BEGIN;
INSERT INTO `Blog_Live` VALUES ('12', '6', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `Blog_Writers`
-- ----------------------------
DROP TABLE IF EXISTS `Blog_Writers`;
CREATE TABLE `Blog_Writers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BlogID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BlogID` (`BlogID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Blog_versions`
-- ----------------------------
DROP TABLE IF EXISTS `Blog_versions`;
CREATE TABLE `Blog_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `PostsPerPage` int(11) NOT NULL DEFAULT '0',
  `InheritSideBar` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SideBarID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `SideBarID` (`SideBarID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Blog_versions`
-- ----------------------------
BEGIN;
INSERT INTO `Blog_versions` VALUES ('1', '12', '3', '10', '0', '0'), ('2', '12', '4', '10', '0', '0'), ('3', '12', '5', '4', '0', '0'), ('4', '12', '6', '6', '0', '0'), ('5', '12', '7', '6', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `CartPage`
-- ----------------------------
DROP TABLE IF EXISTS `CartPage`;
CREATE TABLE `CartPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CheckoutPageID` int(11) NOT NULL DEFAULT '0',
  `ContinuePageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CheckoutPageID` (`CheckoutPageID`),
  KEY `ContinuePageID` (`ContinuePageID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `CartPage`
-- ----------------------------
BEGIN;
INSERT INTO `CartPage` VALUES ('25', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `CartPage_Live`
-- ----------------------------
DROP TABLE IF EXISTS `CartPage_Live`;
CREATE TABLE `CartPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CheckoutPageID` int(11) NOT NULL DEFAULT '0',
  `ContinuePageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CheckoutPageID` (`CheckoutPageID`),
  KEY `ContinuePageID` (`ContinuePageID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `CartPage_Live`
-- ----------------------------
BEGIN;
INSERT INTO `CartPage_Live` VALUES ('25', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `CartPage_versions`
-- ----------------------------
DROP TABLE IF EXISTS `CartPage_versions`;
CREATE TABLE `CartPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `CheckoutPageID` int(11) NOT NULL DEFAULT '0',
  `ContinuePageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `CheckoutPageID` (`CheckoutPageID`),
  KEY `ContinuePageID` (`ContinuePageID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `CartPage_versions`
-- ----------------------------
BEGIN;
INSERT INTO `CartPage_versions` VALUES ('1', '25', '1', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `CheckoutPage`
-- ----------------------------
DROP TABLE IF EXISTS `CheckoutPage`;
CREATE TABLE `CheckoutPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PurchaseComplete` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `CheckoutPage`
-- ----------------------------
BEGIN;
INSERT INTO `CheckoutPage` VALUES ('26', null);
COMMIT;

-- ----------------------------
--  Table structure for `CheckoutPage_Live`
-- ----------------------------
DROP TABLE IF EXISTS `CheckoutPage_Live`;
CREATE TABLE `CheckoutPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PurchaseComplete` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `CheckoutPage_Live`
-- ----------------------------
BEGIN;
INSERT INTO `CheckoutPage_Live` VALUES ('26', null);
COMMIT;

-- ----------------------------
--  Table structure for `CheckoutPage_versions`
-- ----------------------------
DROP TABLE IF EXISTS `CheckoutPage_versions`;
CREATE TABLE `CheckoutPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `PurchaseComplete` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `CheckoutPage_versions`
-- ----------------------------
BEGIN;
INSERT INTO `CheckoutPage_versions` VALUES ('1', '26', '1', null);
COMMIT;

-- ----------------------------
--  Table structure for `CoffeePage`
-- ----------------------------
DROP TABLE IF EXISTS `CoffeePage`;
CREATE TABLE `CoffeePage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Content2` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `CoffeePage`
-- ----------------------------
BEGIN;
INSERT INTO `CoffeePage` VALUES ('9', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term \'specialty coffee\' refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>');
COMMIT;

-- ----------------------------
--  Table structure for `CoffeePage_Live`
-- ----------------------------
DROP TABLE IF EXISTS `CoffeePage_Live`;
CREATE TABLE `CoffeePage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Content2` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `CoffeePage_Live`
-- ----------------------------
BEGIN;
INSERT INTO `CoffeePage_Live` VALUES ('9', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term \'specialty coffee\' refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>');
COMMIT;

-- ----------------------------
--  Table structure for `CoffeePage_versions`
-- ----------------------------
DROP TABLE IF EXISTS `CoffeePage_versions`;
CREATE TABLE `CoffeePage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Content2` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `CoffeePage_versions`
-- ----------------------------
BEGIN;
INSERT INTO `CoffeePage_versions` VALUES ('1', '9', '4', null), ('2', '9', '5', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term \'specialty coffee\' refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>'), ('3', '9', '6', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term \'specialty coffee\' refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>'), ('4', '9', '7', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term \'specialty coffee\' refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>'), ('5', '9', '8', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term \'specialty coffee\' refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>'), ('6', '9', '9', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term \'specialty coffee\' refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>'), ('7', '9', '10', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term \'specialty coffee\' refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>'), ('8', '9', '11', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term \'specialty coffee\' refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>'), ('9', '9', '12', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term \'specialty coffee\' refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>'), ('10', '9', '13', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term \'specialty coffee\' refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>');
COMMIT;

-- ----------------------------
--  Table structure for `Comment`
-- ----------------------------
DROP TABLE IF EXISTS `Comment`;
CREATE TABLE `Comment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Comment') CHARACTER SET utf8 DEFAULT 'Comment',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `Comment` mediumtext CHARACTER SET utf8,
  `Email` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `URL` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BaseClass` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `Moderated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsSpam` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `AllowHtml` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SecretToken` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Depth` int(11) NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `ParentCommentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `AuthorID` (`AuthorID`),
  KEY `ParentCommentID` (`ParentCommentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `ErrorPage`
-- ----------------------------
DROP TABLE IF EXISTS `ErrorPage`;
CREATE TABLE `ErrorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ErrorPage`
-- ----------------------------
BEGIN;
INSERT INTO `ErrorPage` VALUES ('4', '404'), ('5', '500');
COMMIT;

-- ----------------------------
--  Table structure for `ErrorPage_Live`
-- ----------------------------
DROP TABLE IF EXISTS `ErrorPage_Live`;
CREATE TABLE `ErrorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ErrorPage_Live`
-- ----------------------------
BEGIN;
INSERT INTO `ErrorPage_Live` VALUES ('4', '404'), ('5', '500');
COMMIT;

-- ----------------------------
--  Table structure for `ErrorPage_versions`
-- ----------------------------
DROP TABLE IF EXISTS `ErrorPage_versions`;
CREATE TABLE `ErrorPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `File`
-- ----------------------------
DROP TABLE IF EXISTS `File`;
CREATE TABLE `File` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('File','Folder','Image','Image_Cached','BetterImage') CHARACTER SET utf8 DEFAULT 'File',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Filename` mediumtext CHARACTER SET utf8,
  `Content` mediumtext CHARACTER SET utf8,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `File`
-- ----------------------------
BEGIN;
INSERT INTO `File` VALUES ('1', 'Folder', '2016-08-29 13:39:34', '2016-08-29 13:39:34', 'Banners', 'Banners', 'assets/Banners/', null, '1', '0', '1'), ('2', 'Image', '2016-08-29 13:39:34', '2016-08-29 13:39:34', 'home-banner2.jpg', 'home banner2', 'assets/Banners/home-banner2.jpg', null, '1', '1', '1'), ('3', 'Image', '2016-08-29 14:48:20', '2016-08-29 14:48:20', 'Cafe-Banner.jpg', 'Cafe Banner', 'assets/Banners/Cafe-Banner.jpg', null, '1', '1', '1'), ('4', 'Image', '2016-08-29 14:48:30', '2016-08-29 14:48:30', 'Cafe-Banner2.jpg', 'Cafe Banner2', 'assets/Banners/Cafe-Banner2.jpg', null, '1', '1', '1'), ('5', 'Image', '2016-08-29 14:48:45', '2016-08-29 14:48:45', 'Cafe-Banner3.jpg', 'Cafe Banner3', 'assets/Banners/Cafe-Banner3.jpg', null, '1', '1', '1'), ('6', 'Image', '2016-08-29 14:52:29', '2016-08-29 14:52:29', 'cafesantos.jpg', 'cafesantos', 'assets/Banners/cafesantos.jpg', null, '1', '1', '1'), ('7', 'Folder', '2016-08-29 14:54:10', '2016-08-29 14:54:10', 'Gallery', 'Gallery', 'assets/Gallery/', null, '1', '0', '1'), ('8', 'BetterImage', '2016-08-29 14:54:10', '2016-08-29 14:54:10', '2016-Santos-031.jpg', '2016 Santos 031', 'assets/Gallery/2016-Santos-031.jpg', null, '1', '7', '1'), ('9', 'BetterImage', '2016-08-29 14:54:25', '2016-08-29 14:54:25', '2016-Santos-035.jpg', '2016 Santos 035', 'assets/Gallery/2016-Santos-035.jpg', null, '1', '7', '1'), ('10', 'BetterImage', '2016-08-29 14:54:36', '2016-08-29 14:54:36', '2016-Santos-036.jpg', '2016 Santos 036', 'assets/Gallery/2016-Santos-036.jpg', null, '1', '7', '1'), ('11', 'BetterImage', '2016-08-29 14:54:58', '2016-08-29 14:54:58', '2016-Santos-057.jpg', '2016 Santos 057', 'assets/Gallery/2016-Santos-057.jpg', null, '1', '7', '1'), ('12', 'BetterImage', '2016-08-29 14:55:27', '2016-08-29 14:55:27', '2016-Santos-066.jpg', '2016 Santos 066', 'assets/Gallery/2016-Santos-066.jpg', null, '1', '7', '1'), ('13', 'BetterImage', '2016-08-29 14:55:40', '2016-08-29 14:55:40', '2016-Santos-073.jpg', '2016 Santos 073', 'assets/Gallery/2016-Santos-073.jpg', null, '1', '7', '1'), ('14', 'BetterImage', '2016-08-29 14:55:54', '2016-08-29 14:55:54', '2016-Santos-082.jpg', '2016 Santos 082', 'assets/Gallery/2016-Santos-082.jpg', null, '1', '7', '1'), ('15', 'BetterImage', '2016-08-29 14:56:09', '2016-08-29 14:56:09', '2016-Santos-101.jpg', '2016 Santos 101', 'assets/Gallery/2016-Santos-101.jpg', null, '1', '7', '1'), ('16', 'BetterImage', '2016-08-29 14:56:20', '2016-08-29 14:56:20', '2016-Santos-136.jpg', '2016 Santos 136', 'assets/Gallery/2016-Santos-136.jpg', null, '1', '7', '1'), ('17', 'BetterImage', '2016-08-29 14:56:53', '2016-08-29 14:56:53', '2016-Santos-171.jpg', '2016 Santos 171', 'assets/Gallery/2016-Santos-171.jpg', null, '1', '7', '1'), ('18', 'Image', '2016-08-29 17:32:19', '2016-08-29 17:32:19', 'About-Banner.jpg', 'About Banner', 'assets/Banners/About-Banner.jpg', null, '1', '1', '1'), ('19', 'Folder', '2016-08-29 17:41:19', '2016-08-29 17:41:19', 'Staff', 'Staff', 'assets/Staff/', null, '1', '0', '1'), ('20', 'BetterImage', '2016-08-29 17:41:19', '2016-08-29 17:41:19', 'george.jpg', 'george', 'assets/Staff/george.jpg', null, '1', '19', '1'), ('21', 'BetterImage', '2016-08-29 18:30:29', '2016-08-29 18:30:29', 'buck.jpg', 'buck', 'assets/Staff/buck.jpg', null, '1', '19', '1'), ('22', 'BetterImage', '2016-08-29 18:31:14', '2016-08-29 18:31:14', 'wilma.jpg', 'wilma', 'assets/Staff/wilma.jpg', null, '1', '19', '1'), ('23', 'BetterImage', '2016-08-29 18:41:19', '2016-08-29 18:41:19', 'wilma2.jpg', 'wilma2', 'assets/Staff/wilma2.jpg', null, '1', '19', '1'), ('24', 'BetterImage', '2016-08-29 18:49:32', '2016-08-29 18:49:32', 'rosa.jpg', 'rosa', 'assets/Staff/rosa.jpg', null, '1', '19', '1'), ('26', 'Image', '2016-08-29 18:51:43', '2016-08-29 18:51:43', 'Team-Banner.jpg', 'Team Banner', 'assets/Banners/Team-Banner.jpg', null, '1', '1', '1'), ('27', 'Folder', '2016-08-30 10:46:41', '2016-08-30 10:46:41', 'Uploads', 'Uploads', 'assets/Uploads/', null, '1', '0', '1'), ('28', 'Image', '2016-08-30 10:46:41', '2016-08-30 10:46:41', 'sanremo.png', 'sanremo', 'assets/Uploads/sanremo.png', null, '1', '27', '1'), ('29', 'Image', '2016-08-30 10:48:47', '2016-08-30 10:48:47', 'sanremo2.png', 'sanremo2', 'assets/Uploads/sanremo2.png', null, '1', '27', '1'), ('30', 'Image', '2016-08-30 10:57:00', '2016-08-30 10:57:00', 'sanremo.jpg', 'sanremo', 'assets/Uploads/sanremo.jpg', null, '1', '27', '1'), ('31', 'Image', '2016-09-01 10:12:54', '2016-09-01 10:12:54', '2016-Santos-031.jpg', '2016 Santos 031', 'assets/Uploads/2016-Santos-031.jpg', null, '1', '27', '1'), ('32', 'Image', '2016-09-01 10:13:28', '2016-09-01 10:13:28', '2016-Santos-136.jpg', '2016 Santos 136', 'assets/Uploads/2016-Santos-136.jpg', null, '1', '27', '1'), ('33', 'Image', '2016-09-01 10:13:59', '2016-09-01 10:13:59', '2016-Santos-077.jpg', '2016 Santos 077', 'assets/Uploads/2016-Santos-077.jpg', null, '1', '27', '1'), ('34', 'Image', '2016-09-01 10:26:17', '2016-09-01 10:26:17', 'News-Banner.jpg', 'News Banner', 'assets/Banners/News-Banner.jpg', null, '1', '1', '1'), ('35', 'Image', '2016-09-01 10:48:31', '2016-09-01 10:48:31', '2016-Santos-044.jpg', '2016 Santos 044', 'assets/Uploads/2016-Santos-044.jpg', null, '1', '27', '1'), ('36', 'Image', '2016-09-01 10:48:43', '2016-09-01 10:48:43', '2016-Santos-082.jpg', '2016 Santos 082', 'assets/Uploads/2016-Santos-082.jpg', null, '1', '27', '1'), ('37', 'Image', '2016-09-01 11:45:53', '2016-09-01 11:45:53', '2016-Santos-073.jpg', '2016 Santos 073', 'assets/Uploads/2016-Santos-073.jpg', null, '1', '27', '1'), ('38', 'Image', '2016-09-02 08:03:01', '2016-09-02 08:03:01', 'marcelos.png', 'marcelos', 'assets/Uploads/marcelos.png', null, '1', '27', '1'), ('39', 'Image', '2016-09-02 08:03:20', '2016-09-02 08:03:20', 'cityblend.png', 'cityblend', 'assets/Uploads/cityblend.png', null, '1', '27', '1'), ('40', 'Image', '2016-09-02 08:06:42', '2016-09-02 08:06:42', 'santosorganic.png', 'santosorganic', 'assets/Uploads/santosorganic.png', null, '1', '27', '1'), ('41', 'Image', '2016-09-02 08:07:01', '2016-09-02 08:07:01', 'decaf.png', 'decaf', 'assets/Uploads/decaf.png', null, '1', '27', '1'), ('42', 'File', '2016-09-02 10:28:25', '2016-09-02 10:28:25', 'roasted-in-nz-blue.svg', 'roasted in nz blue', 'assets/Uploads/roasted-in-nz-blue.svg', null, '1', '27', '1'), ('43', 'Image', '2016-09-02 12:06:11', '2016-09-02 12:06:11', 'northern.png', 'northern', 'assets/Uploads/northern.png', null, '1', '27', '1'), ('44', 'Image', '2016-09-02 12:57:43', '2016-09-02 12:57:43', 'awards.png', 'awards', 'assets/Uploads/awards.png', null, '1', '27', '1'), ('45', 'File', '2016-09-02 13:01:09', '2016-09-02 13:01:09', 'certified-green.svg', 'certified green', 'assets/Uploads/certified-green.svg', null, '1', '27', '1');
COMMIT;

-- ----------------------------
--  Table structure for `Forum`
-- ----------------------------
DROP TABLE IF EXISTS `Forum`;
CREATE TABLE `Forum` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Abstract` mediumtext CHARACTER SET utf8,
  `CanPostType` enum('Inherit','Anyone','LoggedInUsers','OnlyTheseUsers','NoOne') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanAttachFiles` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ModeratorID` int(11) NOT NULL DEFAULT '0',
  `CategoryID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ModeratorID` (`ModeratorID`),
  KEY `CategoryID` (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Forum`
-- ----------------------------
BEGIN;
INSERT INTO `Forum` VALUES ('7', null, 'Inherit', '0', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `ForumCategory`
-- ----------------------------
DROP TABLE IF EXISTS `ForumCategory`;
CREATE TABLE `ForumCategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('ForumCategory') CHARACTER SET utf8 DEFAULT 'ForumCategory',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `StackableOrder` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `ForumHolderID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ForumHolderID` (`ForumHolderID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ForumCategory`
-- ----------------------------
BEGIN;
INSERT INTO `ForumCategory` VALUES ('1', 'ForumCategory', '2016-05-04 09:32:16', '2016-05-04 09:32:16', 'General', null, '6');
COMMIT;

-- ----------------------------
--  Table structure for `ForumHolder`
-- ----------------------------
DROP TABLE IF EXISTS `ForumHolder`;
CREATE TABLE `ForumHolder` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HolderSubtitle` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ProfileSubtitle` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ForumSubtitle` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `HolderAbstract` mediumtext CHARACTER SET utf8,
  `ProfileAbstract` mediumtext CHARACTER SET utf8,
  `ForumAbstract` mediumtext CHARACTER SET utf8,
  `ProfileModify` mediumtext CHARACTER SET utf8,
  `ProfileAdd` mediumtext CHARACTER SET utf8,
  `DisplaySignatures` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInCategories` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AllowGravatars` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `GravatarType` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `ForbiddenWords` mediumtext CHARACTER SET utf8,
  `CanPostType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','NoOne') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ForumHolder`
-- ----------------------------
BEGIN;
INSERT INTO `ForumHolder` VALUES ('6', 'Welcome to our forum!', 'Edit Your Profile', 'Start a new topic', '<p>If this is your first visit, you will need to <a class=\"broken\" title=\"Click here to register\" href=\"ForumMemberProfile/register\">register</a> before you can post. However, you can browse all messages below.</p>', '<p>Please fill out the fields below. You can choose whether some are publically visible by using the checkbox for each one.</p>', '<p>From here you can start a new topic.</p>', '<p>Thanks, your member profile has been modified.</p>', '<p>Thanks, you are now signed up to the forum.</p>', '0', '0', '0', null, null, 'LoggedInUsers');
COMMIT;

-- ----------------------------
--  Table structure for `ForumHolder_Live`
-- ----------------------------
DROP TABLE IF EXISTS `ForumHolder_Live`;
CREATE TABLE `ForumHolder_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HolderSubtitle` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ProfileSubtitle` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ForumSubtitle` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `HolderAbstract` mediumtext CHARACTER SET utf8,
  `ProfileAbstract` mediumtext CHARACTER SET utf8,
  `ForumAbstract` mediumtext CHARACTER SET utf8,
  `ProfileModify` mediumtext CHARACTER SET utf8,
  `ProfileAdd` mediumtext CHARACTER SET utf8,
  `DisplaySignatures` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInCategories` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AllowGravatars` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `GravatarType` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `ForbiddenWords` mediumtext CHARACTER SET utf8,
  `CanPostType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','NoOne') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ForumHolder_Live`
-- ----------------------------
BEGIN;
INSERT INTO `ForumHolder_Live` VALUES ('6', 'Welcome to our forum!', 'Edit Your Profile', 'Start a new topic', '<p>If this is your first visit, you will need to <a class=\"broken\" title=\"Click here to register\" href=\"ForumMemberProfile/register\">register</a> before you can post. However, you can browse all messages below.</p>', '<p>Please fill out the fields below. You can choose whether some are publically visible by using the checkbox for each one.</p>', '<p>From here you can start a new topic.</p>', '<p>Thanks, your member profile has been modified.</p>', '<p>Thanks, you are now signed up to the forum.</p>', '0', '0', '0', null, null, 'LoggedInUsers');
COMMIT;

-- ----------------------------
--  Table structure for `ForumHolder_versions`
-- ----------------------------
DROP TABLE IF EXISTS `ForumHolder_versions`;
CREATE TABLE `ForumHolder_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `HolderSubtitle` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ProfileSubtitle` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ForumSubtitle` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `HolderAbstract` mediumtext CHARACTER SET utf8,
  `ProfileAbstract` mediumtext CHARACTER SET utf8,
  `ForumAbstract` mediumtext CHARACTER SET utf8,
  `ProfileModify` mediumtext CHARACTER SET utf8,
  `ProfileAdd` mediumtext CHARACTER SET utf8,
  `DisplaySignatures` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInCategories` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AllowGravatars` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `GravatarType` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `ForbiddenWords` mediumtext CHARACTER SET utf8,
  `CanPostType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','NoOne') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ForumHolder_versions`
-- ----------------------------
BEGIN;
INSERT INTO `ForumHolder_versions` VALUES ('1', '6', '1', 'Welcome to our forum!', 'Edit Your Profile', 'Start a new topic', '<p>If this is your first visit, you will need to <a class=\"broken\" title=\"Click here to register\" href=\"ForumMemberProfile/register\">register</a> before you can post. However, you can browse all messages below.</p>', '<p>Please fill out the fields below. You can choose whether some are publically visible by using the checkbox for each one.</p>', '<p>From here you can start a new topic.</p>', '<p>Thanks, your member profile has been modified.</p>', '<p>Thanks, you are now signed up to the forum.</p>', '0', '0', '0', null, null, 'LoggedInUsers');
COMMIT;

-- ----------------------------
--  Table structure for `ForumThread`
-- ----------------------------
DROP TABLE IF EXISTS `ForumThread`;
CREATE TABLE `ForumThread` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('ForumThread') CHARACTER SET utf8 DEFAULT 'ForumThread',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `NumViews` int(11) NOT NULL DEFAULT '0',
  `IsSticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsReadOnly` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsGlobalSticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ForumID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ForumID` (`ForumID`),
  KEY `IsSticky` (`IsSticky`),
  KEY `IsGlobalSticky` (`IsGlobalSticky`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `ForumThread_Subscription`
-- ----------------------------
DROP TABLE IF EXISTS `ForumThread_Subscription`;
CREATE TABLE `ForumThread_Subscription` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('ForumThread_Subscription') CHARACTER SET utf8 DEFAULT 'ForumThread_Subscription',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `LastSent` datetime DEFAULT NULL,
  `ThreadID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ThreadID` (`ThreadID`),
  KEY `MemberID` (`MemberID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Forum_Live`
-- ----------------------------
DROP TABLE IF EXISTS `Forum_Live`;
CREATE TABLE `Forum_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Abstract` mediumtext CHARACTER SET utf8,
  `CanPostType` enum('Inherit','Anyone','LoggedInUsers','OnlyTheseUsers','NoOne') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanAttachFiles` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ModeratorID` int(11) NOT NULL DEFAULT '0',
  `CategoryID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ModeratorID` (`ModeratorID`),
  KEY `CategoryID` (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Forum_Live`
-- ----------------------------
BEGIN;
INSERT INTO `Forum_Live` VALUES ('7', null, 'Inherit', '0', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `Forum_Moderators`
-- ----------------------------
DROP TABLE IF EXISTS `Forum_Moderators`;
CREATE TABLE `Forum_Moderators` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ForumID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ForumID` (`ForumID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Forum_PosterGroups`
-- ----------------------------
DROP TABLE IF EXISTS `Forum_PosterGroups`;
CREATE TABLE `Forum_PosterGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ForumID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ForumID` (`ForumID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Forum_versions`
-- ----------------------------
DROP TABLE IF EXISTS `Forum_versions`;
CREATE TABLE `Forum_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Abstract` mediumtext CHARACTER SET utf8,
  `CanPostType` enum('Inherit','Anyone','LoggedInUsers','OnlyTheseUsers','NoOne') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanAttachFiles` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ModeratorID` int(11) NOT NULL DEFAULT '0',
  `CategoryID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `ModeratorID` (`ModeratorID`),
  KEY `CategoryID` (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Forum_versions`
-- ----------------------------
BEGIN;
INSERT INTO `Forum_versions` VALUES ('1', '7', '1', null, 'Inherit', '0', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `GatewayMessage`
-- ----------------------------
DROP TABLE IF EXISTS `GatewayMessage`;
CREATE TABLE `GatewayMessage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Gateway` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Reference` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Code` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `GatewayRequestMessage`
-- ----------------------------
DROP TABLE IF EXISTS `GatewayRequestMessage`;
CREATE TABLE `GatewayRequestMessage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SuccessURL` mediumtext CHARACTER SET utf8,
  `FailureURL` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `GlobalTaxModifier`
-- ----------------------------
DROP TABLE IF EXISTS `GlobalTaxModifier`;
CREATE TABLE `GlobalTaxModifier` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Country` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Group`
-- ----------------------------
DROP TABLE IF EXISTS `Group`;
CREATE TABLE `Group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Group') CHARACTER SET utf8 DEFAULT 'Group',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Description` mediumtext CHARACTER SET utf8,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HtmlEditorConfig` mediumtext CHARACTER SET utf8,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Group`
-- ----------------------------
BEGIN;
INSERT INTO `Group` VALUES ('1', 'Group', '2016-04-20 14:28:51', '2016-04-20 14:28:51', 'Content Authors', null, 'content-authors', '0', '1', null, '0'), ('2', 'Group', '2016-04-20 14:28:51', '2016-04-20 14:28:51', 'Administrators', null, 'administrators', '0', '0', null, '0'), ('3', 'Group', '2016-05-04 09:32:16', '2016-05-04 09:32:16', 'Forum Members', null, 'forum-members', '0', '0', null, '0'), ('4', 'Group', '2016-08-24 18:15:23', '2016-08-24 18:15:23', 'Blog users', null, 'blog-users', '0', '0', null, '0');
COMMIT;

-- ----------------------------
--  Table structure for `Group_Members`
-- ----------------------------
DROP TABLE IF EXISTS `Group_Members`;
CREATE TABLE `Group_Members` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Group_Members`
-- ----------------------------
BEGIN;
INSERT INTO `Group_Members` VALUES ('1', '2', '1');
COMMIT;

-- ----------------------------
--  Table structure for `Group_Roles`
-- ----------------------------
DROP TABLE IF EXISTS `Group_Roles`;
CREATE TABLE `Group_Roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `PermissionRoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `PermissionRoleID` (`PermissionRoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `HomePage`
-- ----------------------------
DROP TABLE IF EXISTS `HomePage`;
CREATE TABLE `HomePage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Block1Text` mediumtext CHARACTER SET utf8,
  `Block2Text` mediumtext CHARACTER SET utf8,
  `Block3Text` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `HomePage`
-- ----------------------------
BEGIN;
INSERT INTO `HomePage` VALUES ('1', '<h1>Producing the perfect coffee is a lifelong commitment. </h1><p>Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste. We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.  </p>', '<p>FANCY ONE OF THESE BAD BOYS<br>AND THE BEST COFFEE IN YOUR CAFE?<br><span>CALL NOW</span> ON <span>021 XXX XXX</span><br> OR EMAIL <a href=\"mailto:xxx@xxx.com\"><img class=\"email-us\" src=\"themes/santos/images/email-icon.svg\" width=\"106\" alt=\"\" title=\"\"></a></p>', '<p>With an amazing year-round view of the Auckland Domain, Santos Café in Carlton Gore Road, Newmarket is the perfect place to enjoy a true artisan roasting experience. Santos is a popular meeting place with plenty of seating and large tables, inside and out.</p>');
COMMIT;

-- ----------------------------
--  Table structure for `HomePage_Live`
-- ----------------------------
DROP TABLE IF EXISTS `HomePage_Live`;
CREATE TABLE `HomePage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Block1Text` mediumtext CHARACTER SET utf8,
  `Block2Text` mediumtext CHARACTER SET utf8,
  `Block3Text` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `HomePage_Live`
-- ----------------------------
BEGIN;
INSERT INTO `HomePage_Live` VALUES ('1', '<h1>Producing the perfect coffee is a lifelong commitment. </h1><p>Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste. We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.  </p>', '<p>FANCY ONE OF THESE BAD BOYS<br>AND THE BEST COFFEE IN YOUR CAFE?<br><span>CALL NOW</span> ON <span>021 XXX XXX</span><br> OR EMAIL <a href=\"mailto:xxx@xxx.com\"><img class=\"email-us\" src=\"themes/santos/images/email-icon.svg\" width=\"106\" alt=\"\" title=\"\"></a></p>', '<p>With an amazing year-round view of the Auckland Domain, Santos Café in Carlton Gore Road, Newmarket is the perfect place to enjoy a true artisan roasting experience. Santos is a popular meeting place with plenty of seating and large tables, inside and out.</p>');
COMMIT;

-- ----------------------------
--  Table structure for `HomePage_versions`
-- ----------------------------
DROP TABLE IF EXISTS `HomePage_versions`;
CREATE TABLE `HomePage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Block1Text` mediumtext CHARACTER SET utf8,
  `Block2Text` mediumtext CHARACTER SET utf8,
  `Block3Text` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `HomePage_versions`
-- ----------------------------
BEGIN;
INSERT INTO `HomePage_versions` VALUES ('1', '1', '6', '<h1>Producing the perfect coffee is a lifelong commitment. </h1><p>Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste. We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.  </p>', '<p>FANCY ONE OF THESE BAD BOYS<br>AND THE BEST COFFEE IN YOUR CAFE?<br><span>CALL NOW</span> ON <span>021 XXX XXX</span><br> OR EMAIL <a href=\"mailto:xxx@xxx.com\"><img class=\"email-us\" src=\"themes/santos/images/email-icon.svg\" width=\"106\" alt=\"\" title=\"\"></a></p>', '<p>With an amazing year-round view of the Auckland Domain, Santos Café in Carlton Gore Road, Newmarket is the perfect place to enjoy a true artisan roasting experience. Santos is a popular meeting place with plenty of seating and large tables, inside and out.</p>');
COMMIT;

-- ----------------------------
--  Table structure for `LoginAttempt`
-- ----------------------------
DROP TABLE IF EXISTS `LoginAttempt`;
CREATE TABLE `LoginAttempt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('LoginAttempt') CHARACTER SET utf8 DEFAULT 'LoginAttempt',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Status` enum('Success','Failure') CHARACTER SET utf8 DEFAULT 'Success',
  `IP` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `MemberID` (`MemberID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Member`
-- ----------------------------
DROP TABLE IF EXISTS `Member`;
CREATE TABLE `Member` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Member') CHARACTER SET utf8 DEFAULT 'Member',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `FirstName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Surname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `TempIDHash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `TempIDExpired` datetime DEFAULT NULL,
  `Password` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `RememberLoginToken` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `NumVisit` int(11) NOT NULL DEFAULT '0',
  `LastVisited` datetime DEFAULT NULL,
  `AutoLoginHash` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `AutoLoginExpired` datetime DEFAULT NULL,
  `PasswordEncryption` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Salt` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PasswordExpiry` date DEFAULT NULL,
  `LockedOutUntil` datetime DEFAULT NULL,
  `Locale` varchar(6) CHARACTER SET utf8 DEFAULT NULL,
  `FailedLoginCount` int(11) NOT NULL DEFAULT '0',
  `DateFormat` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `TimeFormat` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `ForumRank` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Occupation` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Company` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `City` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Country` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Nickname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `FirstNamePublic` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SurnamePublic` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `OccupationPublic` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CompanyPublic` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CityPublic` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CountryPublic` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EmailPublic` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `LastViewed` datetime DEFAULT NULL,
  `Signature` mediumtext CHARACTER SET utf8,
  `ForumStatus` enum('Normal','Banned','Ghost') CHARACTER SET utf8 DEFAULT 'Normal',
  `SuspendedUntil` date DEFAULT NULL,
  `AvatarID` int(11) NOT NULL DEFAULT '0',
  `URLSegment` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `BlogProfileSummary` mediumtext CHARACTER SET utf8,
  `BlogProfileImageID` int(11) NOT NULL DEFAULT '0',
  `DefaultShippingAddressID` int(11) NOT NULL DEFAULT '0',
  `DefaultBillingAddressID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Email` (`Email`),
  KEY `ClassName` (`ClassName`),
  KEY `AvatarID` (`AvatarID`),
  KEY `Nickname` (`Nickname`),
  KEY `BlogProfileImageID` (`BlogProfileImageID`),
  KEY `DefaultShippingAddressID` (`DefaultShippingAddressID`),
  KEY `DefaultBillingAddressID` (`DefaultBillingAddressID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Member`
-- ----------------------------
BEGIN;
INSERT INTO `Member` VALUES ('1', 'Member', '2016-09-02 10:23:47', '2016-04-20 14:28:51', 'Default Admin', null, 'admin', 'c2d12ab0cd75d6c84eacb16159efb65f0e51a8ef', '2016-09-05 10:23:47', null, null, '2', '2016-09-02 13:07:22', null, null, null, null, null, null, 'en_US', '0', null, null, null, null, null, null, null, null, '0', '0', '0', '0', '0', '0', '0', null, null, 'Normal', null, '0', 'default-admin', null, '0', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `MemberPassword`
-- ----------------------------
DROP TABLE IF EXISTS `MemberPassword`;
CREATE TABLE `MemberPassword` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('MemberPassword') CHARACTER SET utf8 DEFAULT 'MemberPassword',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Password` varchar(160) CHARACTER SET utf8 DEFAULT NULL,
  `Salt` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PasswordEncryption` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `MemberID` (`MemberID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Order`
-- ----------------------------
DROP TABLE IF EXISTS `Order`;
CREATE TABLE `Order` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Order') CHARACTER SET utf8 DEFAULT 'Order',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Total` decimal(9,2) NOT NULL DEFAULT '0.00',
  `Reference` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Placed` datetime DEFAULT NULL,
  `Paid` datetime DEFAULT NULL,
  `ReceiptSent` datetime DEFAULT NULL,
  `Printed` datetime DEFAULT NULL,
  `Dispatched` datetime DEFAULT NULL,
  `Status` enum('Unpaid','Paid','Processing','Refunded','Sent','Complete','AdminCancelled','MemberCancelled','Cart') CHARACTER SET utf8 DEFAULT 'Cart',
  `FirstName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Surname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Notes` mediumtext CHARACTER SET utf8,
  `IPAddress` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `SeparateBillingAddress` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Locale` varchar(16) CHARACTER SET utf8 DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  `ShippingAddressID` int(11) NOT NULL DEFAULT '0',
  `BillingAddressID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `MemberID` (`MemberID`),
  KEY `ShippingAddressID` (`ShippingAddressID`),
  KEY `BillingAddressID` (`BillingAddressID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Order`
-- ----------------------------
BEGIN;
INSERT INTO `Order` VALUES ('1', 'Order', '2016-09-01 14:17:47', '2016-09-01 14:17:47', '0.00', null, null, null, null, null, null, 'Cart', null, null, null, null, null, '0', 'en_NZ', '1', '0', '0'), ('2', 'Order', '2016-09-01 14:22:44', '2016-09-01 14:22:44', '0.00', null, null, null, null, null, null, 'Cart', null, null, null, null, null, '0', 'en_NZ', '1', '0', '0'), ('3', 'Order', '2016-09-01 14:27:06', '2016-09-01 14:27:06', '0.00', null, null, null, null, null, null, 'Cart', null, null, null, null, null, '0', 'en_NZ', '1', '0', '0'), ('4', 'Order', '2016-09-02 10:23:47', '2016-09-02 09:12:18', '65.20', null, null, null, null, null, null, 'Cart', null, null, null, null, null, '0', 'en_NZ', '1', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `OrderAttribute`
-- ----------------------------
DROP TABLE IF EXISTS `OrderAttribute`;
CREATE TABLE `OrderAttribute` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('OrderAttribute','OrderItem','Product_OrderItem','ProductVariation_OrderItem','OrderModifier','SubTotalModifier','ShippingModifier','FreeShippingModifier','PickupShippingModifier','SimpleShippingModifier','WeightShippingModifier','TaxModifier','FlatTaxModifier','GlobalTaxModifier') CHARACTER SET utf8 DEFAULT 'OrderAttribute',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `CalculatedTotal` decimal(9,2) NOT NULL DEFAULT '0.00',
  `OrderID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `OrderID` (`OrderID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `OrderAttribute`
-- ----------------------------
BEGIN;
INSERT INTO `OrderAttribute` VALUES ('1', 'FlatTaxModifier', '2016-09-02 09:11:18', '2016-09-01 14:27:06', '0.00', '3'), ('2', 'SimpleShippingModifier', '2016-09-02 09:11:18', '2016-09-01 14:27:06', '0.00', '3'), ('3', 'Product_OrderItem', '2016-09-01 14:27:06', '2016-09-01 14:27:06', '200.00', '3'), ('4', 'Product_OrderItem', '2016-09-01 21:29:19', '2016-09-01 21:29:19', '23.00', '3'), ('5', 'Product_OrderItem', '2016-09-02 09:11:18', '2016-09-02 09:11:18', '12.00', '3'), ('6', 'FlatTaxModifier', '2016-09-02 10:23:47', '2016-09-02 09:12:18', '0.00', '4'), ('7', 'SimpleShippingModifier', '2016-09-02 10:23:47', '2016-09-02 09:12:18', '0.00', '4'), ('8', 'Product_OrderItem', '2016-09-02 10:16:17', '2016-09-02 09:12:18', '48.00', '4');
COMMIT;

-- ----------------------------
--  Table structure for `OrderItem`
-- ----------------------------
DROP TABLE IF EXISTS `OrderItem`;
CREATE TABLE `OrderItem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Quantity` int(11) NOT NULL DEFAULT '0',
  `UnitPrice` decimal(9,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `OrderItem`
-- ----------------------------
BEGIN;
INSERT INTO `OrderItem` VALUES ('3', '1', '200.00'), ('4', '1', '23.00'), ('5', '1', '12.00'), ('8', '4', '12.00');
COMMIT;

-- ----------------------------
--  Table structure for `OrderModifier`
-- ----------------------------
DROP TABLE IF EXISTS `OrderModifier`;
CREATE TABLE `OrderModifier` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Amount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `Type` enum('Chargable','Deductable','Ignored') CHARACTER SET utf8 DEFAULT 'Chargable',
  `Sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `OrderModifier`
-- ----------------------------
BEGIN;
INSERT INTO `OrderModifier` VALUES ('1', '35.25', 'Chargable', '1'), ('2', '10.00', 'Chargable', '2'), ('6', '7.20', 'Chargable', '1'), ('7', '10.00', 'Chargable', '2');
COMMIT;

-- ----------------------------
--  Table structure for `OrderStatusLog`
-- ----------------------------
DROP TABLE IF EXISTS `OrderStatusLog`;
CREATE TABLE `OrderStatusLog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('OrderStatusLog') CHARACTER SET utf8 DEFAULT 'OrderStatusLog',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Note` mediumtext CHARACTER SET utf8,
  `DispatchedBy` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `DispatchedOn` date DEFAULT NULL,
  `DispatchTicket` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `PaymentCode` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `PaymentOK` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SentToCustomer` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `OrderID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `AuthorID` (`AuthorID`),
  KEY `OrderID` (`OrderID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Page`
-- ----------------------------
DROP TABLE IF EXISTS `Page`;
CREATE TABLE `Page` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HeaderText` mediumtext CHARACTER SET utf8,
  `BannerImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BannerImageID` (`BannerImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Page`
-- ----------------------------
BEGIN;
INSERT INTO `Page` VALUES ('1', 'Santos Coffee contains the soul of the city. Created by artisan roasters with a passion to supply our customers with the world’s finest coffees.', '2'), ('2', null, '18'), ('8', null, '0'), ('9', null, '0'), ('10', 'Someday the mountain might get ‘em but the law never will? Their house is a museum where people come to see ‘em. They really are a scream the Addams Family.', '26'), ('11', 'Come and knock on our door. We\'ve been waiting for you. Where the kisses are hers and hers and his. Three\'s company too.', '6'), ('12', null, '34'), ('13', null, '0'), ('14', null, '0'), ('15', null, '0'), ('16', null, '0'), ('17', null, '0'), ('18', null, '0'), ('19', null, '0'), ('20', null, '0'), ('21', null, '0'), ('22', null, '0'), ('23', null, '0'), ('24', null, '0'), ('25', null, '0'), ('26', null, '0'), ('61', null, '0'), ('62', null, '0'), ('63', null, '0'), ('64', null, '0'), ('65', null, '0'), ('66', null, '0');
COMMIT;

-- ----------------------------
--  Table structure for `Page_Live`
-- ----------------------------
DROP TABLE IF EXISTS `Page_Live`;
CREATE TABLE `Page_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HeaderText` mediumtext CHARACTER SET utf8,
  `BannerImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BannerImageID` (`BannerImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Page_Live`
-- ----------------------------
BEGIN;
INSERT INTO `Page_Live` VALUES ('1', 'Santos Coffee contains the soul of the city. Created by artisan roasters with a passion to supply our customers with the world’s finest coffees.', '2'), ('2', null, '18'), ('8', null, '0'), ('9', null, '0'), ('10', 'Someday the mountain might get ‘em but the law never will? Their house is a museum where people come to see ‘em. They really are a scream the Addams Family.', '26'), ('11', 'Come and knock on our door. We\'ve been waiting for you. Where the kisses are hers and hers and his. Three\'s company too.', '6'), ('12', null, '34'), ('13', null, '0'), ('14', null, '0'), ('15', null, '0'), ('16', null, '0'), ('17', null, '0'), ('18', null, '0'), ('19', null, '0'), ('20', null, '0'), ('21', null, '0'), ('22', null, '0'), ('23', null, '0'), ('24', null, '0'), ('25', null, '0'), ('26', null, '0'), ('61', null, '0'), ('62', null, '0'), ('63', null, '0'), ('64', null, '0'), ('65', null, '0'), ('66', null, '0');
COMMIT;

-- ----------------------------
--  Table structure for `Page_versions`
-- ----------------------------
DROP TABLE IF EXISTS `Page_versions`;
CREATE TABLE `Page_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `HeaderText` mediumtext CHARACTER SET utf8,
  `BannerImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `BannerImageID` (`BannerImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Page_versions`
-- ----------------------------
BEGIN;
INSERT INTO `Page_versions` VALUES ('1', '1', '4', 'Santos Coffee contains the soul of the city. Created by artisan roasters with a passion to supply our customers with the world’s finest coffees.', '0'), ('2', '1', '5', 'Santos Coffee contains the soul of the city. Created by artisan roasters with a passion to supply our customers with the world’s finest coffees.', '2'), ('3', '11', '3', null, '0'), ('4', '11', '4', 'Come and knock on our door. We\'ve been waiting for you. Where the kisses are hers and hers and his. Three\'s company too.', '4'), ('5', '11', '5', 'Come and knock on our door. We\'ve been waiting for you. Where the kisses are hers and hers and his. Three\'s company too.', '5'), ('6', '11', '6', 'Come and knock on our door. We\'ve been waiting for you. Where the kisses are hers and hers and his. Three\'s company too.', '6'), ('7', '11', '7', 'Come and knock on our door. We\'ve been waiting for you. Where the kisses are hers and hers and his. Three\'s company too.', '6'), ('8', '2', '3', null, '0'), ('9', '2', '4', null, '0'), ('10', '2', '5', null, '0'), ('11', '2', '6', null, '0'), ('12', '2', '7', null, '0'), ('13', '2', '8', 'And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Baby if you\'ve ever wondered - wondered whatever became of me.', '18'), ('14', '10', '3', null, '0'), ('15', '13', '1', null, '0'), ('16', '13', '2', null, '0'), ('17', '13', '3', null, '0'), ('18', '13', '4', null, '0'), ('19', '11', '8', 'Come and knock on our door. We\'ve been waiting for you. Where the kisses are hers and hers and his. Three\'s company too.', '6'), ('20', '11', '9', 'Come and knock on our door. We\'ve been waiting for you. Where the kisses are hers and hers and his. Three\'s company too.', '6'), ('21', '11', '10', 'Come and knock on our door. We\'ve been waiting for you. Where the kisses are hers and hers and his. Three\'s company too.', '6'), ('22', '14', '1', null, '0'), ('23', '14', '2', null, '0'), ('24', '14', '3', null, '0'), ('25', '15', '1', null, '0'), ('26', '15', '2', null, '0'), ('27', '15', '3', null, '0'), ('28', '15', '4', null, '0'), ('29', '16', '1', null, '0'), ('30', '16', '2', null, '0'), ('31', '13', '5', null, '0'), ('32', '13', '6', 'Someday the mountain might get ‘em but the law never will? Their house is a museum where people come to see ‘em. They really are a scream the Addams Family.', '25'), ('33', '13', '7', 'Someday the mountain might get ‘em but the law never will? Their house is a museum where people come to see ‘em. They really are a scream the Addams Family.', '0'), ('34', '10', '4', 'Someday the mountain might get ‘em but the law never will? Their house is a museum where people come to see ‘em. They really are a scream the Addams Family.', '26'), ('35', '1', '6', 'Santos Coffee contains the soul of the city. Created by artisan roasters with a passion to supply our customers with the world’s finest coffees.', '2'), ('36', '2', '9', 'And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Baby if you\'ve ever wondered - wondered whatever became of me.', '18'), ('37', '2', '10', 'And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Baby if you\'ve ever wondered - wondered whatever became of me.', '18'), ('38', '2', '11', 'And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Baby if you\'ve ever wondered - wondered whatever became of me.', '18'), ('39', '2', '12', 'And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Baby if you\'ve ever wondered - wondered whatever became of me.', '18'), ('40', '2', '13', 'And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Baby if you\'ve ever wondered - wondered whatever became of me.', '18'), ('41', '2', '14', 'And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Baby if you\'ve ever wondered - wondered whatever became of me.', '18'), ('42', '2', '15', 'And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Baby if you\'ve ever wondered - wondered whatever became of me.', '18'), ('43', '2', '16', null, '18'), ('44', '17', '1', null, '0'), ('45', '17', '2', null, '0'), ('46', '17', '3', null, '0'), ('47', '18', '1', null, '0'), ('48', '18', '2', null, '0'), ('49', '18', '3', null, '0'), ('50', '19', '1', null, '0'), ('51', '19', '2', null, '0'), ('52', '19', '3', null, '0'), ('53', '12', '3', null, '0'), ('54', '12', '4', null, '34'), ('55', '20', '1', null, '0'), ('56', '20', '2', null, '0'), ('57', '20', '3', null, '0'), ('58', '21', '1', null, '0'), ('59', '21', '2', null, '0'), ('60', '21', '3', null, '0'), ('61', '22', '1', null, '0'), ('62', '22', '2', null, '0'), ('63', '22', '3', null, '0'), ('64', '12', '5', null, '34'), ('65', '12', '6', null, '34'), ('66', '23', '1', null, '0'), ('67', '23', '2', null, '0'), ('68', '23', '3', null, '0'), ('69', '12', '7', null, '34'), ('70', '23', '4', null, '0'), ('71', '24', '1', null, '0'), ('72', '25', '1', null, '0'), ('73', '26', '1', null, '0'), ('74', '27', '1', null, '0'), ('75', '28', '1', null, '0'), ('76', '28', '2', null, '0'), ('77', '29', '1', null, '0'), ('78', '29', '2', null, '0'), ('79', '30', '1', null, '0'), ('80', '30', '2', null, '0'), ('81', '31', '1', null, '0'), ('82', '31', '2', null, '0'), ('83', '32', '1', null, '0'), ('84', '32', '2', null, '0'), ('85', '33', '1', null, '0'), ('86', '33', '2', null, '0'), ('87', '34', '1', null, '0'), ('88', '34', '2', null, '0'), ('89', '35', '1', null, '0'), ('90', '35', '2', null, '0'), ('91', '36', '1', null, '0'), ('92', '36', '2', null, '0'), ('93', '37', '1', null, '0'), ('94', '37', '2', null, '0'), ('95', '38', '1', null, '0'), ('96', '38', '2', null, '0'), ('97', '39', '1', null, '0'), ('98', '39', '2', null, '0'), ('99', '40', '1', null, '0'), ('100', '40', '2', null, '0'), ('101', '41', '1', null, '0'), ('102', '41', '2', null, '0'), ('103', '42', '1', null, '0'), ('104', '42', '2', null, '0'), ('105', '43', '1', null, '0'), ('106', '43', '2', null, '0'), ('107', '44', '1', null, '0'), ('108', '44', '2', null, '0'), ('109', '45', '1', null, '0'), ('110', '45', '2', null, '0'), ('111', '46', '1', null, '0'), ('112', '46', '2', null, '0'), ('113', '47', '1', null, '0'), ('114', '47', '2', null, '0'), ('115', '48', '1', null, '0'), ('116', '48', '2', null, '0'), ('117', '49', '1', null, '0'), ('118', '49', '2', null, '0'), ('119', '50', '1', null, '0'), ('120', '50', '2', null, '0'), ('121', '51', '1', null, '0'), ('122', '51', '2', null, '0'), ('123', '52', '1', null, '0'), ('124', '52', '2', null, '0'), ('125', '53', '1', null, '0'), ('126', '53', '2', null, '0'), ('127', '54', '1', null, '0'), ('128', '54', '2', null, '0'), ('129', '55', '1', null, '0'), ('130', '55', '2', null, '0'), ('131', '56', '1', null, '0'), ('132', '56', '2', null, '0'), ('133', '57', '1', null, '0'), ('134', '57', '2', null, '0'), ('135', '58', '1', null, '0'), ('136', '58', '2', null, '0'), ('137', '59', '1', null, '0'), ('138', '59', '2', null, '0'), ('139', '60', '1', null, '0'), ('140', '60', '2', null, '0'), ('141', '61', '1', null, '0'), ('142', '8', '3', null, '0'), ('143', '62', '1', null, '0'), ('144', '62', '2', null, '0'), ('145', '63', '1', null, '0'), ('146', '64', '1', null, '0'), ('147', '63', '2', null, '0'), ('148', '62', '3', null, '0'), ('149', '63', '3', null, '0'), ('150', '64', '2', null, '0'), ('151', '62', '4', null, '0'), ('152', '65', '1', null, '0'), ('153', '65', '2', null, '0'), ('154', '65', '3', null, '0'), ('155', '66', '1', null, '0'), ('156', '66', '2', null, '0'), ('157', '63', '4', null, '0'), ('158', '66', '3', null, '0'), ('159', '62', '5', null, '0'), ('160', '64', '3', null, '0'), ('161', '65', '4', null, '0'), ('162', '8', '4', null, '0'), ('163', '8', '5', null, '0'), ('164', '8', '6', null, '0'), ('165', '8', '7', null, '0'), ('166', '8', '8', null, '0'), ('167', '8', '9', null, '0'), ('168', '8', '10', null, '0'), ('169', '62', '6', null, '0'), ('170', '63', '5', null, '0'), ('171', '64', '4', null, '0'), ('172', '65', '5', null, '0'), ('173', '66', '4', null, '0'), ('174', '8', '11', null, '0'), ('175', '8', '12', null, '0'), ('176', '8', '13', null, '0'), ('177', '9', '3', null, '0'), ('178', '9', '4', null, '0'), ('179', '9', '5', null, '0'), ('180', '63', '6', null, '0'), ('181', '63', '7', null, '0'), ('182', '63', '8', null, '0'), ('183', '63', '9', null, '0'), ('184', '63', '10', null, '0'), ('185', '63', '11', null, '0'), ('186', '64', '5', null, '0'), ('187', '64', '6', null, '0'), ('188', '64', '7', null, '0'), ('189', '64', '8', null, '0'), ('190', '23', '5', null, '0'), ('191', '22', '4', null, '0'), ('192', '20', '4', null, '0'), ('193', '67', '1', null, '0'), ('194', '67', '2', null, '0'), ('195', '67', '3', null, '0'), ('196', '9', '6', null, '0'), ('197', '9', '7', null, '0'), ('198', '9', '8', null, '0'), ('199', '9', '9', null, '0'), ('200', '9', '10', null, '0'), ('201', '9', '11', null, '0'), ('202', '9', '12', null, '0'), ('203', '9', '13', null, '0'), ('204', '8', '14', null, '0');
COMMIT;

-- ----------------------------
--  Table structure for `Payment`
-- ----------------------------
DROP TABLE IF EXISTS `Payment`;
CREATE TABLE `Payment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Payment') CHARACTER SET utf8 DEFAULT 'Payment',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Gateway` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `Status` enum('Created','PendingAuthorization','Authorized','PendingPurchase','PendingCapture','Captured','PendingRefund','Refunded','PendingVoid','Void') CHARACTER SET utf8 DEFAULT 'Created',
  `Identifier` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `TransactionReference` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `SuccessUrl` mediumtext CHARACTER SET utf8,
  `FailureUrl` mediumtext CHARACTER SET utf8,
  `MoneyCurrency` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `MoneyAmount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `InitialPaymentID` int(11) NOT NULL DEFAULT '0',
  `OrderID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `InitialPaymentID` (`InitialPaymentID`),
  KEY `OrderID` (`OrderID`),
  KEY `Identifier` (`Identifier`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `PaymentMessage`
-- ----------------------------
DROP TABLE IF EXISTS `PaymentMessage`;
CREATE TABLE `PaymentMessage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('PaymentMessage','GatewayMessage','GatewayErrorMessage','AuthorizeError','CompleteAuthorizeError','CaptureError','NotificationError','PurchaseError','CompletePurchaseError','RefundError','VoidError','GatewayRedirectResponseMessage','AuthorizeRedirectResponse','PurchaseRedirectResponse','GatewayRequestMessage','AuthorizeRequest','CompleteAuthorizeRequest','CaptureRequest','PurchaseRequest','CompletePurchaseRequest','RefundRequest','VoidRequest','GatewayResponseMessage','AwaitingAuthorizeResponse','AuthorizedResponse','CapturedResponse','PartiallyCapturedResponse','NotificationSuccessful','NotificationPending','AwaitingPurchaseResponse','PurchasedResponse','RefundedResponse','PartiallyRefundedResponse','VoidedResponse') CHARACTER SET utf8 DEFAULT 'PaymentMessage',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Message` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ClientIp` varchar(39) CHARACTER SET utf8 DEFAULT NULL,
  `PaymentID` int(11) NOT NULL DEFAULT '0',
  `UserID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PaymentID` (`PaymentID`),
  KEY `UserID` (`UserID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Permission`
-- ----------------------------
DROP TABLE IF EXISTS `Permission`;
CREATE TABLE `Permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Permission') CHARACTER SET utf8 DEFAULT 'Permission',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Arg` int(11) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL DEFAULT '1',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `Code` (`Code`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Permission`
-- ----------------------------
BEGIN;
INSERT INTO `Permission` VALUES ('1', 'Permission', '2016-04-20 14:28:51', '2016-04-20 14:28:51', 'CMS_ACCESS_CMSMain', '0', '1', '1'), ('2', 'Permission', '2016-04-20 14:28:51', '2016-04-20 14:28:51', 'CMS_ACCESS_AssetAdmin', '0', '1', '1'), ('3', 'Permission', '2016-04-20 14:28:51', '2016-04-20 14:28:51', 'CMS_ACCESS_ReportAdmin', '0', '1', '1'), ('4', 'Permission', '2016-04-20 14:28:51', '2016-04-20 14:28:51', 'SITETREE_REORGANISE', '0', '1', '1'), ('5', 'Permission', '2016-04-20 14:28:51', '2016-04-20 14:28:51', 'ADMIN', '0', '1', '2'), ('6', 'Permission', '2016-05-04 09:32:16', '2016-05-04 09:32:16', 'ACCESS_FORUM', '0', '1', '3'), ('7', 'Permission', '2016-08-24 18:15:23', '2016-08-24 18:15:23', 'CMS_ACCESS_CMSMain', '0', '1', '4');
COMMIT;

-- ----------------------------
--  Table structure for `PermissionRole`
-- ----------------------------
DROP TABLE IF EXISTS `PermissionRole`;
CREATE TABLE `PermissionRole` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('PermissionRole') CHARACTER SET utf8 DEFAULT 'PermissionRole',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `OnlyAdminCanApply` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `PermissionRoleCode`
-- ----------------------------
DROP TABLE IF EXISTS `PermissionRoleCode`;
CREATE TABLE `PermissionRoleCode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('PermissionRoleCode') CHARACTER SET utf8 DEFAULT 'PermissionRoleCode',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Code` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `RoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RoleID` (`RoleID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Photo`
-- ----------------------------
DROP TABLE IF EXISTS `Photo`;
CREATE TABLE `Photo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Photo') CHARACTER SET utf8 DEFAULT 'Photo',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `SortOrder` int(11) NOT NULL DEFAULT '0',
  `PageID` int(11) NOT NULL DEFAULT '0',
  `PhotoImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PageID` (`PageID`),
  KEY `PhotoImageID` (`PhotoImageID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Photo`
-- ----------------------------
BEGIN;
INSERT INTO `Photo` VALUES ('1', 'Photo', '2016-08-29 14:54:18', '2016-08-29 14:54:14', '1', '11', '8'), ('3', 'Photo', '2016-08-29 14:54:44', '2016-08-29 14:54:38', '3', '11', '10'), ('4', 'Photo', '2016-08-29 14:55:03', '2016-08-29 14:55:01', '4', '11', '11'), ('5', 'Photo', '2016-08-29 14:55:31', '2016-08-29 14:55:29', '5', '11', '12'), ('6', 'Photo', '2016-08-29 14:55:43', '2016-08-29 14:55:41', '6', '11', '13'), ('7', 'Photo', '2016-08-29 14:55:58', '2016-08-29 14:55:56', '7', '11', '14'), ('8', 'Photo', '2016-08-29 14:56:12', '2016-08-29 14:56:10', '8', '11', '15'), ('9', 'Photo', '2016-08-29 14:56:26', '2016-08-29 14:56:23', '9', '11', '16'), ('10', 'Photo', '2016-08-29 15:01:19', '2016-08-29 14:56:56', '10', '11', '17');
COMMIT;

-- ----------------------------
--  Table structure for `Post`
-- ----------------------------
DROP TABLE IF EXISTS `Post`;
CREATE TABLE `Post` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Post') CHARACTER SET utf8 DEFAULT 'Post',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `Status` enum('Awaiting','Moderated','Rejected','Archived') CHARACTER SET utf8 DEFAULT 'Moderated',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `ThreadID` int(11) NOT NULL DEFAULT '0',
  `ForumID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `AuthorID` (`AuthorID`),
  KEY `ThreadID` (`ThreadID`),
  KEY `ForumID` (`ForumID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Post_Attachment`
-- ----------------------------
DROP TABLE IF EXISTS `Post_Attachment`;
CREATE TABLE `Post_Attachment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PostID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `PostID` (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Product`
-- ----------------------------
DROP TABLE IF EXISTS `Product`;
CREATE TABLE `Product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `InternalItemID` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `Model` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `CostPrice` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `BasePrice` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `Weight` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Height` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Width` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Depth` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AllowPurchase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Popularity` float NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `BGColour` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  KEY `ImageID` (`ImageID`),
  KEY `Featured` (`Featured`),
  KEY `AllowPurchase` (`AllowPurchase`),
  KEY `InternalItemID` (`InternalItemID`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Product`
-- ----------------------------
BEGIN;
INSERT INTO `Product` VALUES ('62', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '38', 'santos-blue'), ('63', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '39', 'santos-orange'), ('64', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '43', 'santos-white'), ('65', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '40', 'santos-green'), ('66', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '41', 'santos-darkblue');
COMMIT;

-- ----------------------------
--  Table structure for `ProductAttributeType`
-- ----------------------------
DROP TABLE IF EXISTS `ProductAttributeType`;
CREATE TABLE `ProductAttributeType` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('ProductAttributeType') CHARACTER SET utf8 DEFAULT 'ProductAttributeType',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Label` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LastEdited` (`LastEdited`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ProductAttributeType`
-- ----------------------------
BEGIN;
INSERT INTO `ProductAttributeType` VALUES ('1', 'ProductAttributeType', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'Shirt Size', 'Size'), ('2', 'ProductAttributeType', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'Shirt Colour', 'Colour');
COMMIT;

-- ----------------------------
--  Table structure for `ProductAttributeValue`
-- ----------------------------
DROP TABLE IF EXISTS `ProductAttributeValue`;
CREATE TABLE `ProductAttributeValue` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('ProductAttributeValue') CHARACTER SET utf8 DEFAULT 'ProductAttributeValue',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Value` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `TypeID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `TypeID` (`TypeID`),
  KEY `LastEdited` (`LastEdited`),
  KEY `Sort` (`Sort`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ProductAttributeValue`
-- ----------------------------
BEGIN;
INSERT INTO `ProductAttributeValue` VALUES ('1', 'ProductAttributeValue', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'Large', '0', '1'), ('2', 'ProductAttributeValue', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'Medium', '0', '1'), ('3', 'ProductAttributeValue', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'Small', '0', '1'), ('4', 'ProductAttributeValue', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'Red', '0', '2'), ('5', 'ProductAttributeValue', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'Blue', '0', '2');
COMMIT;

-- ----------------------------
--  Table structure for `ProductCategory`
-- ----------------------------
DROP TABLE IF EXISTS `ProductCategory`;
CREATE TABLE `ProductCategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Content2` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ProductCategory`
-- ----------------------------
BEGIN;
INSERT INTO `ProductCategory` VALUES ('8', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term ‘specialty coffee’ refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>');
COMMIT;

-- ----------------------------
--  Table structure for `ProductCategory_Live`
-- ----------------------------
DROP TABLE IF EXISTS `ProductCategory_Live`;
CREATE TABLE `ProductCategory_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Content2` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ProductCategory_Live`
-- ----------------------------
BEGIN;
INSERT INTO `ProductCategory_Live` VALUES ('8', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term ‘specialty coffee’ refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>');
COMMIT;

-- ----------------------------
--  Table structure for `ProductCategory_versions`
-- ----------------------------
DROP TABLE IF EXISTS `ProductCategory_versions`;
CREATE TABLE `ProductCategory_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Content2` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ProductCategory_versions`
-- ----------------------------
BEGIN;
INSERT INTO `ProductCategory_versions` VALUES ('1', '8', '4', null), ('2', '8', '5', null), ('3', '8', '6', null), ('4', '8', '7', null), ('5', '8', '8', null), ('6', '8', '9', null), ('7', '8', '10', null), ('8', '8', '11', '<p>Single origins</p><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><p>Specialty Coffees</p><p>The term ‘specialty coffee’ refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>'), ('9', '8', '12', '<h1>Single origins</h1><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h1>Specialty Coffees</h1><p>The term ‘specialty coffee’ refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>'), ('10', '8', '13', '<h2>Single origins</h2><p>We have a range of single origins available to purchase in the café. When you visit our store you can learn everything you’d like to know, from the farm to your cup.</p><h2>Specialty Coffees</h2><p>The term ‘specialty coffee’ refers to the highest-quality green coffee beans, roasted to their greatest flavour potential by true craftspeople and then properly brewed to well-established standards. Specialty coffee in the green bean state can be defined as a coffee that has no defects and has a distinctive character in the cup, with a score of 80 or above when graded.</p><p>Our speciality range of coffees are produced in micro lots and are not always available. <a href=\"[sitetree_link,id=1]\">Join our email list</a> and we’ll let you know about our special treats each month.</p>');
COMMIT;

-- ----------------------------
--  Table structure for `ProductVariation`
-- ----------------------------
DROP TABLE IF EXISTS `ProductVariation`;
CREATE TABLE `ProductVariation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('ProductVariation') CHARACTER SET utf8 DEFAULT 'ProductVariation',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `InternalItemID` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `Price` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `Weight` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Height` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Width` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Depth` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ProductID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ProductID` (`ProductID`),
  KEY `ImageID` (`ImageID`),
  KEY `InternalItemID` (`InternalItemID`),
  KEY `LastEdited` (`LastEdited`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `ProductVariation_AttributeValues`
-- ----------------------------
DROP TABLE IF EXISTS `ProductVariation_AttributeValues`;
CREATE TABLE `ProductVariation_AttributeValues` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductVariationID` int(11) NOT NULL DEFAULT '0',
  `ProductAttributeValueID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ProductVariationID` (`ProductVariationID`),
  KEY `ProductAttributeValueID` (`ProductAttributeValueID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ProductVariation_AttributeValues`
-- ----------------------------
BEGIN;
INSERT INTO `ProductVariation_AttributeValues` VALUES ('1', '1', '1'), ('2', '1', '4'), ('3', '2', '2'), ('4', '2', '5');
COMMIT;

-- ----------------------------
--  Table structure for `ProductVariation_OrderItem`
-- ----------------------------
DROP TABLE IF EXISTS `ProductVariation_OrderItem`;
CREATE TABLE `ProductVariation_OrderItem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductVariationVersion` int(11) NOT NULL DEFAULT '0',
  `ProductVariationID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ProductVariationID` (`ProductVariationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `ProductVariation_versions`
-- ----------------------------
DROP TABLE IF EXISTS `ProductVariation_versions`;
CREATE TABLE `ProductVariation_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('ProductVariation') CHARACTER SET utf8 DEFAULT 'ProductVariation',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `InternalItemID` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `Price` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `Weight` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Height` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Width` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Depth` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `ProductID` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ProductID` (`ProductID`),
  KEY `ImageID` (`ImageID`),
  KEY `InternalItemID` (`InternalItemID`),
  KEY `LastEdited` (`LastEdited`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `ProductVariation_versions`
-- ----------------------------
BEGIN;
INSERT INTO `ProductVariation_versions` VALUES ('1', '1', '1', '0', '1', '0', 'ProductVariation', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'abc128-lr', '25.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '0'), ('2', '1', '2', '0', '1', '0', 'ProductVariation', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'abc128-lr', '25.0000', '0.00000', '0.00000', '0.00000', '0.00000', '60', '0'), ('3', '2', '1', '0', '1', '0', 'ProductVariation', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'abc128-mb', '25.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '0'), ('4', '2', '2', '0', '1', '0', 'ProductVariation', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'abc128-mb', '25.0000', '0.00000', '0.00000', '0.00000', '0.00000', '60', '0');
COMMIT;

-- ----------------------------
--  Table structure for `Product_Live`
-- ----------------------------
DROP TABLE IF EXISTS `Product_Live`;
CREATE TABLE `Product_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `InternalItemID` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `Model` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `CostPrice` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `BasePrice` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `Weight` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Height` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Width` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Depth` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AllowPurchase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Popularity` float NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `BGColour` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  KEY `ImageID` (`ImageID`),
  KEY `Featured` (`Featured`),
  KEY `AllowPurchase` (`AllowPurchase`),
  KEY `InternalItemID` (`InternalItemID`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Product_Live`
-- ----------------------------
BEGIN;
INSERT INTO `Product_Live` VALUES ('62', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '38', 'santos-blue'), ('63', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '39', 'santos-orange'), ('64', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '43', 'santos-white'), ('65', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '40', 'santos-green'), ('66', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '41', 'santos-darkblue');
COMMIT;

-- ----------------------------
--  Table structure for `Product_OrderItem`
-- ----------------------------
DROP TABLE IF EXISTS `Product_OrderItem`;
CREATE TABLE `Product_OrderItem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductVersion` int(11) NOT NULL DEFAULT '0',
  `ProductID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ProductID` (`ProductID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Product_OrderItem`
-- ----------------------------
BEGIN;
INSERT INTO `Product_OrderItem` VALUES ('3', '0', '50'), ('4', '0', '51'), ('5', '0', '65'), ('8', '0', '63');
COMMIT;

-- ----------------------------
--  Table structure for `Product_ProductCategories`
-- ----------------------------
DROP TABLE IF EXISTS `Product_ProductCategories`;
CREATE TABLE `Product_ProductCategories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductID` int(11) NOT NULL DEFAULT '0',
  `ProductCategoryID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ProductID` (`ProductID`),
  KEY `ProductCategoryID` (`ProductCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Product_VariationAttributeTypes`
-- ----------------------------
DROP TABLE IF EXISTS `Product_VariationAttributeTypes`;
CREATE TABLE `Product_VariationAttributeTypes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductID` int(11) NOT NULL DEFAULT '0',
  `ProductAttributeTypeID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ProductID` (`ProductID`),
  KEY `ProductAttributeTypeID` (`ProductAttributeTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Product_VariationAttributeTypes`
-- ----------------------------
BEGIN;
INSERT INTO `Product_VariationAttributeTypes` VALUES ('1', '60', '1'), ('2', '60', '2');
COMMIT;

-- ----------------------------
--  Table structure for `Product_versions`
-- ----------------------------
DROP TABLE IF EXISTS `Product_versions`;
CREATE TABLE `Product_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `InternalItemID` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `Model` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `CostPrice` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `BasePrice` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `Weight` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Height` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Width` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Depth` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `Featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AllowPurchase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Popularity` float NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `BGColour` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `ImageID` (`ImageID`),
  KEY `Featured` (`Featured`),
  KEY `AllowPurchase` (`AllowPurchase`),
  KEY `InternalItemID` (`InternalItemID`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Product_versions`
-- ----------------------------
BEGIN;
INSERT INTO `Product_versions` VALUES ('1', '47', '1', 'abc136', null, '0.0000', '8.0000', '0.10000', '20.00000', '15.00000', '2.00000', '0', '1', '0', '0', null), ('2', '47', '2', 'abc136', null, '0.0000', '8.0000', '0.10000', '20.00000', '15.00000', '2.00000', '0', '1', '0', '0', null), ('3', '48', '1', null, null, '0.0000', '0.0000', '45.00000', '85.00000', '115.50000', '12.40000', '0', '1', '0', '0', null), ('4', '48', '2', null, null, '0.0000', '0.0000', '45.00000', '85.00000', '115.50000', '12.40000', '0', '1', '0', '0', null), ('5', '49', '1', 'abc130', null, '0.0000', '10.0000', '0.40000', '30.00000', '23.00000', '1.00000', '0', '0', '0', '0', null), ('6', '49', '2', 'abc130', null, '0.0000', '10.0000', '0.40000', '30.00000', '23.00000', '1.00000', '0', '0', '0', '0', null), ('7', '50', '1', null, null, '0.0000', '200.0000', '0.30000', '14.00000', '10.00000', '10.00000', '0', '1', '0', '0', null), ('8', '50', '2', null, null, '0.0000', '200.0000', '0.30000', '14.00000', '10.00000', '10.00000', '0', '1', '0', '0', null), ('9', '51', '1', 'abc137', null, '0.0000', '23.0000', '0.10000', '19.00000', '13.50000', '1.50000', '0', '1', '0', '0', null), ('10', '51', '2', 'abc137', null, '0.0000', '23.0000', '0.10000', '19.00000', '13.50000', '1.50000', '0', '1', '0', '0', null), ('11', '52', '1', 'abc140', null, '0.0000', '16.9900', '0.25000', '14.00000', '23.00000', '6.00000', '0', '1', '0', '0', null), ('12', '52', '2', 'abc140', null, '0.0000', '16.9900', '0.25000', '14.00000', '23.00000', '6.00000', '0', '1', '0', '0', null), ('13', '53', '1', 'abc126', null, '0.0000', '8.9900', '0.60000', '20.00000', '15.00000', '3.00000', '0', '1', '0', '0', null), ('14', '53', '2', 'abc126', null, '0.0000', '8.9900', '0.60000', '20.00000', '15.00000', '3.00000', '0', '1', '0', '0', null), ('15', '54', '1', 'abc125', null, '0.0000', '32.9900', '4.30000', '30.00000', '30.00000', '50.00000', '0', '1', '0', '0', null), ('16', '54', '2', 'abc125', null, '0.0000', '32.9900', '4.30000', '30.00000', '30.00000', '50.00000', '0', '1', '0', '0', null), ('17', '55', '1', 'abc135', null, '0.0000', '1236.3000', '0.01000', '10.00000', '10.00000', '6.00000', '0', '1', '0', '0', null), ('18', '55', '2', 'abc135', null, '0.0000', '1236.3000', '0.01000', '10.00000', '10.00000', '6.00000', '0', '1', '0', '0', null), ('19', '56', '1', 'abc134', null, '0.0000', '20.0000', '0.03000', '20.00000', '15.00000', '2.00000', '0', '1', '0', '0', null), ('20', '56', '2', 'abc134', null, '0.0000', '20.0000', '0.03000', '20.00000', '15.00000', '2.00000', '0', '1', '0', '0', null), ('21', '57', '1', 'abc131', null, '0.0000', '4.0000', '0.00000', '150.00000', '150.00000', '2.00000', '0', '1', '0', '0', null), ('22', '57', '2', 'abc131', null, '0.0000', '4.0000', '0.00000', '150.00000', '150.00000', '2.00000', '0', '1', '0', '0', null), ('23', '58', '1', 'abc132', null, '0.0000', '12.0000', '1.40000', '7.00000', '7.00000', '120.00000', '0', '1', '0', '0', null), ('24', '58', '2', 'abc132', null, '0.0000', '12.0000', '1.40000', '7.00000', '7.00000', '120.00000', '0', '1', '0', '0', null), ('25', '59', '1', 'abc123', null, '0.0000', '12.0000', '0.10000', '10.00000', '5.00000', '3.00000', '0', '1', '0', '0', null), ('26', '59', '2', 'abc123', null, '0.0000', '12.0000', '0.10000', '10.00000', '5.00000', '3.00000', '0', '1', '0', '0', null), ('27', '60', '1', 'abc128', null, '0.0000', '25.0000', '0.30000', '28.00000', '18.50000', '2.00000', '0', '1', '0', '0', null), ('28', '60', '2', 'abc128', null, '0.0000', '25.0000', '0.30000', '28.00000', '18.50000', '2.00000', '0', '1', '0', '0', null), ('29', '62', '1', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '0', null), ('30', '62', '2', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '0', null), ('31', '63', '1', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '0', null), ('32', '64', '1', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '0', null), ('33', '63', '2', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '0', null), ('34', '62', '3', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '38', null), ('35', '63', '3', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '39', null), ('36', '64', '2', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '0', null), ('37', '62', '4', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '38', null), ('38', '65', '1', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '0', null), ('39', '65', '2', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '0', null), ('40', '65', '3', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '40', null), ('41', '66', '1', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '0', null), ('42', '66', '2', null, null, '0.0000', '0.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '41', null), ('43', '63', '4', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '39', null), ('44', '66', '3', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '41', null), ('45', '62', '5', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '38', null), ('46', '64', '3', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '0', null), ('47', '65', '4', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '40', null), ('48', '62', '6', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '38', 'santos-blue'), ('49', '63', '5', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '39', 'santos-orange'), ('50', '64', '4', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '1', '0', '0', 'santos-white'), ('51', '65', '5', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '40', 'santos-green'), ('52', '66', '4', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '41', 'santos-darkblue'), ('53', '63', '6', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '39', 'santos-orange'), ('54', '63', '7', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '39', 'santos-orange'), ('55', '63', '8', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '39', 'santos-orange'), ('56', '63', '9', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '39', 'santos-orange'), ('57', '63', '10', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '0', '0', '39', 'santos-orange'), ('58', '63', '11', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '39', 'santos-orange'), ('59', '64', '5', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '0', '0', '0', '0', 'santos-white'), ('60', '64', '6', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '0', 'santos-white'), ('61', '64', '7', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '43', 'santos-white'), ('62', '64', '8', null, null, '0.0000', '12.0000', '0.00000', '0.00000', '0.00000', '0.00000', '1', '1', '0', '43', 'santos-white');
COMMIT;

-- ----------------------------
--  Table structure for `RedirectorPage`
-- ----------------------------
DROP TABLE IF EXISTS `RedirectorPage`;
CREATE TABLE `RedirectorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `RedirectorPage_Live`
-- ----------------------------
DROP TABLE IF EXISTS `RedirectorPage_Live`;
CREATE TABLE `RedirectorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `RedirectorPage_versions`
-- ----------------------------
DROP TABLE IF EXISTS `RedirectorPage_versions`;
CREATE TABLE `RedirectorPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `RedirectionType` enum('Internal','External') CHARACTER SET utf8 DEFAULT 'Internal',
  `ExternalURL` varchar(2083) CHARACTER SET utf8 DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `RegionRestriction`
-- ----------------------------
DROP TABLE IF EXISTS `RegionRestriction`;
CREATE TABLE `RegionRestriction` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('RegionRestriction','ZoneRegion') CHARACTER SET utf8 DEFAULT 'RegionRestriction',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Country` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `State` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `City` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PostalCode` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `SimpleShippingModifier`
-- ----------------------------
DROP TABLE IF EXISTS `SimpleShippingModifier`;
CREATE TABLE `SimpleShippingModifier` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Country` mediumtext CHARACTER SET utf8,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `SimpleShippingModifier`
-- ----------------------------
BEGIN;
INSERT INTO `SimpleShippingModifier` VALUES ('2', null), ('7', null);
COMMIT;

-- ----------------------------
--  Table structure for `SiteConfig`
-- ----------------------------
DROP TABLE IF EXISTS `SiteConfig`;
CREATE TABLE `SiteConfig` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SiteConfig') CHARACTER SET utf8 DEFAULT 'SiteConfig',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Tagline` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Theme` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'Anyone',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  `CanCreateTopLevelType` enum('LoggedInUsers','OnlyTheseUsers') CHARACTER SET utf8 DEFAULT 'LoggedInUsers',
  `AllowedCountries` mediumtext CHARACTER SET utf8,
  `TermsPageID` int(11) NOT NULL DEFAULT '0',
  `CustomerGroupID` int(11) NOT NULL DEFAULT '0',
  `DefaultProductImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`),
  KEY `TermsPageID` (`TermsPageID`),
  KEY `CustomerGroupID` (`CustomerGroupID`),
  KEY `DefaultProductImageID` (`DefaultProductImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `SiteConfig`
-- ----------------------------
BEGIN;
INSERT INTO `SiteConfig` VALUES ('1', 'SiteConfig', '2016-09-02 08:44:11', '2016-04-20 14:28:52', 'Santos', 'Coffee with Soul', null, 'Anyone', 'LoggedInUsers', 'LoggedInUsers', 'NZ', '61', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `SiteConfig_CreateTopLevelGroups`
-- ----------------------------
DROP TABLE IF EXISTS `SiteConfig_CreateTopLevelGroups`;
CREATE TABLE `SiteConfig_CreateTopLevelGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `SiteConfig_EditorGroups`
-- ----------------------------
DROP TABLE IF EXISTS `SiteConfig_EditorGroups`;
CREATE TABLE `SiteConfig_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `SiteConfig_ViewerGroups`
-- ----------------------------
DROP TABLE IF EXISTS `SiteConfig_ViewerGroups`;
CREATE TABLE `SiteConfig_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `SiteTree`
-- ----------------------------
DROP TABLE IF EXISTS `SiteTree`;
CREATE TABLE `SiteTree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SiteTree','Page','CafePage','CoffeePage','HomePage','StaffHolder','StaffProfile','Blog','BlogPost','BlogEntry','ErrorPage','RedirectorPage','VirtualPage','AccountPage','CartPage','CheckoutPage','Product','ProductCategory','BlogTree','BlogHolder') CHARACTER SET utf8 DEFAULT 'SiteTree',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Priority` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `SiteTree`
-- ----------------------------
BEGIN;
INSERT INTO `SiteTree` VALUES ('1', 'HomePage', '2016-08-30 10:27:56', '2016-04-20 14:28:51', 'home', 'Home', null, null, null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '6', '0'), ('2', 'Page', '2016-08-30 11:55:32', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">The Wyatt name is synonymous with coffee and tea in New Zealand, dating back to when the Wyatt family purchased Wilcox Tea in 1966.</p><p>We were very English back then, with everyone drinking loose leaf tea which arrived in the country in large tea chests. Coffee was a small side-business at the time, but as the years progressed it became a bigger focus.</p><p>Lance Wyatt saw a need to start a fresh roasted coffee movement here but import duties made it cost prohibitive to roast coffee in the early 80s. So Lance built his own coffee roasting machine in 1983, effectively becoming one of New Zealand’s coffee roasting pioneer’s.</p><p>Aaron grew up in the industry, working for his dad during school holidays, until he joined full-time in 1999 as a coffee machine technician.</p><p>Santos Coffee was founded in 1995 by Brazilian native, Marcelo de Souza. Marcelo started the roastery in Mt. Eden, before opening the iconic Santos Café in Ponsonby, where it was part of the scenery for many years. The roastery was moved to the top of New North Road in Kingsland, until selling Santos to the Wyatts in 2012.</p><p>Santos Coffee continued to be roasted from its Kingsland location until Lance and Aaron found a more public-friendly location in Carlton Gore Road, Newmarket. This was the first time Santos had a building large enough to house the roaster, an espresso bar and café. The idea was, if you weren’t admiring the amazing view of Auckland domain whilst enjoying a great brew, you could melt into the coffee roasting process.</p><p>The doors of our new location opened to the public in April 2015. From here we continue to build on our urban, city coffee brand for coffee aficionados. You can enjoy Santos Coffee at our own and other high-quality cafes around the country.</p><h1>Service</h1><hr><p class=\"lead\">What we offer is proven, premium and Speciality grade award winning coffees.</p><p>As artisans we know coffee inside-out, and we know what works for different requirements. We offer our cafē and corporate customers the full package: supply, service and support. This ensures you can concentrate on serving great coffee at your cafē or workplace without any of the associated headaches. We serve our customers best by having great products and equipment delivered with awesome service from well-trained people. All of our staff are trained baristas, even our receptionist and warehouse manager. That’s because we all wear multiple hats here. Our team knows every aspect of the coffee business. If someone’s unavailable, someone else has you covered. They can answer any questions you have and make you a great coffee to top it off.</p><p>Our after sales service is unbelievably good too. It’s really only when something goes wrong that you understand the value of your coffee roaster and what they offer you. should your espresso machine break down, we react within an hour to fix or replace it. It’s this kind of service from a large behind the scenes team that will make all the difference to your business. We can tailor how you deal with us, with either automatic weekly or daily deliveries, or email or phone us with your orders.</p><p>We’re confident you’ll love Santos Coffee, which is why we won\'t tie you down to a contract. At Santos Coffee we blend everything together to provide you the best coffee experience of your life.</p><p>Ask us about:</p><ul><li><em>Santos branded merchandise</em> – signs, shirts, aprons, umbrellas</li>\n<li><em>Sanremo espresso machines</em> – we think they’re the best espresso machine available</li>\n<li><em>Barista training</em></li>\n<li><em>Our easy ordering system</em> – a one-stop-shop for coffee and consumables</li>\n<li><em>Technical service</em></li>\n<li><em>The best coffees money can buy</em>.</li>\n</ul><h1>Machines</h1><hr><p class=\"lead\">We’ve seen and used a lot of different coffee machines over the decades.</p><p>We’re proud to use, recommend and represent Sanremo espresso machines and grinders, manufactured in Treviso, Italy. Like us, Sanremo is a family-owned business, with a factory producing outstanding machines. They have a tightknit team who like us are focused on excellence.</p><p>Aesthetically, Sanremo machines are not only beautiful, but they’re also extremely reliable and easy to use. We have models to suit every size environment, from the small office to the large corporate workplace and busy cafē.</p><p>If you want reliability, beauty and perfection, choose Sanremo.</p><p class=\"lead\"><img class=\"img-responsive\" style=\"margin-left: -30px;\" title=\"\" src=\"assets/Uploads/sanremo.jpg\" alt=\"sanremo\"></p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '16', '0'), ('4', 'ErrorPage', '2016-08-24 18:17:23', '2016-04-20 14:28:52', 'page-not-found', 'Page not found', null, '<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', null, null, '0', '0', '10', '0', '0', null, 'Inherit', 'Inherit', null, '2', '0'), ('5', 'ErrorPage', '2016-08-24 18:17:26', '2016-04-20 14:28:52', 'server-error', 'Server error', null, '<p>Sorry, there was a problem with handling your request.</p>', null, null, '0', '0', '11', '0', '0', null, 'Inherit', 'Inherit', null, '2', '0'), ('8', 'ProductCategory', '2016-09-02 13:06:12', '2016-08-24 18:14:27', 'shop', 'Shop', null, null, null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '14', '0'), ('9', 'CoffeePage', '2016-09-02 13:04:30', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p><h1>Awards</h1><hr><p class=\"lead\">Here\'s a few awards we\'ve won</p><p><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/awards.png\" alt=\"awards\"></p><h1>Certified</h1><hr><p class=\"lead\">We help reduce the environmental impact of bad agriculture practice by choosing beans from sustainable farms.</p><p>Santos Coffee is dedicated to helping people preserve the environment, which is why traceability and sustainability in sourcing coffee beans is very high on our agenda.</p><p>The Rainforest Alliance is an international non-profit organisation that works to conserve biodiversity and promote the rights and well-being of workers, their families and communities. Farms that meet comprehensive standards for sustainability earn the Rainforest Alliance Certified™ seal. These standards conserve biodiversity, ensure that soils, waterways and wildlife habitat is protected and that farm workers enjoy decent housing, access to medical care and schools for their children.</p><p>Farms must commit to a process of continuous improvement and are audited each year in order to maintain their certification. By shopping for products bearing the Rainforest Alliance Certified seal, you can support a healthy environment and help to improve the quality of life for farm families. To learn more about the Rainforest Alliance, visit www.rainforest-alliance.org.</p><p><img title=\"\" src=\"assets/Uploads/certified-green.svg\" alt=\"\" width=\"144\"></p><h1>Blends</h1><hr><p class=\"lead\">It’s a fine art creating quality blends, because a blend is greater than the sum of its parts. When creating a blend, our goal is to achieve a balanced, distinct flavour profile that can then be consistently reproduced.</p><p>Beans are chosen for their ability to complement each other, based on flavour, aroma and body. For this reason, blends often provide a more complex character when compared to varietals or single origin beans.</p><p>A lot of factors goes into the blending process to ensure strict quality control. This includes a lot of coffee cupping, or tasting, to observe the tastes and aromas of brewed coffee. It’s here that we identify the flavour profiles, much like a chef does with food. We then experiment until we have the ideal blend.</p><p>One way were able to produce consistent blends is in the way we roast and blend. There are two ways blending occurs. The first is mix all beans together and roast. The second, our way, is to roast the beans separately, then blend them. This allows us to roast each bean exactly how we want to, and the way that we know will produce optimal results for that bean.</p><p> </p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '13', '0'), ('10', 'StaffHolder', '2016-08-29 18:51:46', '2016-08-24 18:14:55', 'team', 'Team', null, null, null, null, '1', '1', '6', '0', '0', null, 'Inherit', 'Inherit', null, '4', '0'), ('11', 'CafePage', '2016-08-29 17:55:13', '2016-08-24 18:15:08', 'cafe', 'Cafe', null, '<h1>Cafe</h1><hr><p class=\"lead\">With an amazing year-round view of the Auckland Domain, Santos Café in Carlton Gore Road, Newmarket is the perfect place to enjoy a true artisan roasting experience.</p><p>Santos is a popular meeting place with plenty of seating and large tables, indoors and out.</p><p>Our friendly and welcoming staff will look after you from the minute you walk in our door. For discerning tea drinkers we also stock Harney &amp; Sons Tea.</p><p>Our cabinet food changes daily and includes salads, gourmet sandwiches &amp; wraps, cakes, slices, scones, brioche, Pioneer Pies, gluten-free goodies, smoothies, Paleo Bliss balls and more.</p><p>We stock a range of domestic espresso machines, grinders, plungers, and stovetop espresso makers for the serious coffee enthusiast.</p><p>Come and see us any time from 7am to 3pm Monday to Friday.</p>', null, null, '1', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '10', '0'), ('12', 'Blog', '2016-09-01 11:57:26', '2016-08-24 18:15:23', 'news', 'News', null, '<p class=\"lead\">This is where we’ll keep you posted about what’s happening at SANTOS and the wonderful world of coffee.</p>', null, null, '1', '1', '8', '0', '0', null, 'Inherit', 'Inherit', null, '7', '0'), ('13', 'StaffProfile', '2016-08-29 18:51:33', '2016-08-29 17:35:48', 'george', 'George', null, '<p>As long as we live its you and me baby. There ain\'t nothin\' wrong with that. The movie star the professor and Mary Ann here on Gilligans Isle.</p><p>They call him Flipper Flipper faster than lightning. No one you see is smarter than he. The year is 1987 and NASA launches the last of Americas deep space probes.</p>', null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '7', '10'), ('14', 'StaffProfile', '2016-08-29 18:30:48', '2016-08-29 18:30:18', 'buck', 'Buck', null, '<p>And if you threw a party - invited everyone you knew. You would see the biggest gift would be from me and the card attached would say thank you for being a friend. Sunny Days sweepin\' the clouds away. On my way to where the air is sweet. Can you tell me how to get how to get to Sesame Street? Doin\' it our way.</p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '3', '10'), ('15', 'StaffProfile', '2016-08-29 18:41:36', '2016-08-29 18:30:53', 'wilma', 'Wilma', null, '<p>As long as we live its you and me baby. There ain\'t nothin\' wrong with that. The movie star the professor and Mary Ann here on Gilligans Isle., They call him Flipper Flipper faster than lightning. No one you see is smarter than he. The year is 1987 and NASA launches the last of Americas deep space probes.</p>', null, null, '1', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '4', '10'), ('16', 'StaffProfile', '2016-08-29 18:49:33', '2016-08-29 18:49:07', 'rosa', 'Rosa', null, '<p>In 1972 a crack commando unit was sent to prison by a military court for a crime they didn\'t commit. These men promptly escaped from a maximum security stockade to the Los Angeles underground. It\'s time to play the music. It\'s time to light the lights. It\'s time to meet the Muppets on the Muppet Show tonight. Straightnin\' the curves.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '2', '10'), ('17', 'BlogPost', '2016-09-01 10:12:56', '2016-09-01 10:12:16', 'pouring-the-perfect-cup', 'Pouring the perfect cup', null, '<p>Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. So this is the tale of our castaways they\'re here for a long long time. They\'ll have to make the best of things its an uphill climb. Maybe you and me were never meant to be.</p><p>But baby think of me once in awhile. I\'m at WKRP in Cincinnati? It\'s a beautiful day in this neighborhood a beautiful day for a neighbor. Would you be mine? Could you be mine? Its a neighborly day in this beautywood a neighborly day for a beauty. Would you be mine? Could you be mine. Boy the way Glen Miller played. Songs that made the hit parade. Guys like us we had it made. Those were the days.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '3', '12'), ('18', 'BlogPost', '2016-09-01 10:13:30', '2016-09-01 10:13:04', 'pouring-the-perfect-cup-2', 'Pouring the perfect cup 2', null, '<p>Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. So this is the tale of our castaways they\'re here for a long long time. They\'ll have to make the best of things its an uphill climb. Maybe you and me were never meant to be. But baby think of me once in awhile. I\'m at WKRP in Cincinnati?</p><p>It\'s a beautiful day in this neighborhood a beautiful day for a neighbor. Would you be mine? Could you be mine? Its a neighborly day in this beautywood a neighborly day for a beauty. Would you be mine? Could you be mine. Boy the way Glen Miller played. Songs that made the hit parade. Guys like us we had it made. Those were the days.</p><p> </p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '3', '12'), ('19', 'BlogPost', '2016-09-01 10:14:03', '2016-09-01 10:13:37', 'pouring-the-perfect-cup-3', 'Pouring the perfect cup 3', null, '<p>Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. So this is the tale of our castaways they\'re here for a long long time. They\'ll have to make the best of things its an uphill climb.</p><p>Maybe you and me were never meant to be. But baby think of me once in awhile. I\'m at WKRP in Cincinnati? It\'s a beautiful day in this neighborhood a beautiful day for a neighbor. Would you be mine? Could you be mine?</p><p>Its a neighborly day in this beautywood a neighborly day for a beauty. Would you be mine? Could you be mine. Boy the way Glen Miller played. Songs that made the hit parade. Guys like us we had it made. Those were the days.</p><p> </p>', null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '3', '12'), ('20', 'BlogPost', '2016-09-02 12:40:02', '2016-09-01 10:42:30', 'pouring-the-perfect-cup-4', 'Pouring the perfect cup 4', null, '<p><span>Maybe you and me were never meant to be. But baby think of me once in awhile. </span></p><p><span>I\'m at WKRP in Cincinnati. Straightnin\' the curves. Flatnin\' the hills Someday the mountain might get ‘em but the law never will? And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Flying away on a wing and a prayer. </span></p><p><span>Who could it be? Believe it or not its just me.</span></p>', null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '4', '12'), ('21', 'BlogPost', '2016-09-01 10:47:59', '2016-09-01 10:47:34', 'a-new-news-item', 'A new news item', null, '<p>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</p><p><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></p>', null, null, '0', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '3', '12'), ('22', 'BlogPost', '2016-09-02 12:39:50', '2016-09-01 10:48:03', 'another-new-item-of-news', 'Another new item of news', null, '<p><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></p><p><span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></span></p>', null, null, '0', '1', '6', '0', '0', null, 'Inherit', 'Inherit', null, '4', '12'), ('23', 'BlogPost', '2016-09-02 12:32:01', '2016-09-01 11:45:07', 'maybe-you-and-me-were-never-meant-to-be', 'Maybe you and me were never meant to be', null, '<p><span>But baby think of me once in awhile. I\'m at WKRP in Cincinnati. Straightnin\' the curves. Flatnin\' the hills Someday the mountain might get ‘em but the law never will? </span></p>', null, null, '0', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '5', '12'), ('24', 'AccountPage', '2016-09-01 14:12:40', '2016-09-01 14:12:40', 'account', 'Account', null, null, null, null, '0', '1', '12', '0', '0', null, 'Inherit', 'Inherit', null, '1', '0'), ('25', 'CartPage', '2016-09-01 14:12:40', '2016-09-01 14:12:40', 'cart', 'Shopping Cart', null, null, null, null, '0', '1', '13', '0', '0', null, 'Inherit', 'Inherit', null, '1', '0'), ('26', 'CheckoutPage', '2016-09-01 14:12:40', '2016-09-01 14:12:40', 'checkout', 'Checkout', null, null, null, null, '0', '1', '14', '0', '0', null, 'Inherit', 'Inherit', null, '1', '0'), ('61', 'Page', '2016-09-01 14:16:37', '2016-09-01 14:16:37', 'terms-and-conditions', 'Terms and Conditions', 'Terms & Conditions', 'You agree to...', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '1', '0'), ('62', 'Product', '2016-09-02 12:00:53', '2016-09-02 07:38:14', 'marcelos-blend', 'Marcelo\'s Blend', null, '<p>Marcelo De Souza’s original Santos blend. This is a Rainforest Alliance certified coffee, using 100% Brazilian beans blended from four different estates. It’s medium roasted to produce a smooth velvety body with a distinct nutty finish. Low acidity.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '6', '8'), ('63', 'Product', '2016-09-02 12:05:22', '2016-09-02 07:49:56', 'city-blend', 'City Blend', null, '<p>Developed with the flat white in mind, this blend perfectly cuts through the milk-based drinks that Kiwi coffee drinkers love so much. This is a four origin blend roasted medium-dark, giving rich, warm, chocolate flavour notes. Medium acidity.</p><p>Our City Blend was voted one of the best Flat White blends in NZ, and awarded bronze at the 2013 NZ Coffee Awards.</p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '11', '8'), ('64', 'Product', '2016-09-02 12:24:19', '2016-09-02 07:49:56', 'new-northern', 'New Northern', null, '<p>This blend contains medium roasted coffee beans from the Pacific, Central &amp; South America.</p><p>It’s a full bodied brew with subtle complexity, exhibiting a light spicy finish. It’s equally good as an espresso or in the plunger.</p>', null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '8', '8'), ('65', 'Product', '2016-09-02 10:38:35', '2016-09-02 08:06:32', 'santos-organic', 'Santos Organic', null, '<p>A blend of three Fairtrade organic coffees, medium roasted producing vanilla aromas with silky honey and caramel notes. Medium acidity.</p>', null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '5', '8'), ('66', 'Product', '2016-09-02 10:38:56', '2016-09-02 08:07:32', 'decaf', 'Decaf', null, '<p>Our decaffeinated coffee is a Swiss Water® Brazilian bean which is 99.9% caffeine-free and 100% chemical-free. It\'s decaffeinated coffee without compromise.</p><p>The unique SWISS WATER® process uses pure water from British Columbia, Canada to gently remove the caffeine until the coffee beans are 99.9% caffeine-free, while maintaining the bean\'s distinctive origin and flavour characteristics.</p>', null, null, '0', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '4', '8');
COMMIT;

-- ----------------------------
--  Table structure for `SiteTree_EditorGroups`
-- ----------------------------
DROP TABLE IF EXISTS `SiteTree_EditorGroups`;
CREATE TABLE `SiteTree_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `SiteTree_ImageTracking`
-- ----------------------------
DROP TABLE IF EXISTS `SiteTree_ImageTracking`;
CREATE TABLE `SiteTree_ImageTracking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `FileID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `FileID` (`FileID`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `SiteTree_ImageTracking`
-- ----------------------------
BEGIN;
INSERT INTO `SiteTree_ImageTracking` VALUES ('31', '2', '30', 'Content'), ('149', '9', '42', 'Content'), ('150', '9', '44', 'Content'), ('151', '9', '45', 'Content');
COMMIT;

-- ----------------------------
--  Table structure for `SiteTree_LinkTracking`
-- ----------------------------
DROP TABLE IF EXISTS `SiteTree_LinkTracking`;
CREATE TABLE `SiteTree_LinkTracking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `ChildID` (`ChildID`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `SiteTree_LinkTracking`
-- ----------------------------
BEGIN;
INSERT INTO `SiteTree_LinkTracking` VALUES ('11', '8', '1', 'Content2'), ('46', '9', '1', 'Content2');
COMMIT;

-- ----------------------------
--  Table structure for `SiteTree_Live`
-- ----------------------------
DROP TABLE IF EXISTS `SiteTree_Live`;
CREATE TABLE `SiteTree_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SiteTree','Page','CafePage','CoffeePage','HomePage','StaffHolder','StaffProfile','Blog','BlogPost','BlogEntry','ErrorPage','RedirectorPage','VirtualPage','AccountPage','CartPage','CheckoutPage','Product','ProductCategory','BlogTree','BlogHolder') CHARACTER SET utf8 DEFAULT 'SiteTree',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Priority` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `SiteTree_Live`
-- ----------------------------
BEGIN;
INSERT INTO `SiteTree_Live` VALUES ('1', 'HomePage', '2016-08-30 10:27:56', '2016-04-20 14:28:51', 'home', 'Home', null, null, null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '6', '0'), ('2', 'Page', '2016-08-30 11:55:32', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">The Wyatt name is synonymous with coffee and tea in New Zealand, dating back to when the Wyatt family purchased Wilcox Tea in 1966.</p><p>We were very English back then, with everyone drinking loose leaf tea which arrived in the country in large tea chests. Coffee was a small side-business at the time, but as the years progressed it became a bigger focus.</p><p>Lance Wyatt saw a need to start a fresh roasted coffee movement here but import duties made it cost prohibitive to roast coffee in the early 80s. So Lance built his own coffee roasting machine in 1983, effectively becoming one of New Zealand’s coffee roasting pioneer’s.</p><p>Aaron grew up in the industry, working for his dad during school holidays, until he joined full-time in 1999 as a coffee machine technician.</p><p>Santos Coffee was founded in 1995 by Brazilian native, Marcelo de Souza. Marcelo started the roastery in Mt. Eden, before opening the iconic Santos Café in Ponsonby, where it was part of the scenery for many years. The roastery was moved to the top of New North Road in Kingsland, until selling Santos to the Wyatts in 2012.</p><p>Santos Coffee continued to be roasted from its Kingsland location until Lance and Aaron found a more public-friendly location in Carlton Gore Road, Newmarket. This was the first time Santos had a building large enough to house the roaster, an espresso bar and café. The idea was, if you weren’t admiring the amazing view of Auckland domain whilst enjoying a great brew, you could melt into the coffee roasting process.</p><p>The doors of our new location opened to the public in April 2015. From here we continue to build on our urban, city coffee brand for coffee aficionados. You can enjoy Santos Coffee at our own and other high-quality cafes around the country.</p><h1>Service</h1><hr><p class=\"lead\">What we offer is proven, premium and Speciality grade award winning coffees.</p><p>As artisans we know coffee inside-out, and we know what works for different requirements. We offer our cafē and corporate customers the full package: supply, service and support. This ensures you can concentrate on serving great coffee at your cafē or workplace without any of the associated headaches. We serve our customers best by having great products and equipment delivered with awesome service from well-trained people. All of our staff are trained baristas, even our receptionist and warehouse manager. That’s because we all wear multiple hats here. Our team knows every aspect of the coffee business. If someone’s unavailable, someone else has you covered. They can answer any questions you have and make you a great coffee to top it off.</p><p>Our after sales service is unbelievably good too. It’s really only when something goes wrong that you understand the value of your coffee roaster and what they offer you. should your espresso machine break down, we react within an hour to fix or replace it. It’s this kind of service from a large behind the scenes team that will make all the difference to your business. We can tailor how you deal with us, with either automatic weekly or daily deliveries, or email or phone us with your orders.</p><p>We’re confident you’ll love Santos Coffee, which is why we won\'t tie you down to a contract. At Santos Coffee we blend everything together to provide you the best coffee experience of your life.</p><p>Ask us about:</p><ul><li><em>Santos branded merchandise</em> – signs, shirts, aprons, umbrellas</li>\n<li><em>Sanremo espresso machines</em> – we think they’re the best espresso machine available</li>\n<li><em>Barista training</em></li>\n<li><em>Our easy ordering system</em> – a one-stop-shop for coffee and consumables</li>\n<li><em>Technical service</em></li>\n<li><em>The best coffees money can buy</em>.</li>\n</ul><h1>Machines</h1><hr><p class=\"lead\">We’ve seen and used a lot of different coffee machines over the decades.</p><p>We’re proud to use, recommend and represent Sanremo espresso machines and grinders, manufactured in Treviso, Italy. Like us, Sanremo is a family-owned business, with a factory producing outstanding machines. They have a tightknit team who like us are focused on excellence.</p><p>Aesthetically, Sanremo machines are not only beautiful, but they’re also extremely reliable and easy to use. We have models to suit every size environment, from the small office to the large corporate workplace and busy cafē.</p><p>If you want reliability, beauty and perfection, choose Sanremo.</p><p class=\"lead\"><img class=\"img-responsive\" style=\"margin-left: -30px;\" title=\"\" src=\"assets/Uploads/sanremo.jpg\" alt=\"sanremo\"></p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '16', '0'), ('4', 'ErrorPage', '2016-08-24 18:17:23', '2016-04-20 14:28:52', 'page-not-found', 'Page not found', null, '<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', null, null, '0', '0', '10', '0', '0', null, 'Inherit', 'Inherit', null, '2', '0'), ('5', 'ErrorPage', '2016-08-24 18:17:26', '2016-04-20 14:28:52', 'server-error', 'Server error', null, '<p>Sorry, there was a problem with handling your request.</p>', null, null, '0', '0', '11', '0', '0', null, 'Inherit', 'Inherit', null, '2', '0'), ('8', 'ProductCategory', '2016-09-02 13:06:12', '2016-08-24 18:14:27', 'shop', 'Shop', null, null, null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '14', '0'), ('9', 'CoffeePage', '2016-09-02 13:04:30', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p><h1>Awards</h1><hr><p class=\"lead\">Here\'s a few awards we\'ve won</p><p><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/awards.png\" alt=\"awards\"></p><h1>Certified</h1><hr><p class=\"lead\">We help reduce the environmental impact of bad agriculture practice by choosing beans from sustainable farms.</p><p>Santos Coffee is dedicated to helping people preserve the environment, which is why traceability and sustainability in sourcing coffee beans is very high on our agenda.</p><p>The Rainforest Alliance is an international non-profit organisation that works to conserve biodiversity and promote the rights and well-being of workers, their families and communities. Farms that meet comprehensive standards for sustainability earn the Rainforest Alliance Certified™ seal. These standards conserve biodiversity, ensure that soils, waterways and wildlife habitat is protected and that farm workers enjoy decent housing, access to medical care and schools for their children.</p><p>Farms must commit to a process of continuous improvement and are audited each year in order to maintain their certification. By shopping for products bearing the Rainforest Alliance Certified seal, you can support a healthy environment and help to improve the quality of life for farm families. To learn more about the Rainforest Alliance, visit www.rainforest-alliance.org.</p><p><img title=\"\" src=\"assets/Uploads/certified-green.svg\" alt=\"\" width=\"144\"></p><h1>Blends</h1><hr><p class=\"lead\">It’s a fine art creating quality blends, because a blend is greater than the sum of its parts. When creating a blend, our goal is to achieve a balanced, distinct flavour profile that can then be consistently reproduced.</p><p>Beans are chosen for their ability to complement each other, based on flavour, aroma and body. For this reason, blends often provide a more complex character when compared to varietals or single origin beans.</p><p>A lot of factors goes into the blending process to ensure strict quality control. This includes a lot of coffee cupping, or tasting, to observe the tastes and aromas of brewed coffee. It’s here that we identify the flavour profiles, much like a chef does with food. We then experiment until we have the ideal blend.</p><p>One way were able to produce consistent blends is in the way we roast and blend. There are two ways blending occurs. The first is mix all beans together and roast. The second, our way, is to roast the beans separately, then blend them. This allows us to roast each bean exactly how we want to, and the way that we know will produce optimal results for that bean.</p><p> </p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '13', '0'), ('10', 'StaffHolder', '2016-08-29 18:51:46', '2016-08-24 18:14:55', 'team', 'Team', null, null, null, null, '1', '1', '6', '0', '0', null, 'Inherit', 'Inherit', null, '4', '0'), ('11', 'CafePage', '2016-08-29 17:55:13', '2016-08-24 18:15:08', 'cafe', 'Cafe', null, '<h1>Cafe</h1><hr><p class=\"lead\">With an amazing year-round view of the Auckland Domain, Santos Café in Carlton Gore Road, Newmarket is the perfect place to enjoy a true artisan roasting experience.</p><p>Santos is a popular meeting place with plenty of seating and large tables, indoors and out.</p><p>Our friendly and welcoming staff will look after you from the minute you walk in our door. For discerning tea drinkers we also stock Harney &amp; Sons Tea.</p><p>Our cabinet food changes daily and includes salads, gourmet sandwiches &amp; wraps, cakes, slices, scones, brioche, Pioneer Pies, gluten-free goodies, smoothies, Paleo Bliss balls and more.</p><p>We stock a range of domestic espresso machines, grinders, plungers, and stovetop espresso makers for the serious coffee enthusiast.</p><p>Come and see us any time from 7am to 3pm Monday to Friday.</p>', null, null, '1', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '10', '0'), ('12', 'Blog', '2016-09-01 11:57:26', '2016-08-24 18:15:23', 'news', 'News', null, '<p class=\"lead\">This is where we’ll keep you posted about what’s happening at SANTOS and the wonderful world of coffee.</p>', null, null, '1', '1', '8', '0', '0', null, 'Inherit', 'Inherit', null, '7', '0'), ('13', 'StaffProfile', '2016-08-29 18:51:33', '2016-08-29 17:35:48', 'george', 'George', null, '<p>As long as we live its you and me baby. There ain\'t nothin\' wrong with that. The movie star the professor and Mary Ann here on Gilligans Isle.</p><p>They call him Flipper Flipper faster than lightning. No one you see is smarter than he. The year is 1987 and NASA launches the last of Americas deep space probes.</p>', null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '7', '10'), ('14', 'StaffProfile', '2016-08-29 18:30:48', '2016-08-29 18:30:18', 'buck', 'Buck', null, '<p>And if you threw a party - invited everyone you knew. You would see the biggest gift would be from me and the card attached would say thank you for being a friend. Sunny Days sweepin\' the clouds away. On my way to where the air is sweet. Can you tell me how to get how to get to Sesame Street? Doin\' it our way.</p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '3', '10'), ('15', 'StaffProfile', '2016-08-29 18:41:36', '2016-08-29 18:30:53', 'wilma', 'Wilma', null, '<p>As long as we live its you and me baby. There ain\'t nothin\' wrong with that. The movie star the professor and Mary Ann here on Gilligans Isle., They call him Flipper Flipper faster than lightning. No one you see is smarter than he. The year is 1987 and NASA launches the last of Americas deep space probes.</p>', null, null, '1', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '4', '10'), ('16', 'StaffProfile', '2016-08-29 18:49:34', '2016-08-29 18:49:07', 'rosa', 'Rosa', null, '<p>In 1972 a crack commando unit was sent to prison by a military court for a crime they didn\'t commit. These men promptly escaped from a maximum security stockade to the Los Angeles underground. It\'s time to play the music. It\'s time to light the lights. It\'s time to meet the Muppets on the Muppet Show tonight. Straightnin\' the curves.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '2', '10'), ('17', 'BlogPost', '2016-09-01 10:12:56', '2016-09-01 10:12:16', 'pouring-the-perfect-cup', 'Pouring the perfect cup', null, '<p>Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. So this is the tale of our castaways they\'re here for a long long time. They\'ll have to make the best of things its an uphill climb. Maybe you and me were never meant to be.</p><p>But baby think of me once in awhile. I\'m at WKRP in Cincinnati? It\'s a beautiful day in this neighborhood a beautiful day for a neighbor. Would you be mine? Could you be mine? Its a neighborly day in this beautywood a neighborly day for a beauty. Would you be mine? Could you be mine. Boy the way Glen Miller played. Songs that made the hit parade. Guys like us we had it made. Those were the days.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '3', '12'), ('18', 'BlogPost', '2016-09-01 10:13:30', '2016-09-01 10:13:04', 'pouring-the-perfect-cup-2', 'Pouring the perfect cup 2', null, '<p>Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. So this is the tale of our castaways they\'re here for a long long time. They\'ll have to make the best of things its an uphill climb. Maybe you and me were never meant to be. But baby think of me once in awhile. I\'m at WKRP in Cincinnati?</p><p>It\'s a beautiful day in this neighborhood a beautiful day for a neighbor. Would you be mine? Could you be mine? Its a neighborly day in this beautywood a neighborly day for a beauty. Would you be mine? Could you be mine. Boy the way Glen Miller played. Songs that made the hit parade. Guys like us we had it made. Those were the days.</p><p> </p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '3', '12'), ('19', 'BlogPost', '2016-09-01 10:14:03', '2016-09-01 10:13:37', 'pouring-the-perfect-cup-3', 'Pouring the perfect cup 3', null, '<p>Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. So this is the tale of our castaways they\'re here for a long long time. They\'ll have to make the best of things its an uphill climb.</p><p>Maybe you and me were never meant to be. But baby think of me once in awhile. I\'m at WKRP in Cincinnati? It\'s a beautiful day in this neighborhood a beautiful day for a neighbor. Would you be mine? Could you be mine?</p><p>Its a neighborly day in this beautywood a neighborly day for a beauty. Would you be mine? Could you be mine. Boy the way Glen Miller played. Songs that made the hit parade. Guys like us we had it made. Those were the days.</p><p> </p>', null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '3', '12'), ('20', 'BlogPost', '2016-09-02 12:40:02', '2016-09-01 10:42:30', 'pouring-the-perfect-cup-4', 'Pouring the perfect cup 4', null, '<p><span>Maybe you and me were never meant to be. But baby think of me once in awhile. </span></p><p><span>I\'m at WKRP in Cincinnati. Straightnin\' the curves. Flatnin\' the hills Someday the mountain might get ‘em but the law never will? And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Flying away on a wing and a prayer. </span></p><p><span>Who could it be? Believe it or not its just me.</span></p>', null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '4', '12'), ('21', 'BlogPost', '2016-09-01 10:47:59', '2016-09-01 10:47:34', 'a-new-news-item', 'A new news item', null, '<p>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</p><p><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></p>', null, null, '0', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '3', '12'), ('22', 'BlogPost', '2016-09-02 12:39:50', '2016-09-01 10:48:03', 'another-new-item-of-news', 'Another new item of news', null, '<p><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></p><p><span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></span></p>', null, null, '0', '1', '6', '0', '0', null, 'Inherit', 'Inherit', null, '4', '12'), ('23', 'BlogPost', '2016-09-02 12:32:01', '2016-09-01 11:45:07', 'maybe-you-and-me-were-never-meant-to-be', 'Maybe you and me were never meant to be', null, '<p><span>But baby think of me once in awhile. I\'m at WKRP in Cincinnati. Straightnin\' the curves. Flatnin\' the hills Someday the mountain might get ‘em but the law never will? </span></p>', null, null, '0', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '5', '12'), ('24', 'AccountPage', '2016-09-01 14:12:40', '2016-09-01 14:12:40', 'account', 'Account', null, null, null, null, '0', '1', '12', '0', '0', null, 'Inherit', 'Inherit', null, '1', '0'), ('25', 'CartPage', '2016-09-01 14:12:40', '2016-09-01 14:12:40', 'cart', 'Shopping Cart', null, null, null, null, '0', '1', '13', '0', '0', null, 'Inherit', 'Inherit', null, '1', '0'), ('26', 'CheckoutPage', '2016-09-01 14:12:40', '2016-09-01 14:12:40', 'checkout', 'Checkout', null, null, null, null, '0', '1', '14', '0', '0', null, 'Inherit', 'Inherit', null, '1', '0'), ('61', 'Page', '2016-09-01 14:16:37', '2016-09-01 14:16:37', 'terms-and-conditions', 'Terms and Conditions', 'Terms & Conditions', 'You agree to...', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '1', '0'), ('62', 'Product', '2016-09-02 12:00:53', '2016-09-02 07:38:14', 'marcelos-blend', 'Marcelo\'s Blend', null, '<p>Marcelo De Souza’s original Santos blend. This is a Rainforest Alliance certified coffee, using 100% Brazilian beans blended from four different estates. It’s medium roasted to produce a smooth velvety body with a distinct nutty finish. Low acidity.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '6', '8'), ('63', 'Product', '2016-09-02 12:05:22', '2016-09-02 07:49:56', 'city-blend', 'City Blend', null, '<p>Developed with the flat white in mind, this blend perfectly cuts through the milk-based drinks that Kiwi coffee drinkers love so much. This is a four origin blend roasted medium-dark, giving rich, warm, chocolate flavour notes. Medium acidity.</p><p>Our City Blend was voted one of the best Flat White blends in NZ, and awarded bronze at the 2013 NZ Coffee Awards.</p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '11', '8'), ('64', 'Product', '2016-09-02 12:24:19', '2016-09-02 07:49:56', 'new-northern', 'New Northern', null, '<p>This blend contains medium roasted coffee beans from the Pacific, Central &amp; South America.</p><p>It’s a full bodied brew with subtle complexity, exhibiting a light spicy finish. It’s equally good as an espresso or in the plunger.</p>', null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '8', '8'), ('65', 'Product', '2016-09-02 10:38:35', '2016-09-02 08:06:32', 'santos-organic', 'Santos Organic', null, '<p>A blend of three Fairtrade organic coffees, medium roasted producing vanilla aromas with silky honey and caramel notes. Medium acidity.</p>', null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '5', '8'), ('66', 'Product', '2016-09-02 10:38:56', '2016-09-02 08:07:32', 'decaf', 'Decaf', null, '<p>Our decaffeinated coffee is a Swiss Water® Brazilian bean which is 99.9% caffeine-free and 100% chemical-free. It\'s decaffeinated coffee without compromise.</p><p>The unique SWISS WATER® process uses pure water from British Columbia, Canada to gently remove the caffeine until the coffee beans are 99.9% caffeine-free, while maintaining the bean\'s distinctive origin and flavour characteristics.</p>', null, null, '0', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '4', '8');
COMMIT;

-- ----------------------------
--  Table structure for `SiteTree_ViewerGroups`
-- ----------------------------
DROP TABLE IF EXISTS `SiteTree_ViewerGroups`;
CREATE TABLE `SiteTree_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `SiteTree_versions`
-- ----------------------------
DROP TABLE IF EXISTS `SiteTree_versions`;
CREATE TABLE `SiteTree_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('SiteTree','Page','CafePage','CoffeePage','HomePage','StaffHolder','StaffProfile','Blog','BlogPost','BlogEntry','ErrorPage','RedirectorPage','VirtualPage','AccountPage','CartPage','CheckoutPage','Product','ProductCategory','BlogTree','BlogHolder') CHARACTER SET utf8 DEFAULT 'SiteTree',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `URLSegment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MenuTitle` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Content` mediumtext CHARACTER SET utf8,
  `MetaDescription` mediumtext CHARACTER SET utf8,
  `ExtraMeta` mediumtext CHARACTER SET utf8,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') CHARACTER SET utf8 DEFAULT 'Inherit',
  `Priority` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=230 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `SiteTree_versions`
-- ----------------------------
BEGIN;
INSERT INTO `SiteTree_versions` VALUES ('1', '1', '1', '1', '1', '1', 'Page', '2016-04-20 14:28:51', '2016-04-20 14:28:51', 'home', 'Home', null, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href=\"admin/\">the CMS</a>.</p><p>You can now access the <a href=\"http://docs.silverstripe.org\">developer documentation</a>, or begin the <a href=\"http://www.silverstripe.org/learn/lessons\">SilverStripe lessons</a>.</p>', null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('2', '2', '1', '1', '1', '1', 'Page', '2016-04-20 14:28:52', '2016-04-20 14:28:52', 'about-us', 'About Us', null, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('3', '3', '1', '1', '1', '1', 'Page', '2016-04-20 14:28:52', '2016-04-20 14:28:52', 'contact-us', 'Contact Us', null, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', null, null, '1', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('4', '4', '1', '1', '1', '1', 'ErrorPage', '2016-04-20 14:28:52', '2016-04-20 14:28:52', 'page-not-found', 'Page not found', null, '<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', null, null, '0', '0', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('5', '5', '1', '1', '1', '1', 'ErrorPage', '2016-04-20 14:28:52', '2016-04-20 14:28:52', 'server-error', 'Server error', null, '<p>Sorry, there was a problem with handling your request.</p>', null, null, '0', '0', '5', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('6', '6', '1', '1', '0', '0', '', '2016-05-04 09:32:16', '2016-05-04 09:32:16', 'forums', 'Forums', null, '<p>Welcome to SilverStripe Forum Module! This is the default ForumHolder page. You can now add forums.</p>', null, null, '1', '1', '6', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('7', '7', '1', '1', '0', '0', '', '2016-05-04 09:32:16', '2016-05-04 09:32:16', 'general-discussion', 'General Discussion', null, '<p>Welcome to SilverStripe Forum Module! This is the default Forum page. You can now add topics.</p>', null, null, '1', '1', '0', '0', '0', null, 'Inherit', 'Inherit', null, '6'), ('8', '2', '2', '1', '1', '1', 'Page', '2016-08-24 18:14:05', '2016-04-20 14:28:52', 'about', 'About', null, '<p>You can fill this page out with your own content, or delete it and create your own pages.</p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('9', '8', '1', '0', '1', '0', 'Page', '2016-08-24 18:14:27', '2016-08-24 18:14:27', 'new-page', 'New Page', null, null, null, null, '1', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('10', '8', '2', '1', '1', '1', 'Page', '2016-08-24 18:14:35', '2016-08-24 18:14:27', 'shop', 'Shop', null, null, null, null, '1', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('11', '9', '1', '0', '1', '0', 'Page', '2016-08-24 18:14:41', '2016-08-24 18:14:41', 'new-page', 'New Page', null, null, null, null, '1', '1', '8', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('12', '9', '2', '1', '1', '1', 'Page', '2016-08-24 18:14:49', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, null, null, null, '1', '1', '8', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('13', '10', '1', '0', '1', '0', 'Page', '2016-08-24 18:14:55', '2016-08-24 18:14:55', 'new-page', 'New Page', null, null, null, null, '1', '1', '9', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('14', '10', '2', '1', '1', '1', 'Page', '2016-08-24 18:15:00', '2016-08-24 18:14:55', 'team', 'Team', null, null, null, null, '1', '1', '9', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('15', '11', '1', '0', '1', '0', 'Page', '2016-08-24 18:15:08', '2016-08-24 18:15:08', 'new-page', 'New Page', null, null, null, null, '1', '1', '10', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('16', '11', '2', '1', '1', '1', 'Page', '2016-08-24 18:15:13', '2016-08-24 18:15:08', 'cafe', 'Cafe', null, null, null, null, '1', '1', '10', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('17', '12', '1', '0', '1', '0', 'Blog', '2016-08-24 18:15:23', '2016-08-24 18:15:23', 'new-blog', 'New Blog', null, null, null, null, '1', '1', '11', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('18', '12', '2', '1', '1', '1', 'Blog', '2016-08-24 18:15:51', '2016-08-24 18:15:23', 'news', 'News', null, null, null, null, '1', '1', '11', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('19', '3', '2', '0', '1', '0', 'Page', '2016-08-24 18:16:00', '2016-04-20 14:28:52', 'contact-us', 'Contact Us', null, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', null, null, '1', '1', '11', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('20', '4', '2', '1', '1', '1', 'ErrorPage', '2016-08-24 18:16:04', '2016-04-20 14:28:52', 'page-not-found', 'Page not found', null, '<p>Sorry, it seems you were trying to access a page that doesn\'t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', null, null, '0', '0', '11', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('21', '5', '2', '1', '1', '1', 'ErrorPage', '2016-08-24 18:16:07', '2016-04-20 14:28:52', 'server-error', 'Server error', null, '<p>Sorry, there was a problem with handling your request.</p>', null, null, '0', '0', '11', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('22', '6', '2', '1', '1', '1', 'Page', '2016-08-24 18:16:28', '2016-05-04 09:32:16', 'forums', 'Forums', null, '<p>Welcome to SilverStripe Forum Module! This is the default ForumHolder page. You can now add forums.</p>', null, null, '1', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('23', '7', '2', '1', '1', '1', 'Page', '2016-08-24 18:16:39', '2016-05-04 09:32:16', 'general-discussion', 'General Discussion', null, '<p>Welcome to SilverStripe Forum Module! This is the default Forum page. You can now add topics.</p>', null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '6'), ('24', '1', '2', '1', '1', '1', 'Page', '2016-08-24 18:17:36', '2016-04-20 14:28:51', 'home', 'Home', null, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href=\"admin/\">the CMS</a>.</p><p>You can now access the <a href=\"http://docs.silverstripe.org\">developer documentation</a>, or begin the <a href=\"http://www.silverstripe.org/learn/lessons\">SilverStripe lessons</a>.</p>', null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('25', '1', '3', '1', '1', '1', 'HomePage', '2016-08-24 22:18:57', '2016-04-20 14:28:51', 'home', 'Home', null, null, null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('26', '1', '4', '1', '1', '1', 'HomePage', '2016-08-26 11:39:44', '2016-04-20 14:28:51', 'home', 'Home', null, null, null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('27', '1', '5', '1', '1', '1', 'HomePage', '2016-08-29 13:39:36', '2016-04-20 14:28:51', 'home', 'Home', null, null, null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('28', '11', '3', '1', '1', '1', 'CafePage', '2016-08-29 14:45:15', '2016-08-24 18:15:08', 'cafe', 'Cafe', null, null, null, null, '1', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('29', '11', '4', '1', '1', '1', 'CafePage', '2016-08-29 14:48:34', '2016-08-24 18:15:08', 'cafe', 'Cafe', null, null, null, null, '1', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('30', '11', '5', '1', '1', '1', 'CafePage', '2016-08-29 14:49:02', '2016-08-24 18:15:08', 'cafe', 'Cafe', null, null, null, null, '1', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('31', '11', '6', '1', '1', '1', 'CafePage', '2016-08-29 14:52:32', '2016-08-24 18:15:08', 'cafe', 'Cafe', null, null, null, null, '1', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('32', '11', '7', '1', '1', '1', 'CafePage', '2016-08-29 15:28:33', '2016-08-24 18:15:08', 'cafe', 'Cafe', null, '<p class=\"lead\">All of them had hair of gold like their mother the youngest one in curls. Wouldn\'t you like to get away? Sometimes you want to go where everybody knows your name. And they\'re always glad you came. But they got diff\'rent strokes. It takes diff\'rent strokes - it takes diff\'rent strokes to move the world.</p><p>Well the first thing you know ol\' Jeds a millionaire. Kinfolk said Jed move away from there. And if you threw a party - invited everyone you knew. You would see the biggest gift would be from me and the card attached would say thank you for being a friend. Thank you for being a friend. Travelled down the road and back again. Your heart is true you\'re a pal and a confidant. Now the world don\'t move to the beat of just one drum. What might be right for you may not be right for some.</p><p>It\'s time to play the music. It\'s time to light the lights. It\'s time to meet the Muppets on the Muppet Show tonight. Its mission - to explore strange new worlds to seek out new life and new civilizations to boldly go where no man has gone before?</p><p> </p>', null, null, '1', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('33', '2', '3', '1', '1', '1', 'Page', '2016-08-29 16:48:35', '2016-04-20 14:28:52', 'about', 'About', null, '<p>History</p><p>Didn\'t need no welfare states. Everybody pulled his weight. Gee our old Lasalle ran great. Those were the days. They were four men living all together yet they were all alone. Here he comes Here comes Speed Racer. He\'s a demon on wheels. Just two good ol\' boys Wouldn\'t change if they could.</p><p>Fightin\' the system like a true modern day Robin Hood. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. And we know Flipper lives in a world full of wonder flying there-under under the sea. Wouldn\'t you like to get away?</p><p>Sometimes you want to go where everybody knows your name. And they\'re always glad you came. Here\'s the story of a lovely lady who was bringing up three very lovely girls.</p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('34', '2', '4', '1', '1', '1', 'Page', '2016-08-29 17:06:34', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><p>Didn\'t need no welfare states. Everybody pulled his weight. Gee our old Lasalle ran great. Those were the days. They were four men living all together yet they were all alone. Here he comes Here comes Speed Racer. He\'s a demon on wheels. Just two good ol\' boys Wouldn\'t change if they could.</p><p>Fightin\' the system like a true modern day Robin Hood. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. And we know Flipper lives in a world full of wonder flying there-under under the sea. Wouldn\'t you like to get away?</p><p>Sometimes you want to go where everybody knows your name. And they\'re always glad you came. Here\'s the story of a lovely lady who was bringing up three very lovely girls.</p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('35', '2', '5', '1', '1', '1', 'Page', '2016-08-29 17:11:55', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p>Didn\'t need no welfare states. Everybody pulled his weight. Gee our old Lasalle ran great. Those were the days. They were four men living all together yet they were all alone. Here he comes Here comes Speed Racer. He\'s a demon on wheels. Just two good ol\' boys Wouldn\'t change if they could.</p><p>Fightin\' the system like a true modern day Robin Hood. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. And we know Flipper lives in a world full of wonder flying there-under under the sea. Wouldn\'t you like to get away?</p><p>Sometimes you want to go where everybody knows your name. And they\'re always glad you came. Here\'s the story of a lovely lady who was bringing up three very lovely girls.</p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('36', '2', '6', '1', '1', '1', 'Page', '2016-08-29 17:15:52', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">Didn\'t need no welfare states. Everybody pulled his weight. Gee our old Lasalle ran great. Those were the days. They were four men living all together yet they were all alone. Here he comes Here comes Speed Racer. He\'s a demon on wheels. Just two good ol\' boys Wouldn\'t change if they could.</p><p>Fightin\' the system like a true modern day Robin Hood. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. And we know Flipper lives in a world full of wonder flying there-under under the sea. Wouldn\'t you like to get away?</p><p>Sometimes you want to go where everybody knows your name. And they\'re always glad you came. Here\'s the story of a lovely lady who was bringing up three very lovely girls.</p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('37', '2', '7', '1', '1', '1', 'Page', '2016-08-29 17:16:25', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">Didn\'t need no welfare states. Everybody pulled his weight. Gee our old Lasalle ran great. Those were the days. They were four men living all together yet they were all alone. Here he comes Here comes Speed Racer. He\'s a demon on wheels. Just two good ol\' boys Wouldn\'t change if they could.</p><p>Fightin\' the system like a true modern day Robin Hood. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. And we know Flipper lives in a world full of wonder flying there-under under the sea. Wouldn\'t you like to get away?</p><p>Sometimes you want to go where everybody knows your name. And they\'re always glad you came. Here\'s the story of a lovely lady who was bringing up three very lovely girls.</p><h1>Service</h1><hr><p class=\"lead\">Didn\'t need no welfare states. Everybody pulled his weight. Gee our old Lasalle ran great. Those were the days. They were four men living all together yet they were all alone. Here he comes Here comes Speed Racer. He\'s a demon on wheels. Just two good ol\' boys Wouldn\'t change if they could.</p><p>Fightin\' the system like a true modern day Robin Hood. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. And we know Flipper lives in a world full of wonder flying there-under under the sea. Wouldn\'t you like to get away?</p><p>Sometimes you want to go where everybody knows your name. And they\'re always glad you came. Here\'s the story of a lovely lady who was bringing up three very lovely girls.</p><h1>Machines</h1><hr><p class=\"lead\">Didn\'t need no welfare states. Everybody pulled his weight. Gee our old Lasalle ran great. Those were the days. They were four men living all together yet they were all alone. Here he comes Here comes Speed Racer. He\'s a demon on wheels. Just two good ol\' boys Wouldn\'t change if they could.</p><p>Fightin\' the system like a true modern day Robin Hood. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. And we know Flipper lives in a world full of wonder flying there-under under the sea. Wouldn\'t you like to get away?</p><p>Sometimes you want to go where everybody knows your name. And they\'re always glad you came. Here\'s the story of a lovely lady who was bringing up three very lovely girls.</p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('38', '2', '8', '1', '1', '1', 'Page', '2016-08-29 17:32:22', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">Didn\'t need no welfare states. Everybody pulled his weight. Gee our old Lasalle ran great. Those were the days. They were four men living all together yet they were all alone. Here he comes Here comes Speed Racer. He\'s a demon on wheels. Just two good ol\' boys Wouldn\'t change if they could.</p><p>Fightin\' the system like a true modern day Robin Hood. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. And we know Flipper lives in a world full of wonder flying there-under under the sea. Wouldn\'t you like to get away?</p><p>Sometimes you want to go where everybody knows your name. And they\'re always glad you came. Here\'s the story of a lovely lady who was bringing up three very lovely girls.</p><h1>Service</h1><hr><p class=\"lead\">Didn\'t need no welfare states. Everybody pulled his weight. Gee our old Lasalle ran great. Those were the days. They were four men living all together yet they were all alone. Here he comes Here comes Speed Racer. He\'s a demon on wheels. Just two good ol\' boys Wouldn\'t change if they could.</p><p>Fightin\' the system like a true modern day Robin Hood. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. And we know Flipper lives in a world full of wonder flying there-under under the sea. Wouldn\'t you like to get away?</p><p>Sometimes you want to go where everybody knows your name. And they\'re always glad you came. Here\'s the story of a lovely lady who was bringing up three very lovely girls.</p><h1>Machines</h1><hr><p class=\"lead\">Didn\'t need no welfare states. Everybody pulled his weight. Gee our old Lasalle ran great. Those were the days. They were four men living all together yet they were all alone. Here he comes Here comes Speed Racer. He\'s a demon on wheels. Just two good ol\' boys Wouldn\'t change if they could.</p><p>Fightin\' the system like a true modern day Robin Hood. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. And we know Flipper lives in a world full of wonder flying there-under under the sea. Wouldn\'t you like to get away?</p><p>Sometimes you want to go where everybody knows your name. And they\'re always glad you came. Here\'s the story of a lovely lady who was bringing up three very lovely girls.</p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('39', '10', '3', '1', '1', '1', 'StaffHolder', '2016-08-29 17:35:39', '2016-08-24 18:14:55', 'team', 'Team', null, null, null, null, '1', '1', '6', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('40', '13', '1', '0', '1', '0', 'StaffProfile', '2016-08-29 17:35:48', '2016-08-29 17:35:48', 'new-staff-profile', 'New Staff Profile', null, null, null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('41', '13', '2', '1', '1', '1', 'StaffProfile', '2016-08-29 17:39:36', '2016-08-29 17:35:48', 'george', 'George', null, null, null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('42', '13', '3', '1', '1', '1', 'StaffProfile', '2016-08-29 17:40:21', '2016-08-29 17:35:48', 'george', 'George', null, '<p>As long as we live its you and me baby. There ain\'t nothin\' wrong with that. The movie star the professor and Mary Ann here on Gilligans Isle.</p><p>They call him Flipper Flipper faster than lightning. No one you see is smarter than he. The year is 1987 and NASA launches the last of Americas deep space probes.</p><p>And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Baby if you\'ve ever wondered - wondered whatever became of me. I\'m living on the air in Cincinnati. Cincinnati WKRP.</p><p><span>And if you threw a party - invited everyone you knew. You would see the biggest gift would be from me and the card attached would say thank you for being a friend. Sunny Days sweepin\' the clouds away. On my way to where the air is sweet. </span></p>', null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('43', '13', '4', '1', '1', '1', 'StaffProfile', '2016-08-29 17:41:21', '2016-08-29 17:35:48', 'george', 'George', null, '<p>As long as we live its you and me baby. There ain\'t nothin\' wrong with that. The movie star the professor and Mary Ann here on Gilligans Isle.</p><p>They call him Flipper Flipper faster than lightning. No one you see is smarter than he. The year is 1987 and NASA launches the last of Americas deep space probes.</p><p>And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Baby if you\'ve ever wondered - wondered whatever became of me. I\'m living on the air in Cincinnati. Cincinnati WKRP.</p><p><span>And if you threw a party - invited everyone you knew. You would see the biggest gift would be from me and the card attached would say thank you for being a friend. Sunny Days sweepin\' the clouds away. On my way to where the air is sweet. </span></p>', null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('44', '11', '8', '1', '1', '1', 'CafePage', '2016-08-29 17:51:01', '2016-08-24 18:15:08', 'cafe', 'Cafe', null, '<p>With an amazing year-round view of the Auckland Domain, Santos Café in Carlton Gore Road, Newmarket is the perfect place to enjoy a true artisan roasting experience.</p><p>Santos is a popular meeting place with plenty of seating and large tables, indoors and out.</p><p>Our friendly and welcoming staff will look after you from the minute you walk in our door. For discerning tea drinkers we also stock Harney &amp; Sons Tea.</p><p>Our cabinet food changes daily and includes salads, gourmet sandwiches &amp; wraps, cakes, slices, scones, brioche, Pioneer Pies, gluten-free goodies, smoothies, Paleo Bliss balls and more.</p><p>We stock a range of domestic espresso machines, grinders, plungers, and stovetop espresso makers for the serious coffee enthusiast.</p><p>Come and see us any time from 7am to 3pm Monday to Friday.</p>', null, null, '1', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('45', '11', '9', '1', '1', '1', 'CafePage', '2016-08-29 17:51:24', '2016-08-24 18:15:08', 'cafe', 'Cafe', null, '<p class=\"lead\">With an amazing year-round view of the Auckland Domain, Santos Café in Carlton Gore Road, Newmarket is the perfect place to enjoy a true artisan roasting experience.</p><p>Santos is a popular meeting place with plenty of seating and large tables, indoors and out.</p><p>Our friendly and welcoming staff will look after you from the minute you walk in our door. For discerning tea drinkers we also stock Harney &amp; Sons Tea.</p><p>Our cabinet food changes daily and includes salads, gourmet sandwiches &amp; wraps, cakes, slices, scones, brioche, Pioneer Pies, gluten-free goodies, smoothies, Paleo Bliss balls and more.</p><p>We stock a range of domestic espresso machines, grinders, plungers, and stovetop espresso makers for the serious coffee enthusiast.</p><p>Come and see us any time from 7am to 3pm Monday to Friday.</p>', null, null, '1', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('46', '11', '10', '1', '1', '1', 'CafePage', '2016-08-29 17:55:13', '2016-08-24 18:15:08', 'cafe', 'Cafe', null, '<h1>Cafe</h1><hr><p class=\"lead\">With an amazing year-round view of the Auckland Domain, Santos Café in Carlton Gore Road, Newmarket is the perfect place to enjoy a true artisan roasting experience.</p><p>Santos is a popular meeting place with plenty of seating and large tables, indoors and out.</p><p>Our friendly and welcoming staff will look after you from the minute you walk in our door. For discerning tea drinkers we also stock Harney &amp; Sons Tea.</p><p>Our cabinet food changes daily and includes salads, gourmet sandwiches &amp; wraps, cakes, slices, scones, brioche, Pioneer Pies, gluten-free goodies, smoothies, Paleo Bliss balls and more.</p><p>We stock a range of domestic espresso machines, grinders, plungers, and stovetop espresso makers for the serious coffee enthusiast.</p><p>Come and see us any time from 7am to 3pm Monday to Friday.</p>', null, null, '1', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('47', '14', '1', '0', '1', '0', 'StaffProfile', '2016-08-29 18:30:18', '2016-08-29 18:30:18', 'new-staff-profile', 'New Staff Profile', null, null, null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('48', '14', '2', '1', '1', '1', 'StaffProfile', '2016-08-29 18:30:32', '2016-08-29 18:30:18', 'buck', 'Buck', null, null, null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('49', '14', '3', '1', '1', '1', 'StaffProfile', '2016-08-29 18:30:48', '2016-08-29 18:30:18', 'buck', 'Buck', null, '<p>And if you threw a party - invited everyone you knew. You would see the biggest gift would be from me and the card attached would say thank you for being a friend. Sunny Days sweepin\' the clouds away. On my way to where the air is sweet. Can you tell me how to get how to get to Sesame Street? Doin\' it our way.</p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('50', '15', '1', '0', '1', '0', 'StaffProfile', '2016-08-29 18:30:53', '2016-08-29 18:30:53', 'new-staff-profile', 'New Staff Profile', null, null, null, null, '1', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('51', '15', '2', '1', '1', '1', 'StaffProfile', '2016-08-29 18:31:15', '2016-08-29 18:30:53', 'wima', 'Wima', null, '<p>As long as we live its you and me baby. There ain\'t nothin\' wrong with that. The movie star the professor and Mary Ann here on Gilligans Isle., They call him Flipper Flipper faster than lightning. No one you see is smarter than he. The year is 1987 and NASA launches the last of Americas deep space probes.</p>', null, null, '1', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('52', '15', '3', '1', '1', '1', 'StaffProfile', '2016-08-29 18:41:21', '2016-08-29 18:30:53', 'wima', 'Wima', null, '<p>As long as we live its you and me baby. There ain\'t nothin\' wrong with that. The movie star the professor and Mary Ann here on Gilligans Isle., They call him Flipper Flipper faster than lightning. No one you see is smarter than he. The year is 1987 and NASA launches the last of Americas deep space probes.</p>', null, null, '1', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('53', '15', '4', '1', '1', '1', 'StaffProfile', '2016-08-29 18:41:36', '2016-08-29 18:30:53', 'wilma', 'Wilma', null, '<p>As long as we live its you and me baby. There ain\'t nothin\' wrong with that. The movie star the professor and Mary Ann here on Gilligans Isle., They call him Flipper Flipper faster than lightning. No one you see is smarter than he. The year is 1987 and NASA launches the last of Americas deep space probes.</p>', null, null, '1', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('54', '16', '1', '0', '1', '0', 'StaffProfile', '2016-08-29 18:49:07', '2016-08-29 18:49:07', 'new-staff-profile', 'New Staff Profile', null, null, null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('55', '16', '2', '1', '1', '1', 'StaffProfile', '2016-08-29 18:49:33', '2016-08-29 18:49:07', 'rosa', 'Rosa', null, '<p>In 1972 a crack commando unit was sent to prison by a military court for a crime they didn\'t commit. These men promptly escaped from a maximum security stockade to the Los Angeles underground. It\'s time to play the music. It\'s time to light the lights. It\'s time to meet the Muppets on the Muppet Show tonight. Straightnin\' the curves.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('56', '13', '5', '1', '1', '1', 'StaffProfile', '2016-08-29 18:49:57', '2016-08-29 17:35:48', 'george', 'George', null, '<p>As long as we live its you and me baby. There ain\'t nothin\' wrong with that. The movie star the professor and Mary Ann here on Gilligans Isle.</p><p>They call him Flipper Flipper faster than lightning. No one you see is smarter than he. The year is 1987 and NASA launches the last of Americas deep space probes.</p>', null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('57', '13', '6', '1', '1', '1', 'StaffProfile', '2016-08-29 18:51:09', '2016-08-29 17:35:48', 'george', 'George', null, '<p>As long as we live its you and me baby. There ain\'t nothin\' wrong with that. The movie star the professor and Mary Ann here on Gilligans Isle.</p><p>They call him Flipper Flipper faster than lightning. No one you see is smarter than he. The year is 1987 and NASA launches the last of Americas deep space probes.</p>', null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('58', '13', '7', '1', '1', '1', 'StaffProfile', '2016-08-29 18:51:33', '2016-08-29 17:35:48', 'george', 'George', null, '<p>As long as we live its you and me baby. There ain\'t nothin\' wrong with that. The movie star the professor and Mary Ann here on Gilligans Isle.</p><p>They call him Flipper Flipper faster than lightning. No one you see is smarter than he. The year is 1987 and NASA launches the last of Americas deep space probes.</p>', null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '10'), ('59', '10', '4', '1', '1', '1', 'StaffHolder', '2016-08-29 18:51:46', '2016-08-24 18:14:55', 'team', 'Team', null, null, null, null, '1', '1', '6', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('60', '1', '6', '1', '1', '1', 'HomePage', '2016-08-30 10:27:56', '2016-04-20 14:28:51', 'home', 'Home', null, null, null, null, '1', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('61', '2', '9', '1', '1', '1', 'Page', '2016-08-30 10:46:53', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">The Wyatt name is synonymous with coffee and tea in New Zealand, dating back to when the Wyatt family purchased Wilcox Tea in 1966.</p><p>We were very English back then, with everyone drinking loose leaf tea which arrived in the country in large tea chests. Coffee was a small side-business at the time, but as the years progressed it became a bigger focus.</p><p>Lance Wyatt saw a need to start a fresh roasted coffee movement here but import duties made it cost prohibitive to roast coffee in the early 80s. So Lance built his own coffee roasting machine in 1983, effectively becoming one of New Zealand’s coffee roasting pioneer’s.</p><p>Aaron grew up in the industry, working for his dad during school holidays, until he joined full-time in 1999 as a coffee machine technician.</p><p>Santos Coffee was founded in 1995 by Brazilian native, Marcelo de Souza. Marcelo started the roastery in Mt. Eden, before opening the iconic Santos Café in Ponsonby, where it was part of the scenery for many years. The roastery was moved to the top of New North Road in Kingsland, until selling Santos to the Wyatts in 2012.</p><p>Santos Coffee continued to be roasted from its Kingsland location until Lance and Aaron found a more public-friendly location in Carlton Gore Road, Newmarket. This was the first time Santos had a building large enough to house the roaster, an espresso bar and café. The idea was, if you weren’t admiring the amazing view of Auckland domain whilst enjoying a great brew, you could melt into the coffee roasting process.</p><p>The doors of our new location opened to the public in April 2015. From here we continue to build on our urban, city coffee brand for coffee aficionados. You can enjoy Santos Coffee at our own and other high-quality cafes around the country.</p><h1>Service</h1><hr><p class=\"lead\">What we offer is proven, premium and Speciality grade award winning coffees.</p><p class=\"lead\">As artisans we know coffee inside-out, and we know what works for different requirements. We offer our cafē and corporate customers the full package: supply, service and support. This ensures you can concentrate on serving great coffee at your cafē or workplace without any of the associated headaches. We serve our customers best by having great products and equipment delivered with awesome service from well-trained people. All of our staff are trained baristas, even our receptionist and warehouse manager. That’s because we all wear multiple hats here. Our team knows every aspect of the coffee business. If someone’s unavailable, someone else has you covered. They can answer any questions you have and make you a great coffee to top it off.</p><p class=\"lead\">Our after sales service is unbelievably good too. It’s really only when something goes wrong that you understand the value of your coffee roaster and what they offer you. should your espresso machine break down, we react within an hour to fix or replace it. It’s this kind of service from a large behind the scenes team that will make all the difference to your business. We can tailor how you deal with us, with either automatic weekly or daily deliveries, or email or phone us with your orders.</p><p class=\"lead\">We’re confident you’ll love Santos Coffee, which is why we won\'t tie you down to a contract. At Santos Coffee we blend everything together to provide you the best coffee experience of your life.</p><p class=\"lead\">Ask us about:</p><ul><li>Santos branded merchandise – signs, shirts, aprons, umbrellas</li>\n<li>Sanremo espresso machines – we think they’re the best espresso machine available</li>\n<li>Barista training</li>\n<li>Our easy ordering system – a one-stop-shop for coffee and consumables</li>\n<li>Technical service</li>\n<li>The best coffees money can buy.</li>\n</ul><h1>Machines</h1><hr><p class=\"lead\">We’ve seen and used a lot of different coffee machines over the decades.</p><p class=\"lead\">We’re proud to use, recommend and represent Sanremo espresso machines and grinders, manufactured in Treviso, Italy. Like us, Sanremo is a family-owned business, with a factory producing outstanding machines. They have a tightknit team who like us are focused on excellence.</p><p class=\"lead\">Aesthetically, Sanremo machines are not only beautiful, but they’re also extremely reliable and easy to use. We have models to suit every size environment, from the small office to the large corporate workplace and busy cafē.</p><p class=\"lead\">If you want reliability, beauty and perfection, choose Sanremo.</p><p class=\"lead\"><img class=\"leftAlone\" title=\"\" src=\"assets/Uploads/_resampled/ResizedImageWzYwMCwyMTRd/sanremo.png\" alt=\"sanremo\" width=\"600\" height=\"214\"></p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('62', '2', '10', '1', '1', '1', 'Page', '2016-08-30 10:49:12', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">The Wyatt name is synonymous with coffee and tea in New Zealand, dating back to when the Wyatt family purchased Wilcox Tea in 1966.</p><p>We were very English back then, with everyone drinking loose leaf tea which arrived in the country in large tea chests. Coffee was a small side-business at the time, but as the years progressed it became a bigger focus.</p><p>Lance Wyatt saw a need to start a fresh roasted coffee movement here but import duties made it cost prohibitive to roast coffee in the early 80s. So Lance built his own coffee roasting machine in 1983, effectively becoming one of New Zealand’s coffee roasting pioneer’s.</p><p>Aaron grew up in the industry, working for his dad during school holidays, until he joined full-time in 1999 as a coffee machine technician.</p><p>Santos Coffee was founded in 1995 by Brazilian native, Marcelo de Souza. Marcelo started the roastery in Mt. Eden, before opening the iconic Santos Café in Ponsonby, where it was part of the scenery for many years. The roastery was moved to the top of New North Road in Kingsland, until selling Santos to the Wyatts in 2012.</p><p>Santos Coffee continued to be roasted from its Kingsland location until Lance and Aaron found a more public-friendly location in Carlton Gore Road, Newmarket. This was the first time Santos had a building large enough to house the roaster, an espresso bar and café. The idea was, if you weren’t admiring the amazing view of Auckland domain whilst enjoying a great brew, you could melt into the coffee roasting process.</p><p>The doors of our new location opened to the public in April 2015. From here we continue to build on our urban, city coffee brand for coffee aficionados. You can enjoy Santos Coffee at our own and other high-quality cafes around the country.</p><h1>Service</h1><hr><p class=\"lead\">What we offer is proven, premium and Speciality grade award winning coffees.</p><p class=\"lead\">As artisans we know coffee inside-out, and we know what works for different requirements. We offer our cafē and corporate customers the full package: supply, service and support. This ensures you can concentrate on serving great coffee at your cafē or workplace without any of the associated headaches. We serve our customers best by having great products and equipment delivered with awesome service from well-trained people. All of our staff are trained baristas, even our receptionist and warehouse manager. That’s because we all wear multiple hats here. Our team knows every aspect of the coffee business. If someone’s unavailable, someone else has you covered. They can answer any questions you have and make you a great coffee to top it off.</p><p class=\"lead\">Our after sales service is unbelievably good too. It’s really only when something goes wrong that you understand the value of your coffee roaster and what they offer you. should your espresso machine break down, we react within an hour to fix or replace it. It’s this kind of service from a large behind the scenes team that will make all the difference to your business. We can tailor how you deal with us, with either automatic weekly or daily deliveries, or email or phone us with your orders.</p><p class=\"lead\">We’re confident you’ll love Santos Coffee, which is why we won\'t tie you down to a contract. At Santos Coffee we blend everything together to provide you the best coffee experience of your life.</p><p class=\"lead\">Ask us about:</p><ul><li>Santos branded merchandise – signs, shirts, aprons, umbrellas</li>\n<li>Sanremo espresso machines – we think they’re the best espresso machine available</li>\n<li>Barista training</li>\n<li>Our easy ordering system – a one-stop-shop for coffee and consumables</li>\n<li>Technical service</li>\n<li>The best coffees money can buy.</li>\n</ul><h1>Machines</h1><hr><p class=\"lead\">We’ve seen and used a lot of different coffee machines over the decades.</p><p class=\"lead\">We’re proud to use, recommend and represent Sanremo espresso machines and grinders, manufactured in Treviso, Italy. Like us, Sanremo is a family-owned business, with a factory producing outstanding machines. They have a tightknit team who like us are focused on excellence.</p><p class=\"lead\">Aesthetically, Sanremo machines are not only beautiful, but they’re also extremely reliable and easy to use. We have models to suit every size environment, from the small office to the large corporate workplace and busy cafē.</p><p class=\"lead\">If you want reliability, beauty and perfection, choose Sanremo.</p><p class=\"lead\"><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/sanremo2.png\" alt=\"sanremo2\" width=\"765\" height=\"251\"></p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('63', '2', '11', '1', '1', '1', 'Page', '2016-08-30 10:57:25', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">The Wyatt name is synonymous with coffee and tea in New Zealand, dating back to when the Wyatt family purchased Wilcox Tea in 1966.</p><p>We were very English back then, with everyone drinking loose leaf tea which arrived in the country in large tea chests. Coffee was a small side-business at the time, but as the years progressed it became a bigger focus.</p><p>Lance Wyatt saw a need to start a fresh roasted coffee movement here but import duties made it cost prohibitive to roast coffee in the early 80s. So Lance built his own coffee roasting machine in 1983, effectively becoming one of New Zealand’s coffee roasting pioneer’s.</p><p>Aaron grew up in the industry, working for his dad during school holidays, until he joined full-time in 1999 as a coffee machine technician.</p><p>Santos Coffee was founded in 1995 by Brazilian native, Marcelo de Souza. Marcelo started the roastery in Mt. Eden, before opening the iconic Santos Café in Ponsonby, where it was part of the scenery for many years. The roastery was moved to the top of New North Road in Kingsland, until selling Santos to the Wyatts in 2012.</p><p>Santos Coffee continued to be roasted from its Kingsland location until Lance and Aaron found a more public-friendly location in Carlton Gore Road, Newmarket. This was the first time Santos had a building large enough to house the roaster, an espresso bar and café. The idea was, if you weren’t admiring the amazing view of Auckland domain whilst enjoying a great brew, you could melt into the coffee roasting process.</p><p>The doors of our new location opened to the public in April 2015. From here we continue to build on our urban, city coffee brand for coffee aficionados. You can enjoy Santos Coffee at our own and other high-quality cafes around the country.</p><h1>Service</h1><hr><p class=\"lead\">What we offer is proven, premium and Speciality grade award winning coffees.</p><p class=\"lead\">As artisans we know coffee inside-out, and we know what works for different requirements. We offer our cafē and corporate customers the full package: supply, service and support. This ensures you can concentrate on serving great coffee at your cafē or workplace without any of the associated headaches. We serve our customers best by having great products and equipment delivered with awesome service from well-trained people. All of our staff are trained baristas, even our receptionist and warehouse manager. That’s because we all wear multiple hats here. Our team knows every aspect of the coffee business. If someone’s unavailable, someone else has you covered. They can answer any questions you have and make you a great coffee to top it off.</p><p class=\"lead\">Our after sales service is unbelievably good too. It’s really only when something goes wrong that you understand the value of your coffee roaster and what they offer you. should your espresso machine break down, we react within an hour to fix or replace it. It’s this kind of service from a large behind the scenes team that will make all the difference to your business. We can tailor how you deal with us, with either automatic weekly or daily deliveries, or email or phone us with your orders.</p><p class=\"lead\">We’re confident you’ll love Santos Coffee, which is why we won\'t tie you down to a contract. At Santos Coffee we blend everything together to provide you the best coffee experience of your life.</p><p class=\"lead\">Ask us about:</p><ul><li>Santos branded merchandise – signs, shirts, aprons, umbrellas</li>\n<li>Sanremo espresso machines – we think they’re the best espresso machine available</li>\n<li>Barista training</li>\n<li>Our easy ordering system – a one-stop-shop for coffee and consumables</li>\n<li>Technical service</li>\n<li>The best coffees money can buy.</li>\n</ul><h1>Machines</h1><hr><p class=\"lead\">We’ve seen and used a lot of different coffee machines over the decades.</p><p class=\"lead\">We’re proud to use, recommend and represent Sanremo espresso machines and grinders, manufactured in Treviso, Italy. Like us, Sanremo is a family-owned business, with a factory producing outstanding machines. They have a tightknit team who like us are focused on excellence.</p><p class=\"lead\">Aesthetically, Sanremo machines are not only beautiful, but they’re also extremely reliable and easy to use. We have models to suit every size environment, from the small office to the large corporate workplace and busy cafē.</p><p class=\"lead\">If you want reliability, beauty and perfection, choose Sanremo.</p><p class=\"lead\"><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/sanremo.jpg\" alt=\"sanremo\"></p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('64', '2', '12', '1', '1', '1', 'Page', '2016-08-30 10:58:21', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">The Wyatt name is synonymous with coffee and tea in New Zealand, dating back to when the Wyatt family purchased Wilcox Tea in 1966.</p><p>We were very English back then, with everyone drinking loose leaf tea which arrived in the country in large tea chests. Coffee was a small side-business at the time, but as the years progressed it became a bigger focus.</p><p>Lance Wyatt saw a need to start a fresh roasted coffee movement here but import duties made it cost prohibitive to roast coffee in the early 80s. So Lance built his own coffee roasting machine in 1983, effectively becoming one of New Zealand’s coffee roasting pioneer’s.</p><p>Aaron grew up in the industry, working for his dad during school holidays, until he joined full-time in 1999 as a coffee machine technician.</p><p>Santos Coffee was founded in 1995 by Brazilian native, Marcelo de Souza. Marcelo started the roastery in Mt. Eden, before opening the iconic Santos Café in Ponsonby, where it was part of the scenery for many years. The roastery was moved to the top of New North Road in Kingsland, until selling Santos to the Wyatts in 2012.</p><p>Santos Coffee continued to be roasted from its Kingsland location until Lance and Aaron found a more public-friendly location in Carlton Gore Road, Newmarket. This was the first time Santos had a building large enough to house the roaster, an espresso bar and café. The idea was, if you weren’t admiring the amazing view of Auckland domain whilst enjoying a great brew, you could melt into the coffee roasting process.</p><p>The doors of our new location opened to the public in April 2015. From here we continue to build on our urban, city coffee brand for coffee aficionados. You can enjoy Santos Coffee at our own and other high-quality cafes around the country.</p><h1>Service</h1><hr><p class=\"lead\">What we offer is proven, premium and Speciality grade award winning coffees.</p><p>As artisans we know coffee inside-out, and we know what works for different requirements. We offer our cafē and corporate customers the full package: supply, service and support. This ensures you can concentrate on serving great coffee at your cafē or workplace without any of the associated headaches. We serve our customers best by having great products and equipment delivered with awesome service from well-trained people. All of our staff are trained baristas, even our receptionist and warehouse manager. That’s because we all wear multiple hats here. Our team knows every aspect of the coffee business. If someone’s unavailable, someone else has you covered. They can answer any questions you have and make you a great coffee to top it off.</p><p>Our after sales service is unbelievably good too. It’s really only when something goes wrong that you understand the value of your coffee roaster and what they offer you. should your espresso machine break down, we react within an hour to fix or replace it. It’s this kind of service from a large behind the scenes team that will make all the difference to your business. We can tailor how you deal with us, with either automatic weekly or daily deliveries, or email or phone us with your orders.</p><p>We’re confident you’ll love Santos Coffee, which is why we won\'t tie you down to a contract. At Santos Coffee we blend everything together to provide you the best coffee experience of your life.</p><p>Ask us about:</p><ul><li>Santos branded merchandise – signs, shirts, aprons, umbrellas</li>\n<li>Sanremo espresso machines – we think they’re the best espresso machine available</li>\n<li>Barista training</li>\n<li>Our easy ordering system – a one-stop-shop for coffee and consumables</li>\n<li>Technical service</li>\n<li>The best coffees money can buy.</li>\n</ul><h1>Machines</h1><hr><p class=\"lead\">We’ve seen and used a lot of different coffee machines over the decades.</p><p>We’re proud to use, recommend and represent Sanremo espresso machines and grinders, manufactured in Treviso, Italy. Like us, Sanremo is a family-owned business, with a factory producing outstanding machines. They have a tightknit team who like us are focused on excellence.</p><p class=\"lead\">Aesthetically, Sanremo machines are not only beautiful, but they’re also extremely reliable and easy to use. We have models to suit every size environment, from the small office to the large corporate workplace and busy cafē.</p><p>If you want reliability, beauty and perfection, choose Sanremo.</p><p class=\"lead\"><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/sanremo.jpg\" alt=\"sanremo\"></p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('65', '2', '13', '1', '1', '1', 'Page', '2016-08-30 10:58:37', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">The Wyatt name is synonymous with coffee and tea in New Zealand, dating back to when the Wyatt family purchased Wilcox Tea in 1966.</p><p>We were very English back then, with everyone drinking loose leaf tea which arrived in the country in large tea chests. Coffee was a small side-business at the time, but as the years progressed it became a bigger focus.</p><p>Lance Wyatt saw a need to start a fresh roasted coffee movement here but import duties made it cost prohibitive to roast coffee in the early 80s. So Lance built his own coffee roasting machine in 1983, effectively becoming one of New Zealand’s coffee roasting pioneer’s.</p><p>Aaron grew up in the industry, working for his dad during school holidays, until he joined full-time in 1999 as a coffee machine technician.</p><p>Santos Coffee was founded in 1995 by Brazilian native, Marcelo de Souza. Marcelo started the roastery in Mt. Eden, before opening the iconic Santos Café in Ponsonby, where it was part of the scenery for many years. The roastery was moved to the top of New North Road in Kingsland, until selling Santos to the Wyatts in 2012.</p><p>Santos Coffee continued to be roasted from its Kingsland location until Lance and Aaron found a more public-friendly location in Carlton Gore Road, Newmarket. This was the first time Santos had a building large enough to house the roaster, an espresso bar and café. The idea was, if you weren’t admiring the amazing view of Auckland domain whilst enjoying a great brew, you could melt into the coffee roasting process.</p><p>The doors of our new location opened to the public in April 2015. From here we continue to build on our urban, city coffee brand for coffee aficionados. You can enjoy Santos Coffee at our own and other high-quality cafes around the country.</p><h1>Service</h1><hr><p class=\"lead\">What we offer is proven, premium and Speciality grade award winning coffees.</p><p>As artisans we know coffee inside-out, and we know what works for different requirements. We offer our cafē and corporate customers the full package: supply, service and support. This ensures you can concentrate on serving great coffee at your cafē or workplace without any of the associated headaches. We serve our customers best by having great products and equipment delivered with awesome service from well-trained people. All of our staff are trained baristas, even our receptionist and warehouse manager. That’s because we all wear multiple hats here. Our team knows every aspect of the coffee business. If someone’s unavailable, someone else has you covered. They can answer any questions you have and make you a great coffee to top it off.</p><p>Our after sales service is unbelievably good too. It’s really only when something goes wrong that you understand the value of your coffee roaster and what they offer you. should your espresso machine break down, we react within an hour to fix or replace it. It’s this kind of service from a large behind the scenes team that will make all the difference to your business. We can tailor how you deal with us, with either automatic weekly or daily deliveries, or email or phone us with your orders.</p><p>We’re confident you’ll love Santos Coffee, which is why we won\'t tie you down to a contract. At Santos Coffee we blend everything together to provide you the best coffee experience of your life.</p><p>Ask us about:</p><ul><li>Santos branded merchandise – signs, shirts, aprons, umbrellas</li>\n<li>Sanremo espresso machines – we think they’re the best espresso machine available</li>\n<li>Barista training</li>\n<li>Our easy ordering system – a one-stop-shop for coffee and consumables</li>\n<li>Technical service</li>\n<li>The best coffees money can buy.</li>\n</ul><h1>Machines</h1><hr><p class=\"lead\">We’ve seen and used a lot of different coffee machines over the decades.</p><p>We’re proud to use, recommend and represent Sanremo espresso machines and grinders, manufactured in Treviso, Italy. Like us, Sanremo is a family-owned business, with a factory producing outstanding machines. They have a tightknit team who like us are focused on excellence.</p><p>Aesthetically, Sanremo machines are not only beautiful, but they’re also extremely reliable and easy to use. We have models to suit every size environment, from the small office to the large corporate workplace and busy cafē.</p><p>If you want reliability, beauty and perfection, choose Sanremo.</p><p class=\"lead\"><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/sanremo.jpg\" alt=\"sanremo\"></p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('66', '2', '14', '1', '1', '1', 'Page', '2016-08-30 10:59:24', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">The Wyatt name is synonymous with coffee and tea in New Zealand, dating back to when the Wyatt family purchased Wilcox Tea in 1966.</p><p>We were very English back then, with everyone drinking loose leaf tea which arrived in the country in large tea chests. Coffee was a small side-business at the time, but as the years progressed it became a bigger focus.</p><p>Lance Wyatt saw a need to start a fresh roasted coffee movement here but import duties made it cost prohibitive to roast coffee in the early 80s. So Lance built his own coffee roasting machine in 1983, effectively becoming one of New Zealand’s coffee roasting pioneer’s.</p><p>Aaron grew up in the industry, working for his dad during school holidays, until he joined full-time in 1999 as a coffee machine technician.</p><p>Santos Coffee was founded in 1995 by Brazilian native, Marcelo de Souza. Marcelo started the roastery in Mt. Eden, before opening the iconic Santos Café in Ponsonby, where it was part of the scenery for many years. The roastery was moved to the top of New North Road in Kingsland, until selling Santos to the Wyatts in 2012.</p><p>Santos Coffee continued to be roasted from its Kingsland location until Lance and Aaron found a more public-friendly location in Carlton Gore Road, Newmarket. This was the first time Santos had a building large enough to house the roaster, an espresso bar and café. The idea was, if you weren’t admiring the amazing view of Auckland domain whilst enjoying a great brew, you could melt into the coffee roasting process.</p><p>The doors of our new location opened to the public in April 2015. From here we continue to build on our urban, city coffee brand for coffee aficionados. You can enjoy Santos Coffee at our own and other high-quality cafes around the country.</p><h1>Service</h1><hr><p class=\"lead\">What we offer is proven, premium and Speciality grade award winning coffees.</p><p>As artisans we know coffee inside-out, and we know what works for different requirements. We offer our cafē and corporate customers the full package: supply, service and support. This ensures you can concentrate on serving great coffee at your cafē or workplace without any of the associated headaches. We serve our customers best by having great products and equipment delivered with awesome service from well-trained people. All of our staff are trained baristas, even our receptionist and warehouse manager. That’s because we all wear multiple hats here. Our team knows every aspect of the coffee business. If someone’s unavailable, someone else has you covered. They can answer any questions you have and make you a great coffee to top it off.</p><p>Our after sales service is unbelievably good too. It’s really only when something goes wrong that you understand the value of your coffee roaster and what they offer you. should your espresso machine break down, we react within an hour to fix or replace it. It’s this kind of service from a large behind the scenes team that will make all the difference to your business. We can tailor how you deal with us, with either automatic weekly or daily deliveries, or email or phone us with your orders.</p><p>We’re confident you’ll love Santos Coffee, which is why we won\'t tie you down to a contract. At Santos Coffee we blend everything together to provide you the best coffee experience of your life.</p><p>Ask us about:</p><ul><li><em>Santos branded merchandise</em> – signs, shirts, aprons, umbrellas</li>\n<li><em>Sanremo espresso machines</em> – we think they’re the best espresso machine available</li>\n<li><em>Barista training</em></li>\n<li><em>Our easy ordering system</em> – a one-stop-shop for coffee and consumables</li>\n<li><em>Technical service</em></li>\n<li><em>The best coffees money can buy</em>.</li>\n</ul><h1>Machines</h1><hr><p class=\"lead\">We’ve seen and used a lot of different coffee machines over the decades.</p><p>We’re proud to use, recommend and represent Sanremo espresso machines and grinders, manufactured in Treviso, Italy. Like us, Sanremo is a family-owned business, with a factory producing outstanding machines. They have a tightknit team who like us are focused on excellence.</p><p>Aesthetically, Sanremo machines are not only beautiful, but they’re also extremely reliable and easy to use. We have models to suit every size environment, from the small office to the large corporate workplace and busy cafē.</p><p>If you want reliability, beauty and perfection, choose Sanremo.</p><p class=\"lead\"><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/sanremo.jpg\" alt=\"sanremo\"></p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('67', '2', '15', '1', '1', '1', 'Page', '2016-08-30 11:49:52', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">The Wyatt name is synonymous with coffee and tea in New Zealand, dating back to when the Wyatt family purchased Wilcox Tea in 1966.</p><p>We were very English back then, with everyone drinking loose leaf tea which arrived in the country in large tea chests. Coffee was a small side-business at the time, but as the years progressed it became a bigger focus.</p><p>Lance Wyatt saw a need to start a fresh roasted coffee movement here but import duties made it cost prohibitive to roast coffee in the early 80s. So Lance built his own coffee roasting machine in 1983, effectively becoming one of New Zealand’s coffee roasting pioneer’s.</p><p>Aaron grew up in the industry, working for his dad during school holidays, until he joined full-time in 1999 as a coffee machine technician.</p><p>Santos Coffee was founded in 1995 by Brazilian native, Marcelo de Souza. Marcelo started the roastery in Mt. Eden, before opening the iconic Santos Café in Ponsonby, where it was part of the scenery for many years. The roastery was moved to the top of New North Road in Kingsland, until selling Santos to the Wyatts in 2012.</p><p>Santos Coffee continued to be roasted from its Kingsland location until Lance and Aaron found a more public-friendly location in Carlton Gore Road, Newmarket. This was the first time Santos had a building large enough to house the roaster, an espresso bar and café. The idea was, if you weren’t admiring the amazing view of Auckland domain whilst enjoying a great brew, you could melt into the coffee roasting process.</p><p>The doors of our new location opened to the public in April 2015. From here we continue to build on our urban, city coffee brand for coffee aficionados. You can enjoy Santos Coffee at our own and other high-quality cafes around the country.</p><h1>Service</h1><hr><p class=\"lead\">What we offer is proven, premium and Speciality grade award winning coffees.</p><p>As artisans we know coffee inside-out, and we know what works for different requirements. We offer our cafē and corporate customers the full package: supply, service and support. This ensures you can concentrate on serving great coffee at your cafē or workplace without any of the associated headaches. We serve our customers best by having great products and equipment delivered with awesome service from well-trained people. All of our staff are trained baristas, even our receptionist and warehouse manager. That’s because we all wear multiple hats here. Our team knows every aspect of the coffee business. If someone’s unavailable, someone else has you covered. They can answer any questions you have and make you a great coffee to top it off.</p><p>Our after sales service is unbelievably good too. It’s really only when something goes wrong that you understand the value of your coffee roaster and what they offer you. should your espresso machine break down, we react within an hour to fix or replace it. It’s this kind of service from a large behind the scenes team that will make all the difference to your business. We can tailor how you deal with us, with either automatic weekly or daily deliveries, or email or phone us with your orders.</p><p>We’re confident you’ll love Santos Coffee, which is why we won\'t tie you down to a contract. At Santos Coffee we blend everything together to provide you the best coffee experience of your life.</p><p>Ask us about:</p><ul><li><em>Santos branded merchandise</em> – signs, shirts, aprons, umbrellas</li>\n<li><em>Sanremo espresso machines</em> – we think they’re the best espresso machine available</li>\n<li><em>Barista training</em></li>\n<li><em>Our easy ordering system</em> – a one-stop-shop for coffee and consumables</li>\n<li><em>Technical service</em></li>\n<li><em>The best coffees money can buy</em>.</li>\n</ul><h1>Machines</h1><hr><p class=\"lead\">We’ve seen and used a lot of different coffee machines over the decades.</p><p>We’re proud to use, recommend and represent Sanremo espresso machines and grinders, manufactured in Treviso, Italy. Like us, Sanremo is a family-owned business, with a factory producing outstanding machines. They have a tightknit team who like us are focused on excellence.</p><p>Aesthetically, Sanremo machines are not only beautiful, but they’re also extremely reliable and easy to use. We have models to suit every size environment, from the small office to the large corporate workplace and busy cafē.</p><p>If you want reliability, beauty and perfection, choose Sanremo.</p><p class=\"lead\"><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/sanremo.jpg\" alt=\"sanremo\"></p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('68', '2', '16', '1', '1', '1', 'Page', '2016-08-30 11:55:32', '2016-04-20 14:28:52', 'about', 'About', null, '<h1>History</h1><hr><p class=\"lead\">The Wyatt name is synonymous with coffee and tea in New Zealand, dating back to when the Wyatt family purchased Wilcox Tea in 1966.</p><p>We were very English back then, with everyone drinking loose leaf tea which arrived in the country in large tea chests. Coffee was a small side-business at the time, but as the years progressed it became a bigger focus.</p><p>Lance Wyatt saw a need to start a fresh roasted coffee movement here but import duties made it cost prohibitive to roast coffee in the early 80s. So Lance built his own coffee roasting machine in 1983, effectively becoming one of New Zealand’s coffee roasting pioneer’s.</p><p>Aaron grew up in the industry, working for his dad during school holidays, until he joined full-time in 1999 as a coffee machine technician.</p><p>Santos Coffee was founded in 1995 by Brazilian native, Marcelo de Souza. Marcelo started the roastery in Mt. Eden, before opening the iconic Santos Café in Ponsonby, where it was part of the scenery for many years. The roastery was moved to the top of New North Road in Kingsland, until selling Santos to the Wyatts in 2012.</p><p>Santos Coffee continued to be roasted from its Kingsland location until Lance and Aaron found a more public-friendly location in Carlton Gore Road, Newmarket. This was the first time Santos had a building large enough to house the roaster, an espresso bar and café. The idea was, if you weren’t admiring the amazing view of Auckland domain whilst enjoying a great brew, you could melt into the coffee roasting process.</p><p>The doors of our new location opened to the public in April 2015. From here we continue to build on our urban, city coffee brand for coffee aficionados. You can enjoy Santos Coffee at our own and other high-quality cafes around the country.</p><h1>Service</h1><hr><p class=\"lead\">What we offer is proven, premium and Speciality grade award winning coffees.</p><p>As artisans we know coffee inside-out, and we know what works for different requirements. We offer our cafē and corporate customers the full package: supply, service and support. This ensures you can concentrate on serving great coffee at your cafē or workplace without any of the associated headaches. We serve our customers best by having great products and equipment delivered with awesome service from well-trained people. All of our staff are trained baristas, even our receptionist and warehouse manager. That’s because we all wear multiple hats here. Our team knows every aspect of the coffee business. If someone’s unavailable, someone else has you covered. They can answer any questions you have and make you a great coffee to top it off.</p><p>Our after sales service is unbelievably good too. It’s really only when something goes wrong that you understand the value of your coffee roaster and what they offer you. should your espresso machine break down, we react within an hour to fix or replace it. It’s this kind of service from a large behind the scenes team that will make all the difference to your business. We can tailor how you deal with us, with either automatic weekly or daily deliveries, or email or phone us with your orders.</p><p>We’re confident you’ll love Santos Coffee, which is why we won\'t tie you down to a contract. At Santos Coffee we blend everything together to provide you the best coffee experience of your life.</p><p>Ask us about:</p><ul><li><em>Santos branded merchandise</em> – signs, shirts, aprons, umbrellas</li>\n<li><em>Sanremo espresso machines</em> – we think they’re the best espresso machine available</li>\n<li><em>Barista training</em></li>\n<li><em>Our easy ordering system</em> – a one-stop-shop for coffee and consumables</li>\n<li><em>Technical service</em></li>\n<li><em>The best coffees money can buy</em>.</li>\n</ul><h1>Machines</h1><hr><p class=\"lead\">We’ve seen and used a lot of different coffee machines over the decades.</p><p>We’re proud to use, recommend and represent Sanremo espresso machines and grinders, manufactured in Treviso, Italy. Like us, Sanremo is a family-owned business, with a factory producing outstanding machines. They have a tightknit team who like us are focused on excellence.</p><p>Aesthetically, Sanremo machines are not only beautiful, but they’re also extremely reliable and easy to use. We have models to suit every size environment, from the small office to the large corporate workplace and busy cafē.</p><p>If you want reliability, beauty and perfection, choose Sanremo.</p><p class=\"lead\"><img class=\"img-responsive\" style=\"margin-left: -30px;\" title=\"\" src=\"assets/Uploads/sanremo.jpg\" alt=\"sanremo\"></p>', null, null, '1', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '0');
INSERT INTO `SiteTree_versions` VALUES ('69', '17', '1', '0', '1', '0', 'BlogPost', '2016-09-01 10:12:16', '2016-09-01 10:12:16', 'new-blog-post', 'New Blog Post', null, null, null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('70', '17', '2', '0', '1', '0', 'BlogPost', '2016-09-01 10:12:56', '2016-09-01 10:12:16', 'pouring-the-perfect-cup', 'Pouring the perfect cup', null, '<p>Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. So this is the tale of our castaways they\'re here for a long long time. They\'ll have to make the best of things its an uphill climb. Maybe you and me were never meant to be.</p><p>But baby think of me once in awhile. I\'m at WKRP in Cincinnati? It\'s a beautiful day in this neighborhood a beautiful day for a neighbor. Would you be mine? Could you be mine? Its a neighborly day in this beautywood a neighborly day for a beauty. Would you be mine? Could you be mine. Boy the way Glen Miller played. Songs that made the hit parade. Guys like us we had it made. Those were the days.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('71', '17', '3', '1', '1', '1', 'BlogPost', '2016-09-01 10:12:56', '2016-09-01 10:12:16', 'pouring-the-perfect-cup', 'Pouring the perfect cup', null, '<p>Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. So this is the tale of our castaways they\'re here for a long long time. They\'ll have to make the best of things its an uphill climb. Maybe you and me were never meant to be.</p><p>But baby think of me once in awhile. I\'m at WKRP in Cincinnati? It\'s a beautiful day in this neighborhood a beautiful day for a neighbor. Would you be mine? Could you be mine? Its a neighborly day in this beautywood a neighborly day for a beauty. Would you be mine? Could you be mine. Boy the way Glen Miller played. Songs that made the hit parade. Guys like us we had it made. Those were the days.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('72', '18', '1', '0', '1', '0', 'BlogPost', '2016-09-01 10:13:04', '2016-09-01 10:13:04', 'new-blog-post', 'New Blog Post', null, null, null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('73', '18', '2', '0', '1', '0', 'BlogPost', '2016-09-01 10:13:30', '2016-09-01 10:13:04', 'pouring-the-perfect-cup-2', 'Pouring the perfect cup 2', null, '<p>Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. So this is the tale of our castaways they\'re here for a long long time. They\'ll have to make the best of things its an uphill climb. Maybe you and me were never meant to be. But baby think of me once in awhile. I\'m at WKRP in Cincinnati?</p><p>It\'s a beautiful day in this neighborhood a beautiful day for a neighbor. Would you be mine? Could you be mine? Its a neighborly day in this beautywood a neighborly day for a beauty. Would you be mine? Could you be mine. Boy the way Glen Miller played. Songs that made the hit parade. Guys like us we had it made. Those were the days.</p><p> </p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('74', '18', '3', '1', '1', '1', 'BlogPost', '2016-09-01 10:13:30', '2016-09-01 10:13:04', 'pouring-the-perfect-cup-2', 'Pouring the perfect cup 2', null, '<p>Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. So this is the tale of our castaways they\'re here for a long long time. They\'ll have to make the best of things its an uphill climb. Maybe you and me were never meant to be. But baby think of me once in awhile. I\'m at WKRP in Cincinnati?</p><p>It\'s a beautiful day in this neighborhood a beautiful day for a neighbor. Would you be mine? Could you be mine? Its a neighborly day in this beautywood a neighborly day for a beauty. Would you be mine? Could you be mine. Boy the way Glen Miller played. Songs that made the hit parade. Guys like us we had it made. Those were the days.</p><p> </p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('75', '19', '1', '0', '1', '0', 'BlogPost', '2016-09-01 10:13:37', '2016-09-01 10:13:37', 'new-blog-post', 'New Blog Post', null, null, null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('76', '19', '2', '0', '1', '0', 'BlogPost', '2016-09-01 10:14:03', '2016-09-01 10:13:37', 'pouring-the-perfect-cup-3', 'Pouring the perfect cup 3', null, '<p>Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. So this is the tale of our castaways they\'re here for a long long time. They\'ll have to make the best of things its an uphill climb.</p><p>Maybe you and me were never meant to be. But baby think of me once in awhile. I\'m at WKRP in Cincinnati? It\'s a beautiful day in this neighborhood a beautiful day for a neighbor. Would you be mine? Could you be mine?</p><p>Its a neighborly day in this beautywood a neighborly day for a beauty. Would you be mine? Could you be mine. Boy the way Glen Miller played. Songs that made the hit parade. Guys like us we had it made. Those were the days.</p><p> </p>', null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('77', '19', '3', '1', '1', '1', 'BlogPost', '2016-09-01 10:14:03', '2016-09-01 10:13:37', 'pouring-the-perfect-cup-3', 'Pouring the perfect cup 3', null, '<p>Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. So this is the tale of our castaways they\'re here for a long long time. They\'ll have to make the best of things its an uphill climb.</p><p>Maybe you and me were never meant to be. But baby think of me once in awhile. I\'m at WKRP in Cincinnati? It\'s a beautiful day in this neighborhood a beautiful day for a neighbor. Would you be mine? Could you be mine?</p><p>Its a neighborly day in this beautywood a neighborly day for a beauty. Would you be mine? Could you be mine. Boy the way Glen Miller played. Songs that made the hit parade. Guys like us we had it made. Those were the days.</p><p> </p>', null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('78', '12', '3', '1', '1', '1', 'Blog', '2016-09-01 10:19:47', '2016-08-24 18:15:23', 'news', 'News', null, '<p class=\"lead\">This is where we’ll keep you posted about what’s happening at SANTOS and the wonderful world of coffee.</p>', null, null, '1', '1', '8', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('79', '12', '4', '1', '1', '1', 'Blog', '2016-09-01 10:26:22', '2016-08-24 18:15:23', 'news', 'News', null, '<p class=\"lead\">This is where we’ll keep you posted about what’s happening at SANTOS and the wonderful world of coffee.</p>', null, null, '1', '1', '8', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('80', '20', '1', '0', '1', '0', 'BlogPost', '2016-09-01 10:42:30', '2016-09-01 10:42:30', 'new-blog-post', 'New Blog Post', null, null, null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('81', '20', '2', '0', '1', '0', 'BlogPost', '2016-09-01 10:43:03', '2016-09-01 10:42:30', 'pouring-the-perfect-cup-4', 'Pouring the perfect cup 4', null, '<p><span>Maybe you and me were never meant to be. But baby think of me once in awhile. </span></p><p><span>I\'m at WKRP in Cincinnati. Straightnin\' the curves. Flatnin\' the hills Someday the mountain might get ‘em but the law never will? And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Flying away on a wing and a prayer. </span></p><p><span>Who could it be? Believe it or not its just me.</span></p>', null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('82', '20', '3', '1', '1', '1', 'BlogPost', '2016-09-01 10:43:03', '2016-09-01 10:42:30', 'pouring-the-perfect-cup-4', 'Pouring the perfect cup 4', null, '<p><span>Maybe you and me were never meant to be. But baby think of me once in awhile. </span></p><p><span>I\'m at WKRP in Cincinnati. Straightnin\' the curves. Flatnin\' the hills Someday the mountain might get ‘em but the law never will? And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Flying away on a wing and a prayer. </span></p><p><span>Who could it be? Believe it or not its just me.</span></p>', null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('83', '21', '1', '0', '1', '0', 'BlogPost', '2016-09-01 10:47:34', '2016-09-01 10:47:34', 'new-blog-post', 'New Blog Post', null, null, null, null, '0', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('84', '21', '2', '0', '1', '0', 'BlogPost', '2016-09-01 10:47:59', '2016-09-01 10:47:34', 'a-new-news-item', 'A new news item', null, '<p>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</p><p><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></p>', null, null, '0', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('85', '21', '3', '1', '1', '1', 'BlogPost', '2016-09-01 10:47:59', '2016-09-01 10:47:34', 'a-new-news-item', 'A new news item', null, '<p>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</p><p><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></p>', null, null, '0', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('86', '22', '1', '0', '1', '0', 'BlogPost', '2016-09-01 10:48:03', '2016-09-01 10:48:03', 'new-blog-post', 'New Blog Post', null, null, null, null, '0', '1', '6', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('87', '22', '2', '0', '1', '0', 'BlogPost', '2016-09-01 10:48:46', '2016-09-01 10:48:03', 'another-new-item-of-news', 'Another new item of news', null, '<p><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></p><p><span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></span></p>', null, null, '0', '1', '6', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('88', '22', '3', '1', '1', '1', 'BlogPost', '2016-09-01 10:48:46', '2016-09-01 10:48:03', 'another-new-item-of-news', 'Another new item of news', null, '<p><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></p><p><span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></span></p>', null, null, '0', '1', '6', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('89', '12', '5', '1', '1', '1', 'Blog', '2016-09-01 11:05:20', '2016-08-24 18:15:23', 'news', 'News', null, '<p class=\"lead\">This is where we’ll keep you posted about what’s happening at SANTOS and the wonderful world of coffee.</p>', null, null, '1', '1', '8', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('90', '12', '6', '1', '1', '1', 'Blog', '2016-09-01 11:45:01', '2016-08-24 18:15:23', 'news', 'News', null, '<p class=\"lead\">This is where we’ll keep you posted about what’s happening at SANTOS and the wonderful world of coffee.</p>', null, null, '1', '1', '8', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('91', '23', '1', '0', '1', '0', 'BlogPost', '2016-09-01 11:45:07', '2016-09-01 11:45:07', 'new-blog-post', 'New Blog Post', null, null, null, null, '0', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('92', '23', '2', '0', '1', '0', 'BlogPost', '2016-09-01 11:45:55', '2016-09-01 11:45:07', 'maybe-you-and-me-were-never-meant-to-be', 'Maybe you and me were never meant to be', null, '<p><span>But baby think of me once in awhile. I\'m at WKRP in Cincinnati. Straightnin\' the curves. Flatnin\' the hills Someday the mountain might get ‘em but the law never will? </span></p>', null, null, '0', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('93', '23', '3', '1', '1', '1', 'BlogPost', '2016-09-01 11:45:55', '2016-09-01 11:45:07', 'maybe-you-and-me-were-never-meant-to-be', 'Maybe you and me were never meant to be', null, '<p><span>But baby think of me once in awhile. I\'m at WKRP in Cincinnati. Straightnin\' the curves. Flatnin\' the hills Someday the mountain might get ‘em but the law never will? </span></p>', null, null, '0', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('94', '12', '7', '1', '1', '1', 'Blog', '2016-09-01 11:57:00', '2016-08-24 18:15:23', 'news', 'News', null, '<p class=\"lead\">This is where we’ll keep you posted about what’s happening at SANTOS and the wonderful world of coffee.</p>', null, null, '1', '1', '8', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('95', '23', '4', '1', '1', '1', 'BlogPost', '2016-09-01 13:24:53', '2016-09-01 11:45:07', 'maybe-you-and-me-were-never-meant-to-be', 'Maybe you and me were never meant to be', null, '<p><span>But baby think of me once in awhile. I\'m at WKRP in Cincinnati. Straightnin\' the curves. Flatnin\' the hills Someday the mountain might get ‘em but the law never will? </span></p>', null, null, '0', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('96', '24', '1', '1', '1', '1', 'AccountPage', '2016-09-01 14:12:40', '2016-09-01 14:12:40', 'account', 'Account', null, null, null, null, '0', '1', '12', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('97', '25', '1', '1', '1', '1', 'CartPage', '2016-09-01 14:12:40', '2016-09-01 14:12:40', 'cart', 'Shopping Cart', null, null, null, null, '0', '1', '13', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('98', '26', '1', '1', '1', '1', 'CheckoutPage', '2016-09-01 14:12:40', '2016-09-01 14:12:40', 'checkout', 'Checkout', null, null, null, null, '0', '1', '14', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('99', '27', '1', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:33', '2016-09-01 14:16:33', 'shop-2', 'Shop', null, '<p>This is the top level products category, it uses the <em>product category</em> page type. By default it displays prodcuts within this category, and all categories below it. You can nest other product categories inside it.</p>', null, null, '1', '1', '15', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('100', '28', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:33', '2016-09-01 14:16:33', 'electronics', 'Electronics', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('101', '28', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:33', '2016-09-01 14:16:33', 'electronics', 'Electronics', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '27'), ('102', '29', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:33', '2016-09-01 14:16:33', 'food', 'Food', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('103', '29', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:33', '2016-09-01 14:16:33', 'food', 'Food', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '27'), ('104', '30', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:33', '2016-09-01 14:16:33', 'beverages', 'Beverages', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('105', '30', '2', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:33', '2016-09-01 14:16:33', 'beverages', 'Beverages', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '27'), ('106', '31', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'apparel', 'Apparel', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('107', '31', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'apparel', 'Apparel', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '27'), ('108', '32', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'homewares', 'Homewares', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('109', '32', '2', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'homewares', 'Homewares', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '27'), ('110', '33', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'jewellery', 'Jewellery', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('111', '33', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'jewellery', 'Jewellery', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '31'), ('112', '34', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'books', 'Books', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('113', '34', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'books', 'Books', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '27'), ('114', '35', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'toys', 'Toys', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('115', '35', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'toys', 'Toys', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '27'), ('116', '36', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'furniture', 'Furniture', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('117', '36', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'furniture', 'Furniture', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '27'), ('118', '37', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'kitchen', 'Kitchen', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('119', '37', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'kitchen', 'Kitchen', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '36'), ('120', '38', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'bedroom', 'Bedroom', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('121', '38', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'bedroom', 'Bedroom', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '36'), ('122', '39', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'ebooks', 'E-Books', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('123', '39', '2', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'ebooks', 'E-Books', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '34'), ('124', '40', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'entertainment', 'Entertainment', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('125', '40', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'entertainment', 'Entertainment', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '27'), ('126', '41', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'music', 'Music', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('127', '41', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'music', 'Music', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '40'), ('128', '42', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'movies', 'Movies', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('129', '42', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'movies', 'Movies', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '40'), ('130', '43', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'drama', 'Drama', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('131', '43', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'drama', 'Drama', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '42'), ('132', '44', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'automotive', 'Automotive', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('133', '44', '2', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'automotive', 'Automotive', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '27'), ('134', '45', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'flowers', 'Flowers', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('135', '45', '2', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'flowers', 'Flowers', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '27'), ('136', '46', '1', '0', '1', '0', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'stationery', 'Stationery', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('137', '46', '2', '1', '1', '1', 'ProductCategory', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'stationery', 'Stationery', null, null, null, null, '1', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '27'), ('138', '47', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'socks', 'Socks', null, '<p>This sock has cushioned sole for extra comfort with comfortable stay-up top. Our patented technology means quality you can trust.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('139', '47', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'socks', 'Socks', null, '<p>This sock has cushioned sole for extra comfort with comfortable stay-up top. Our patented technology means quality you can trust.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '31'), ('140', '48', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'hdtv', '42inch 1080p LCD TV - 16:9 - HDTV', null, '<p>Whatever room, whatever light, LGs 42 CS560 LCD television adjusts its picture to look crisp, clear, and glare-free! Its sharp pixel resolution at 1920 x 1080p yields a detailed picture while its fast refresh rate at 60 Hz ensures you wont miss a play in the basketball game. Surround sound matches that quality picture with quality sound.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('141', '48', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'hdtv', '42inch 1080p LCD TV - 16:9 - HDTV', null, '<p>Whatever room, whatever light, LGs 42 CS560 LCD television adjusts its picture to look crisp, clear, and glare-free! Its sharp pixel resolution at 1920 x 1080p yields a detailed picture while its fast refresh rate at 60 Hz ensures you wont miss a play in the basketball game. Surround sound matches that quality picture with quality sound.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '28'), ('142', '49', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'beach-ball', 'Beach Ball', null, '<p>Branded 12 inch Beach Ball. Multicolor vinyl ball for Beach and outdoor play</p> <p>NOTE: The manufacture calls this a 12 inch circumference ball. When fully inflated it is around 3 inches smaller.<br> For best results, do not over inflate.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('143', '49', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:34', '2016-09-01 14:16:34', 'beach-ball', 'Beach Ball', null, '<p>Branded 12 inch Beach Ball. Multicolor vinyl ball for Beach and outdoor play</p> <p>NOTE: The manufacture calls this a 12 inch circumference ball. When fully inflated it is around 3 inches smaller.<br> For best results, do not over inflate.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '35'), ('144', '50', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'mp3-player', 'Apple iPod touch 8GB (4th Generation) - Black', null, '<p>The world\'s most popular portable gaming device is even more fun. Now available in black and white, iPod touch includes iOS 5 with over 200 new features, like iMessage, Notification Center, and Twitter integration. Send free, unlimited text messages over Wi-Fi with iMessage. Record HD video and make FaceTime calls. Visit the App Store to choose from over 500,000 apps. iPod touch also features iCloud, which stores your music, photos, apps, and more', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('145', '50', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'mp3-player', 'Apple iPod touch 8GB (4th Generation) - Black', null, '<p>The world\'s most popular portable gaming device is even more fun. Now available in black and white, iPod touch includes iOS 5 with over 200 new features, like iMessage, Notification Center, and Twitter integration. Send free, unlimited text messages over Wi-Fi with iMessage. Record HD video and make FaceTime calls. Visit the App Store to choose from over 500,000 apps. iPod touch also features iCloud, which stores your music, photos, apps, and more', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '28'), ('146', '51', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'generic-movie', 'Generic Movie', null, '<p>The most generic meta-movie you\'ll ever watch. Inspired by every drama movie ever made. <a href=\"http://www.youtube.com/watch?v=AcD292xrpjY\">Watch trailer</a></p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('147', '51', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'generic-movie', 'Generic Movie', null, '<p>The most generic meta-movie you\'ll ever watch. Inspired by every drama movie ever made. <a href=\"http://www.youtube.com/watch?v=AcD292xrpjY\">Watch trailer</a></p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '43'), ('148', '52', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'lemon-chicken', 'Lemon Chicken', null, '<p>Boneless, skinless chicken breast cutlets marinated in a delicious lemon herbal sauce.</p> <p>After cooking, try slicing and adding to your salad.</p> <h3>Cooking Instructions</h3> <p>(From frozen): Grill or saute approximately 6 minutes per side or bake 25 minutes at 400°.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('149', '52', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'lemon-chicken', 'Lemon Chicken', null, '<p>Boneless, skinless chicken breast cutlets marinated in a delicious lemon herbal sauce.</p> <p>After cooking, try slicing and adding to your salad.</p> <h3>Cooking Instructions</h3> <p>(From frozen): Grill or saute approximately 6 minutes per side or bake 25 minutes at 400°.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '29'), ('150', '53', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'the-lion-the-witch-and-the-wardrobe', 'The Lion, the Witch and the Wardrobe', null, '<p>They open a door and enter a world.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('151', '53', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'the-lion-the-witch-and-the-wardrobe', 'The Lion, the Witch and the Wardrobe', null, '<p>They open a door and enter a world.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '34'), ('152', '54', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'desk-lamp', 'Desk Lamp', null, '<p>Product materials: Shade: Paper Tube/ Base plate: Steel, Brush finish nickel-plated, Clear lacquer</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('153', '54', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'desk-lamp', 'Desk Lamp', null, '<p>Product materials: Shade: Paper Tube/ Base plate: Steel, Brush finish nickel-plated, Clear lacquer</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '38'), ('154', '55', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'round-diamond-solitaire-ring-in-14k-white-gold', 'Round Diamond Solitaire Ring in 14K White Gold', null, '<p>Say I love you with this stunning piece that will stun you if you are not careful.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('155', '55', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'round-diamond-solitaire-ring-in-14k-white-gold', 'Round Diamond Solitaire Ring in 14K White Gold', null, '<p>Say I love you with this stunning piece that will stun you if you are not careful.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '33'), ('156', '56', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'kite', 'Kite', null, null, null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('157', '56', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'kite', 'Kite', null, null, null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '35'), ('158', '57', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'hula-hoop', 'Hula Hoop', null, null, null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('159', '57', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'hula-hoop', 'Hula Hoop', null, null, null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '35'), ('160', '58', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'paper-roll', 'Paper Roll', null, null, null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('161', '58', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'paper-roll', 'Paper Roll', null, null, null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '46'), ('162', '59', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'ball-point-pens-7-pack', 'Ball Point Pens (7 pack)', null, null, null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('163', '59', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 'ball-point-pens-7-pack', 'Ball Point Pens (7 pack)', null, null, null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '46'), ('164', '60', '1', '0', '1', '0', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 't-shirt', 'T-Shirt', null, '<p>This popular t-shirt could make you popular.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('165', '60', '2', '1', '1', '1', 'Product', '2016-09-01 14:16:35', '2016-09-01 14:16:35', 't-shirt', 'T-Shirt', null, '<p>This popular t-shirt could make you popular.</p>', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '31'), ('166', '61', '1', '1', '1', '1', 'Page', '2016-09-01 14:16:37', '2016-09-01 14:16:37', 'terms-and-conditions', 'Terms and Conditions', 'Terms & Conditions', 'You agree to...', null, null, '0', '1', '16', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('167', '8', '3', '1', '1', '1', 'ProductCategory', '2016-09-02 07:36:48', '2016-08-24 18:14:27', 'shop', 'Shop', null, null, null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('168', '62', '1', '0', '1', '0', 'Product', '2016-09-02 07:38:14', '2016-09-02 07:38:14', 'new-product', 'New Product', null, null, null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('169', '62', '2', '1', '1', '1', 'Product', '2016-09-02 07:38:52', '2016-09-02 07:38:14', 'marcelos-blend', 'Marcelo\'s Blend', null, '<p>Marcelo De Souza’s original Santos blend. This is a Rainforest Alliance certified coffee, using 100% Brazilian beans blended from four different estates. It’s medium roasted to produce a smooth velvety body with a distinct nutty finish. Low acidity.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('170', '63', '1', '0', '1', '0', 'Product', '2016-09-02 07:49:56', '2016-09-02 07:49:56', 'new-product', 'New Product', null, null, null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('171', '64', '1', '0', '1', '0', 'Product', '2016-09-02 07:49:56', '2016-09-02 07:49:56', 'new-product-2', 'New Product', null, null, null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('172', '63', '2', '1', '1', '1', 'Product', '2016-09-02 07:50:40', '2016-09-02 07:49:56', 'city-blend', 'City Blend', null, '<p>Developed with the flat white in mind, this blend perfectly cuts through the milk-based drinks that Kiwi coffee drinkers love so much. This is a four origin blend roasted medium-dark, giving rich, warm, chocolate flavour notes. Medium acidity.</p><p>Our City Blend was voted one of the best Flat White blends in NZ, and awarded bronze at the 2013 NZ Coffee Awards.</p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('173', '62', '3', '1', '1', '1', 'Product', '2016-09-02 08:03:06', '2016-09-02 07:38:14', 'marcelos-blend', 'Marcelo\'s Blend', null, '<p>Marcelo De Souza’s original Santos blend. This is a Rainforest Alliance certified coffee, using 100% Brazilian beans blended from four different estates. It’s medium roasted to produce a smooth velvety body with a distinct nutty finish. Low acidity.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('174', '63', '3', '1', '1', '1', 'Product', '2016-09-02 08:03:23', '2016-09-02 07:49:56', 'city-blend', 'City Blend', null, '<p>Developed with the flat white in mind, this blend perfectly cuts through the milk-based drinks that Kiwi coffee drinkers love so much. This is a four origin blend roasted medium-dark, giving rich, warm, chocolate flavour notes. Medium acidity.</p><p>Our City Blend was voted one of the best Flat White blends in NZ, and awarded bronze at the 2013 NZ Coffee Awards.</p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('175', '64', '2', '1', '1', '1', 'Product', '2016-09-02 08:03:45', '2016-09-02 07:49:56', 'new-northern', 'New Northern', null, null, null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('176', '62', '4', '1', '1', '1', 'Product', '2016-09-02 08:04:33', '2016-09-02 07:38:14', 'marcelos-blend', 'Marcelo\'s Blend', null, '<p>Marcelo De Souza’s original Santos blend. This is a Rainforest Alliance certified coffee, using 100% Brazilian beans blended from four different estates. It’s medium roasted to produce a smooth velvety body with a distinct nutty finish. Low acidity.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('177', '65', '1', '0', '1', '0', 'Product', '2016-09-02 08:06:32', '2016-09-02 08:06:32', 'santos-organic', 'Santos Organic', null, null, null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('178', '65', '2', '1', '1', '1', 'Product', '2016-09-02 08:06:32', '2016-09-02 08:06:32', 'santos-organic', 'Santos Organic', null, '<p>A blend of three Fairtrade organic coffees, medium roasted producing vanilla aromas with silky honey and caramel notes. Medium acidity.</p>', null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('179', '65', '3', '1', '1', '1', 'Product', '2016-09-02 08:06:43', '2016-09-02 08:06:32', 'santos-organic', 'Santos Organic', null, '<p>A blend of three Fairtrade organic coffees, medium roasted producing vanilla aromas with silky honey and caramel notes. Medium acidity.</p>', null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('180', '66', '1', '0', '1', '0', 'Product', '2016-09-02 08:07:32', '2016-09-02 08:07:32', 'decaf', 'Decaf', null, null, null, null, '0', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('181', '66', '2', '1', '1', '1', 'Product', '2016-09-02 08:07:32', '2016-09-02 08:07:32', 'decaf', 'Decaf', null, '<p>Our decaffeinated coffee is a Swiss Water® Brazilian bean which is 99.9% caffeine-free and 100% chemical-free. It\'s decaffeinated coffee without compromise.</p><p>The unique SWISS WATER® process uses pure water from British Columbia, Canada to gently remove the caffeine until the coffee beans are 99.9% caffeine-free, while maintaining the bean\'s distinctive origin and flavour characteristics.</p>', null, null, '0', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('182', '63', '4', '1', '1', '1', 'Product', '2016-09-02 08:30:06', '2016-09-02 07:49:56', 'city-blend', 'City Blend', null, '<p>Developed with the flat white in mind, this blend perfectly cuts through the milk-based drinks that Kiwi coffee drinkers love so much. This is a four origin blend roasted medium-dark, giving rich, warm, chocolate flavour notes. Medium acidity.</p><p>Our City Blend was voted one of the best Flat White blends in NZ, and awarded bronze at the 2013 NZ Coffee Awards.</p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('183', '66', '3', '1', '1', '1', 'Product', '2016-09-02 08:30:25', '2016-09-02 08:07:32', 'decaf', 'Decaf', null, '<p>Our decaffeinated coffee is a Swiss Water® Brazilian bean which is 99.9% caffeine-free and 100% chemical-free. It\'s decaffeinated coffee without compromise.</p><p>The unique SWISS WATER® process uses pure water from British Columbia, Canada to gently remove the caffeine until the coffee beans are 99.9% caffeine-free, while maintaining the bean\'s distinctive origin and flavour characteristics.</p>', null, null, '0', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('184', '62', '5', '1', '1', '1', 'Product', '2016-09-02 08:30:34', '2016-09-02 07:38:14', 'marcelos-blend', 'Marcelo\'s Blend', null, '<p>Marcelo De Souza’s original Santos blend. This is a Rainforest Alliance certified coffee, using 100% Brazilian beans blended from four different estates. It’s medium roasted to produce a smooth velvety body with a distinct nutty finish. Low acidity.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('185', '64', '3', '1', '1', '1', 'Product', '2016-09-02 08:30:48', '2016-09-02 07:49:56', 'new-northern', 'New Northern', null, null, null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('186', '65', '4', '1', '1', '1', 'Product', '2016-09-02 08:30:58', '2016-09-02 08:06:32', 'santos-organic', 'Santos Organic', null, '<p>A blend of three Fairtrade organic coffees, medium roasted producing vanilla aromas with silky honey and caramel notes. Medium acidity.</p>', null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('187', '8', '4', '1', '1', '1', 'ProductCategory', '2016-09-02 10:26:44', '2016-08-24 18:14:27', 'shop', 'Shop', null, '<p>Roastery</p><p>We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('188', '8', '5', '1', '1', '1', 'ProductCategory', '2016-09-02 10:27:35', '2016-08-24 18:14:27', 'shop', 'Shop', null, '<h1>Roastery</h1><p> </p><p>We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('189', '8', '6', '1', '1', '1', 'ProductCategory', '2016-09-02 10:27:57', '2016-08-24 18:14:27', 'shop', 'Shop', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('190', '8', '7', '1', '1', '1', 'ProductCategory', '2016-09-02 10:29:27', '2016-08-24 18:14:27', 'shop', 'Shop', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img src=\"assets/Uploads/roasted-in-nz-blue.svg\" width=\"125\" alt=\"\" title=\"\"></p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('191', '8', '8', '1', '1', '1', 'ProductCategory', '2016-09-02 10:31:41', '2016-08-24 18:14:27', 'shop', 'Shop', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p> </p><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour. A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround. We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done. The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste. We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('192', '8', '9', '1', '1', '1', 'ProductCategory', '2016-09-02 10:32:00', '2016-08-24 18:14:27', 'shop', 'Shop', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour. A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround. We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done. The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste. We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('193', '8', '10', '1', '1', '1', 'ProductCategory', '2016-09-02 10:33:03', '2016-08-24 18:14:27', 'shop', 'Shop', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('194', '62', '6', '1', '1', '1', 'Product', '2016-09-02 10:37:30', '2016-09-02 07:38:14', 'marcelos-blend', 'Marcelo\'s Blend', null, '<p>Marcelo De Souza’s original Santos blend. This is a Rainforest Alliance certified coffee, using 100% Brazilian beans blended from four different estates. It’s medium roasted to produce a smooth velvety body with a distinct nutty finish. Low acidity.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('195', '63', '5', '1', '1', '1', 'Product', '2016-09-02 10:37:47', '2016-09-02 07:49:56', 'city-blend', 'City Blend', null, '<p>Developed with the flat white in mind, this blend perfectly cuts through the milk-based drinks that Kiwi coffee drinkers love so much. This is a four origin blend roasted medium-dark, giving rich, warm, chocolate flavour notes. Medium acidity.</p><p>Our City Blend was voted one of the best Flat White blends in NZ, and awarded bronze at the 2013 NZ Coffee Awards.</p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('196', '64', '4', '1', '1', '1', 'Product', '2016-09-02 10:38:17', '2016-09-02 07:49:56', 'new-northern', 'New Northern', null, null, null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('197', '65', '5', '1', '1', '1', 'Product', '2016-09-02 10:38:35', '2016-09-02 08:06:32', 'santos-organic', 'Santos Organic', null, '<p>A blend of three Fairtrade organic coffees, medium roasted producing vanilla aromas with silky honey and caramel notes. Medium acidity.</p>', null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('198', '66', '4', '1', '1', '1', 'Product', '2016-09-02 10:38:56', '2016-09-02 08:07:32', 'decaf', 'Decaf', null, '<p>Our decaffeinated coffee is a Swiss Water® Brazilian bean which is 99.9% caffeine-free and 100% chemical-free. It\'s decaffeinated coffee without compromise.</p><p>The unique SWISS WATER® process uses pure water from British Columbia, Canada to gently remove the caffeine until the coffee beans are 99.9% caffeine-free, while maintaining the bean\'s distinctive origin and flavour characteristics.</p>', null, null, '0', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('199', '8', '11', '1', '1', '1', 'ProductCategory', '2016-09-02 10:57:03', '2016-08-24 18:14:27', 'shop', 'Shop', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('200', '8', '12', '1', '1', '1', 'ProductCategory', '2016-09-02 10:57:13', '2016-08-24 18:14:27', 'shop', 'Shop', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('201', '8', '13', '1', '1', '1', 'ProductCategory', '2016-09-02 11:05:52', '2016-08-24 18:14:27', 'shop', 'Shop', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('202', '9', '3', '1', '1', '1', 'Page', '2016-09-02 11:43:22', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '0');
INSERT INTO `SiteTree_versions` VALUES ('203', '9', '4', '1', '1', '1', 'CoffeePage', '2016-09-02 11:48:56', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('204', '9', '5', '1', '1', '1', 'CoffeePage', '2016-09-02 11:49:23', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('205', '63', '6', '0', '1', '0', 'Product', '2016-09-02 12:00:46', '2016-09-02 07:49:56', 'city-blend', 'City Blend', null, '<p>Developed with the flat white in mind, this blend perfectly cuts through the milk-based drinks that Kiwi coffee drinkers love so much. This is a four origin blend roasted medium-dark, giving rich, warm, chocolate flavour notes. Medium acidity.</p><p>Our City Blend was voted one of the best Flat White blends in NZ, and awarded bronze at the 2013 NZ Coffee Awards.</p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '62'), ('206', '63', '7', '0', '1', '0', 'Product', '2016-09-02 12:00:46', '2016-09-02 07:49:56', 'city-blend', 'City Blend', null, '<p>Developed with the flat white in mind, this blend perfectly cuts through the milk-based drinks that Kiwi coffee drinkers love so much. This is a four origin blend roasted medium-dark, giving rich, warm, chocolate flavour notes. Medium acidity.</p><p>Our City Blend was voted one of the best Flat White blends in NZ, and awarded bronze at the 2013 NZ Coffee Awards.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '62'), ('207', '63', '8', '0', '1', '0', 'Product', '2016-09-02 12:00:50', '2016-09-02 07:49:56', 'city-blend', 'City Blend', null, '<p>Developed with the flat white in mind, this blend perfectly cuts through the milk-based drinks that Kiwi coffee drinkers love so much. This is a four origin blend roasted medium-dark, giving rich, warm, chocolate flavour notes. Medium acidity.</p><p>Our City Blend was voted one of the best Flat White blends in NZ, and awarded bronze at the 2013 NZ Coffee Awards.</p>', null, null, '0', '1', '1', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('208', '63', '9', '0', '1', '0', 'Product', '2016-09-02 12:00:50', '2016-09-02 07:49:56', 'city-blend', 'City Blend', null, '<p>Developed with the flat white in mind, this blend perfectly cuts through the milk-based drinks that Kiwi coffee drinkers love so much. This is a four origin blend roasted medium-dark, giving rich, warm, chocolate flavour notes. Medium acidity.</p><p>Our City Blend was voted one of the best Flat White blends in NZ, and awarded bronze at the 2013 NZ Coffee Awards.</p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('209', '63', '10', '1', '1', '1', 'Product', '2016-09-02 12:05:12', '2016-09-02 07:49:56', 'city-blend', 'City Blend', null, '<p>Developed with the flat white in mind, this blend perfectly cuts through the milk-based drinks that Kiwi coffee drinkers love so much. This is a four origin blend roasted medium-dark, giving rich, warm, chocolate flavour notes. Medium acidity.</p><p>Our City Blend was voted one of the best Flat White blends in NZ, and awarded bronze at the 2013 NZ Coffee Awards.</p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('210', '63', '11', '1', '1', '1', 'Product', '2016-09-02 12:05:22', '2016-09-02 07:49:56', 'city-blend', 'City Blend', null, '<p>Developed with the flat white in mind, this blend perfectly cuts through the milk-based drinks that Kiwi coffee drinkers love so much. This is a four origin blend roasted medium-dark, giving rich, warm, chocolate flavour notes. Medium acidity.</p><p>Our City Blend was voted one of the best Flat White blends in NZ, and awarded bronze at the 2013 NZ Coffee Awards.</p>', null, null, '0', '1', '2', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('211', '64', '5', '1', '1', '1', 'Product', '2016-09-02 12:05:41', '2016-09-02 07:49:56', 'new-northern', 'New Northern', null, null, null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('212', '64', '6', '1', '1', '1', 'Product', '2016-09-02 12:05:45', '2016-09-02 07:49:56', 'new-northern', 'New Northern', null, null, null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('213', '64', '7', '1', '1', '1', 'Product', '2016-09-02 12:06:14', '2016-09-02 07:49:56', 'new-northern', 'New Northern', null, null, null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('214', '64', '8', '1', '1', '1', 'Product', '2016-09-02 12:24:19', '2016-09-02 07:49:56', 'new-northern', 'New Northern', null, '<p>This blend contains medium roasted coffee beans from the Pacific, Central &amp; South America.</p><p>It’s a full bodied brew with subtle complexity, exhibiting a light spicy finish. It’s equally good as an espresso or in the plunger.</p>', null, null, '0', '1', '3', '0', '0', null, 'Inherit', 'Inherit', null, '8'), ('215', '23', '5', '1', '1', '1', 'BlogPost', '2016-09-02 12:32:01', '2016-09-01 11:45:07', 'maybe-you-and-me-were-never-meant-to-be', 'Maybe you and me were never meant to be', null, '<p><span>But baby think of me once in awhile. I\'m at WKRP in Cincinnati. Straightnin\' the curves. Flatnin\' the hills Someday the mountain might get ‘em but the law never will? </span></p>', null, null, '0', '1', '7', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('216', '22', '4', '1', '1', '1', 'BlogPost', '2016-09-02 12:39:50', '2016-09-01 10:48:03', 'another-new-item-of-news', 'Another new item of news', null, '<p><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></p><p><span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span><span>Now were up in the big leagues getting\' our turn at bat. Doin\' it our way. There\'s nothing we wont try. Never heard the word impossible. This time there\'s no stopping us.</span></span></p>', null, null, '0', '1', '6', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('217', '20', '4', '1', '1', '1', 'BlogPost', '2016-09-02 12:40:02', '2016-09-01 10:42:30', 'pouring-the-perfect-cup-4', 'Pouring the perfect cup 4', null, '<p><span>Maybe you and me were never meant to be. But baby think of me once in awhile. </span></p><p><span>I\'m at WKRP in Cincinnati. Straightnin\' the curves. Flatnin\' the hills Someday the mountain might get ‘em but the law never will? And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Flying away on a wing and a prayer. </span></p><p><span>Who could it be? Believe it or not its just me.</span></p>', null, null, '0', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('218', '67', '1', '0', '1', '0', 'BlogPost', '2016-09-02 12:41:18', '2016-09-02 12:41:18', 'new-blog-post', 'New Blog Post', null, null, null, null, '0', '1', '8', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('219', '67', '2', '0', '1', '0', 'BlogPost', '2016-09-02 12:41:25', '2016-09-02 12:41:18', 'new-blog-post', 'New Blog Post', null, null, null, null, '0', '1', '8', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('220', '67', '3', '1', '1', '1', 'BlogPost', '2016-09-02 12:41:25', '2016-09-02 12:41:18', 'new-blog-post', 'New Blog Post', null, null, null, null, '0', '1', '8', '0', '0', null, 'Inherit', 'Inherit', null, '12'), ('221', '9', '6', '1', '1', '1', 'CoffeePage', '2016-09-02 12:58:29', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p><p>Awards</p><hr><p> </p><p>Here\'s a few awards we\'ve won</p><p><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/awards.png\" alt=\"awards\"></p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('222', '9', '7', '1', '1', '1', 'CoffeePage', '2016-09-02 12:59:02', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p><p>Awards</p><hr><p class=\"lead\">Here\'s a few awards we\'ve won</p><p><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/awards.png\" alt=\"awards\"></p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('223', '9', '8', '1', '1', '1', 'CoffeePage', '2016-09-02 12:59:38', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p><h1>Awards</h1><hr><p class=\"lead\">Here\'s a few awards we\'ve won</p><p><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/awards.png\" alt=\"awards\"></p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('224', '9', '9', '1', '1', '1', 'CoffeePage', '2016-09-02 13:01:55', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p><h1>Awards</h1><hr><p class=\"lead\">Here\'s a few awards we\'ve won</p><p><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/awards.png\" alt=\"awards\"></p><p>Certified</p><hr><p>We help reduce the environmental impact of bad agriculture practice by choosing beans from sustainable farms.</p><p>Santos Coffee is dedicated to helping people preserve the environment, which is why traceability and sustainability in sourcing coffee beans is very high on our agenda.</p><p>The Rainforest Alliance is an international non-profit organisation that works to conserve biodiversity and promote the rights and well-being of workers, their families and communities. Farms that meet comprehensive standards for sustainability earn the Rainforest Alliance Certified™ seal. These standards conserve biodiversity, ensure that soils, waterways and wildlife habitat is protected and that farm workers enjoy decent housing, access to medical care and schools for their children.</p><p>Farms must commit to a process of continuous improvement and are audited each year in order to maintain their certification. By shopping for products bearing the Rainforest Alliance Certified seal, you can support a healthy environment and help to improve the quality of life for farm families. To learn more about the Rainforest Alliance, visit www.rainforest-alliance.org.</p><p><img src=\"assets/Uploads/certified-green.svg\" width=\"144\" alt=\"\" title=\"\"></p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('225', '9', '10', '1', '1', '1', 'CoffeePage', '2016-09-02 13:03:38', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p><h1>Awards</h1><hr><p class=\"lead\">Here\'s a few awards we\'ve won</p><p><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/awards.png\" alt=\"awards\"></p><p>Certified</p><hr><p>We help reduce the environmental impact of bad agriculture practice by choosing beans from sustainable farms.</p><p>Santos Coffee is dedicated to helping people preserve the environment, which is why traceability and sustainability in sourcing coffee beans is very high on our agenda.</p><p>The Rainforest Alliance is an international non-profit organisation that works to conserve biodiversity and promote the rights and well-being of workers, their families and communities. Farms that meet comprehensive standards for sustainability earn the Rainforest Alliance Certified™ seal. These standards conserve biodiversity, ensure that soils, waterways and wildlife habitat is protected and that farm workers enjoy decent housing, access to medical care and schools for their children.</p><p>Farms must commit to a process of continuous improvement and are audited each year in order to maintain their certification. By shopping for products bearing the Rainforest Alliance Certified seal, you can support a healthy environment and help to improve the quality of life for farm families. To learn more about the Rainforest Alliance, visit www.rainforest-alliance.org.</p><p><img title=\"\" src=\"assets/Uploads/certified-green.svg\" alt=\"\" width=\"144\"></p><h1>Blends</h1><hr><p class=\"lead\">It’s a fine art creating quality blends, because a blend is greater than the sum of its parts. When creating a blend, our goal is to achieve a balanced, distinct flavour profile that can then be consistently reproduced.</p><p>Beans are chosen for their ability to complement each other, based on flavour, aroma and body. For this reason, blends often provide a more complex character when compared to varietals or single origin beans.</p><p>A lot of factors goes into the blending process to ensure strict quality control. This includes a lot of coffee cupping, or tasting, to observe the tastes and aromas of brewed coffee. It’s here that we identify the flavour profiles, much like a chef does with food. We then experiment until we have the ideal blend.</p><p>One way were able to produce consistent blends is in the way we roast and blend. There are two ways blending occurs. The first is mix all beans together and roast. The second, our way, is to roast the beans separately, then blend them. This allows us to roast each bean exactly how we want to, and the way that we know will produce optimal results for that bean.</p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('226', '9', '11', '1', '1', '1', 'CoffeePage', '2016-09-02 13:04:07', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p><h1>Awards</h1><hr><p class=\"lead\">Here\'s a few awards we\'ve won</p><p><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/awards.png\" alt=\"awards\"></p><p>Certified</p><hr><p>We help reduce the environmental impact of bad agriculture practice by choosing beans from sustainable farms.</p><p>Santos Coffee is dedicated to helping people preserve the environment, which is why traceability and sustainability in sourcing coffee beans is very high on our agenda.</p><p>The Rainforest Alliance is an international non-profit organisation that works to conserve biodiversity and promote the rights and well-being of workers, their families and communities. Farms that meet comprehensive standards for sustainability earn the Rainforest Alliance Certified™ seal. These standards conserve biodiversity, ensure that soils, waterways and wildlife habitat is protected and that farm workers enjoy decent housing, access to medical care and schools for their children.</p><p>Farms must commit to a process of continuous improvement and are audited each year in order to maintain their certification. By shopping for products bearing the Rainforest Alliance Certified seal, you can support a healthy environment and help to improve the quality of life for farm families. To learn more about the Rainforest Alliance, visit www.rainforest-alliance.org.</p><p><img title=\"\" src=\"assets/Uploads/certified-green.svg\" alt=\"\" width=\"144\"></p><h1>Blends</h1><hr><p class=\"lead\">It’s a fine art creating quality blends, because a blend is greater than the sum of its parts. When creating a blend, our goal is to achieve a balanced, distinct flavour profile that can then be consistently reproduced.</p><p>Beans are chosen for their ability to complement each other, based on flavour, aroma and body. For this reason, blends often provide a more complex character when compared to varietals or single origin beans.</p><p>A lot of factors goes into the blending process to ensure strict quality control. This includes a lot of coffee cupping, or tasting, to observe the tastes and aromas of brewed coffee. It’s here that we identify the flavour profiles, much like a chef does with food. We then experiment until we have the ideal blend.</p><p>One way were able to produce consistent blends is in the way we roast and blend. There are two ways blending occurs. The first is mix all beans together and roast. The second, our way, is to roast the beans separately, then blend them. This allows us to roast each bean exactly how we want to, and the way that we know will produce optimal results for that bean.</p><p> </p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('227', '9', '12', '1', '1', '1', 'CoffeePage', '2016-09-02 13:04:21', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p><h1>Awards</h1><hr><p class=\"lead\">Here\'s a few awards we\'ve won</p><p><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/awards.png\" alt=\"awards\"></p><h1>Certified</h1><hr><p>We help reduce the environmental impact of bad agriculture practice by choosing beans from sustainable farms.</p><p>Santos Coffee is dedicated to helping people preserve the environment, which is why traceability and sustainability in sourcing coffee beans is very high on our agenda.</p><p>The Rainforest Alliance is an international non-profit organisation that works to conserve biodiversity and promote the rights and well-being of workers, their families and communities. Farms that meet comprehensive standards for sustainability earn the Rainforest Alliance Certified™ seal. These standards conserve biodiversity, ensure that soils, waterways and wildlife habitat is protected and that farm workers enjoy decent housing, access to medical care and schools for their children.</p><p>Farms must commit to a process of continuous improvement and are audited each year in order to maintain their certification. By shopping for products bearing the Rainforest Alliance Certified seal, you can support a healthy environment and help to improve the quality of life for farm families. To learn more about the Rainforest Alliance, visit www.rainforest-alliance.org.</p><p><img title=\"\" src=\"assets/Uploads/certified-green.svg\" alt=\"\" width=\"144\"></p><h1>Blends</h1><hr><p class=\"lead\">It’s a fine art creating quality blends, because a blend is greater than the sum of its parts. When creating a blend, our goal is to achieve a balanced, distinct flavour profile that can then be consistently reproduced.</p><p>Beans are chosen for their ability to complement each other, based on flavour, aroma and body. For this reason, blends often provide a more complex character when compared to varietals or single origin beans.</p><p>A lot of factors goes into the blending process to ensure strict quality control. This includes a lot of coffee cupping, or tasting, to observe the tastes and aromas of brewed coffee. It’s here that we identify the flavour profiles, much like a chef does with food. We then experiment until we have the ideal blend.</p><p>One way were able to produce consistent blends is in the way we roast and blend. There are two ways blending occurs. The first is mix all beans together and roast. The second, our way, is to roast the beans separately, then blend them. This allows us to roast each bean exactly how we want to, and the way that we know will produce optimal results for that bean.</p><p> </p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('228', '9', '13', '1', '1', '1', 'CoffeePage', '2016-09-02 13:04:30', '2016-08-24 18:14:41', 'coffee', 'Coffee', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p><h1>Awards</h1><hr><p class=\"lead\">Here\'s a few awards we\'ve won</p><p><img class=\"img-responsive\" title=\"\" src=\"assets/Uploads/awards.png\" alt=\"awards\"></p><h1>Certified</h1><hr><p class=\"lead\">We help reduce the environmental impact of bad agriculture practice by choosing beans from sustainable farms.</p><p>Santos Coffee is dedicated to helping people preserve the environment, which is why traceability and sustainability in sourcing coffee beans is very high on our agenda.</p><p>The Rainforest Alliance is an international non-profit organisation that works to conserve biodiversity and promote the rights and well-being of workers, their families and communities. Farms that meet comprehensive standards for sustainability earn the Rainforest Alliance Certified™ seal. These standards conserve biodiversity, ensure that soils, waterways and wildlife habitat is protected and that farm workers enjoy decent housing, access to medical care and schools for their children.</p><p>Farms must commit to a process of continuous improvement and are audited each year in order to maintain their certification. By shopping for products bearing the Rainforest Alliance Certified seal, you can support a healthy environment and help to improve the quality of life for farm families. To learn more about the Rainforest Alliance, visit www.rainforest-alliance.org.</p><p><img title=\"\" src=\"assets/Uploads/certified-green.svg\" alt=\"\" width=\"144\"></p><h1>Blends</h1><hr><p class=\"lead\">It’s a fine art creating quality blends, because a blend is greater than the sum of its parts. When creating a blend, our goal is to achieve a balanced, distinct flavour profile that can then be consistently reproduced.</p><p>Beans are chosen for their ability to complement each other, based on flavour, aroma and body. For this reason, blends often provide a more complex character when compared to varietals or single origin beans.</p><p>A lot of factors goes into the blending process to ensure strict quality control. This includes a lot of coffee cupping, or tasting, to observe the tastes and aromas of brewed coffee. It’s here that we identify the flavour profiles, much like a chef does with food. We then experiment until we have the ideal blend.</p><p>One way were able to produce consistent blends is in the way we roast and blend. There are two ways blending occurs. The first is mix all beans together and roast. The second, our way, is to roast the beans separately, then blend them. This allows us to roast each bean exactly how we want to, and the way that we know will produce optimal results for that bean.</p><p> </p>', null, null, '1', '1', '5', '0', '0', null, 'Inherit', 'Inherit', null, '0'), ('229', '8', '14', '1', '1', '1', 'ProductCategory', '2016-09-02 13:06:12', '2016-08-24 18:14:27', 'shop', 'Shop', null, '<h1>Roastery</h1><hr><p class=\"lead\">We roast on a 10 kilo \"Leogap\" hot air drum roaster. </p><p>Its first home was in Mt Eden, before moving to Kingsland, then settling in its current Newmarket location, along with our bar and cafē, overlooking the picturesque Auckland Domain. You’re welcome to visit our roastery any time during opening hours to enjoy a full coffee experience, from green bean to your cup.</p><p><img title=\"\" src=\"assets/Uploads/roasted-in-nz-blue.svg\" alt=\"\" width=\"125\"></p><h1>Coffee</h1><hr><p class=\"lead\">Producing the perfect coffee is a lifelong commitment. We are constantly searching for quality coffee that stands out with first class production values and taste.</p><p>We choose our coffee to reflect different regions around the world; which when roasted our way and then blended together create our unique Santos flavour.</p><p>A lot goes into delivering you the perfect coffee. Even with our depth of experience we spend a lot of time developing blends to suit different types of coffees. Some blends work better as espresso, and others a flat white or plunger, etc. Key to everything though is freshness. A lot of roasted coffees hit our shores beyond their peak. So we ensure optimal quality with a one-week turnaround.</p><p>We batch roast single origins, and then blend them to create distinct taste profiles. This process creates a bit more work for us but it ensures the individual origins are roasted perfectly as opposed to pre-blending and then roasting. This is because different beans roast differently. If you roast two single origins together, one bean type will be perfect while another could be over or under done.</p><p>The beans are then left to de-gas for two days before getting packed into one way valve coffee bags (to keep oxygen out and a low gas to be released, keeping beans as fresh as possible) and then shipped to our customers. Remember, freshness is key to great coffee, but it can be too fresh. Ideally, your coffee will be in the cup one week after roasting, which produces the perfect crema and taste.</p><p>We’re involved in all aspects of the coffee industry. Like any other consumable, there are different grades with coffee. Our blends are all premium A-grade coffee. Our speciality blends are the super-premium end of the market. We roast small batches of speciality beans each month so that our customers can enjoy the best coffee from around the world. Africa, South &amp; Central America and the Pacific. We never blend speciality beans as they are already crazy-good.</p>', null, null, '1', '1', '4', '0', '0', null, 'Inherit', 'Inherit', null, '0');
COMMIT;

-- ----------------------------
--  Table structure for `StaffProfile`
-- ----------------------------
DROP TABLE IF EXISTS `StaffProfile`;
CREATE TABLE `StaffProfile` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SubHead` mediumtext CHARACTER SET utf8,
  `StaffType` enum('Principal','Solicitor','LegalExec') CHARACTER SET utf8 DEFAULT 'Principal',
  `Expertise` mediumtext CHARACTER SET utf8,
  `StaffImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `StaffImageID` (`StaffImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `StaffProfile`
-- ----------------------------
BEGIN;
INSERT INTO `StaffProfile` VALUES ('13', null, 'Principal', null, '20'), ('14', null, 'Principal', null, '21'), ('15', null, 'Principal', null, '23'), ('16', null, 'Principal', null, '24');
COMMIT;

-- ----------------------------
--  Table structure for `StaffProfile_Live`
-- ----------------------------
DROP TABLE IF EXISTS `StaffProfile_Live`;
CREATE TABLE `StaffProfile_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SubHead` mediumtext CHARACTER SET utf8,
  `StaffType` enum('Principal','Solicitor','LegalExec') CHARACTER SET utf8 DEFAULT 'Principal',
  `Expertise` mediumtext CHARACTER SET utf8,
  `StaffImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `StaffImageID` (`StaffImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `StaffProfile_Live`
-- ----------------------------
BEGIN;
INSERT INTO `StaffProfile_Live` VALUES ('13', null, 'Principal', null, '20'), ('14', null, 'Principal', null, '21'), ('15', null, 'Principal', null, '23'), ('16', null, 'Principal', null, '24');
COMMIT;

-- ----------------------------
--  Table structure for `StaffProfile_versions`
-- ----------------------------
DROP TABLE IF EXISTS `StaffProfile_versions`;
CREATE TABLE `StaffProfile_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `SubHead` mediumtext CHARACTER SET utf8,
  `StaffType` enum('Principal','Solicitor','LegalExec') CHARACTER SET utf8 DEFAULT 'Principal',
  `Expertise` mediumtext CHARACTER SET utf8,
  `StaffImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `StaffImageID` (`StaffImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `StaffProfile_versions`
-- ----------------------------
BEGIN;
INSERT INTO `StaffProfile_versions` VALUES ('1', '13', '1', null, 'Principal', null, '0'), ('2', '13', '2', null, 'Principal', null, '0'), ('3', '13', '3', null, 'Principal', null, '0'), ('4', '13', '4', null, 'Principal', null, '20'), ('5', '14', '1', null, 'Principal', null, '0'), ('6', '14', '2', null, 'Principal', null, '21'), ('7', '14', '3', null, 'Principal', null, '21'), ('8', '15', '1', null, 'Principal', null, '0'), ('9', '15', '2', null, 'Principal', null, '22'), ('10', '15', '3', null, 'Principal', null, '23'), ('11', '15', '4', null, 'Principal', null, '23'), ('12', '16', '1', null, 'Principal', null, '0'), ('13', '16', '2', null, 'Principal', null, '24'), ('14', '13', '5', null, 'Principal', null, '20'), ('15', '13', '6', null, 'Principal', null, '20'), ('16', '13', '7', null, 'Principal', null, '20');
COMMIT;

-- ----------------------------
--  Table structure for `TagCloudWidget`
-- ----------------------------
DROP TABLE IF EXISTS `TagCloudWidget`;
CREATE TABLE `TagCloudWidget` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Limit` int(11) NOT NULL DEFAULT '0',
  `Sortby` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `TaxModifier`
-- ----------------------------
DROP TABLE IF EXISTS `TaxModifier`;
CREATE TABLE `TaxModifier` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Rate` double DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `TaxModifier`
-- ----------------------------
BEGIN;
INSERT INTO `TaxModifier` VALUES ('1', '0.15'), ('6', '0.15');
COMMIT;

-- ----------------------------
--  Table structure for `VirtualPage`
-- ----------------------------
DROP TABLE IF EXISTS `VirtualPage`;
CREATE TABLE `VirtualPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `VirtualPage_Live`
-- ----------------------------
DROP TABLE IF EXISTS `VirtualPage_Live`;
CREATE TABLE `VirtualPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `VirtualPage_versions`
-- ----------------------------
DROP TABLE IF EXISTS `VirtualPage_versions`;
CREATE TABLE `VirtualPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Widget`
-- ----------------------------
DROP TABLE IF EXISTS `Widget`;
CREATE TABLE `Widget` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Widget','BlogArchiveWidget','ArchiveWidget','BlogCategoriesWidget','BlogRecentPostsWidget','BlogTagsCloudWidget','BlogTagsWidget','TagCloudWidget','CartWidget') CHARACTER SET utf8 DEFAULT 'Widget',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `Enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `Widget`
-- ----------------------------
BEGIN;
INSERT INTO `Widget` VALUES ('1', 'BlogCategoriesWidget', '2016-09-01 11:57:26', '2016-09-01 11:57:00', 'News Categories', '0', '1', '1'), ('2', 'BlogTagsWidget', '2016-09-01 11:57:26', '2016-09-01 11:57:00', 'News Tags', '1', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `WidgetArea`
-- ----------------------------
DROP TABLE IF EXISTS `WidgetArea`;
CREATE TABLE `WidgetArea` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('WidgetArea') CHARACTER SET utf8 DEFAULT 'WidgetArea',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `WidgetArea`
-- ----------------------------
BEGIN;
INSERT INTO `WidgetArea` VALUES ('1', 'WidgetArea', '2016-09-01 11:57:00', '2016-09-01 11:57:00'), ('2', 'WidgetArea', '2016-09-01 13:24:53', '2016-09-01 13:24:53'), ('3', 'WidgetArea', '2016-09-02 12:39:50', '2016-09-02 12:39:50'), ('4', 'WidgetArea', '2016-09-02 12:40:02', '2016-09-02 12:40:02'), ('5', 'WidgetArea', '2016-09-02 12:41:25', '2016-09-02 12:41:25');
COMMIT;

-- ----------------------------
--  Table structure for `Zone`
-- ----------------------------
DROP TABLE IF EXISTS `Zone`;
CREATE TABLE `Zone` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Zone') CHARACTER SET utf8 DEFAULT 'Zone',
  `LastEdited` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Description` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `ZoneRegion`
-- ----------------------------
DROP TABLE IF EXISTS `ZoneRegion`;
CREATE TABLE `ZoneRegion` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ZoneID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ZoneID` (`ZoneID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
