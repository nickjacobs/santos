<div id="$Name" class="form-group">
    <div class="checkbox">
    	<label for="$ID">$Field $Title</label>
    	<% if Message %><span class="help-block $MessageType">$Message</span><% end_if %>
    </div>
</div>
